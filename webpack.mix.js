const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/assets/js').sass('resources/sass/app.scss', 'public/assets/css');;


mix.js('resources/js/order.js', 'public/assets/js/order.js')
    .sass('resources/sass/order.scss', 'public/assets/css/order.css')
    .version();

// if (mix.inProduction()) {
//     mix.version();
// }

// mix.options({
//     hmrOptions: {
//         host: 'localhost', // site's host name
//     }
// });

// // // fix css files 404 issue
// mix.webpackConfig({
//     // add any webpack dev server config here
//     devServer: {
//         proxy: {
//             host: '127.0.0.1', // host machine ip
//             port: 8080,
//         },
//         watchOptions: {
//             aggregateTimeout: 200,
//             poll: 5000
//         },

//     }
// });
