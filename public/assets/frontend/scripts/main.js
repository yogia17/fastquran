$(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $('#toggle-collapse').click(function() {
        $(this).toggleClass('is-active');
        $(this).parents('aside').toggleClass('collapsed');
        $(this).parents('aside').find('ul').toggle();
    });

    // console.log($(".nav__sub-menu li a").position().top)
});

$(function() {
    //console.log("BWA Run");
    const limit = 6;
    let items = [];
    let showNow = [];
    $('#load-more-wrapper > [class^="col"]').each(function(i, e) {
        if (i + 1 > limit) {
            items.push(e)
        } else {
            $(e).removeClass("d-none")
            setTimeout(function() {
                $(e).removeClass("hide")
            }, 10)
        }
    })
    $(document).on("click", '#load-more-button', function() {
        showNow = items.splice(0, limit)

        if (items <= limit)
            $(this).hide()

        showNow.forEach(e => {
            $(e).removeClass("d-none")
            setTimeout(function() {
                $(e).removeClass("hide")
            }, 10)
        })
    })
})

$(function() {
    //console.log("BWA Parent Load More");
    const limit = 3;
    let items = [];
    let showNow = [];
    $('#load-more-wrapper-materi > [class^="item-parent-materi"]').each(function(i, e) {
        if (i + 1 > limit) {
            items.push(e)
        } else {
            $(e).removeClass("d-none")
            setTimeout(function() {
                $(e).removeClass("hide")
            }, 10)
        }
    })
    $(document).on("click", '#load-more-materi', function() {
        showNow = items.splice(0, limit)

        if (items <= limit)
            $(this).hide()

        showNow.forEach(e => {
            $(e).removeClass("d-none")
            setTimeout(function() {
                $(e).removeClass("hide")
            }, 10)
        })
    })
})

//console.log("BWA Collapse");
var parentMateri = document.getElementsByClassName('parent-materi-public');
if(parentMateri.length > 0) {    
    parentMateri[0].classList.add('show');
}
