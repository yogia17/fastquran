<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Enrollment extends Model
{
    use Notifiable;

    protected $fillable = [
        'user_id', 'course_id'];
    public function course()
    {
        return $this->belongsTo(Course::class);
    }

}
