<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Auth;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

class UserResetPasswordNotification extends Notification
{
    use Queueable;
    /**
     * @var User
     */
    private $user;
    private $reset_url;

    /**
     * Create a new notification instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        $this->reset_url = route('reset', ['token' => encrypt($user->id)]);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail',TelegramChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Reset Password FastQuran.id')
            ->greeting('Ubah Kata Sandi')
            ->line('Silakan atur ulang kata sandi dengan klik tombol di bawah ini')
            ->action('Reset Password', $this->reset_url)
            ->line('Abaikan Email ini jika kamu tidak merasa melakukan penyetelan ulang kata sandi')
            ->salutation(' ');
    }

    public function toTelegram($notifiable)
    {
        $name = $this->user->name;
        $email = $this->user->email;
        return TelegramMessage::create()
            // Optional recipient user id.
            ->to('-352355554') //Fastquran group
            // Markdown supported.
            ->content("Reset Password!\n Name: *$name*\n Email: *$email*")
            // (Optional) Inline Buttons
            ->button('View Link', $this->reset_url);
    }

    public function toSlack($notifiable)
    {
        return (new SlackMessage())
            ->success()
            ->content("Email Forgot Password Gan")
            ->attachment(function ($attachment) use ($notifiable) {
                $attachment->title('Reset Password', $this->reset_url)
                    ->fields([
                        'name' => $this->user->name,
                        'email' => $this->user->email,
                    ]);
            });
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
