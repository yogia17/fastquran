<?php

namespace App\Notifications;

use App\Order;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

class UserRegisterNotification extends Notification
{
    use Queueable;
    /**
     * @var User
     */
    private $order;

    /**
     * Create a new notification instance.
     *
     * @param User $user
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
//        $this->activation_url = route('active', ['token' => encrypt($user->id)]);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [TelegramChannel::class];
    }


    public function toTelegram($notifiable)
    {
        $email = $this->order->email;
        $promo_code = $this->order->promo_code;
        return TelegramMessage::create()
            // Optional recipient user id.
            ->to('-352355554') //Fastquran group
            // Markdown supported.
            ->content("New Register!\n Email: *$email*\n Promo Code: *$promo_code*\n Total: ".rp($this->order->total_price)."\n ")
            // (Optional) Inline Buttons
            ->button('View Link', 'https://fastquran.id/login');
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
