<?php


namespace App\Repositories;

use App\Exceptions\UserException;
use App\Notifications\UserResetPasswordNotification;
use App\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UserRepository
{
    public function bannedUsers()
    {

    }

    public function activeUsers()
    {

    }

    public function adminUsers()
    {

    }

    public function verifiedEmail()
    {

    }

    public function unverifiedEmail()
    {

    }

    public function create($data)
    {
        return User::create($data);
    }
    public function createStudent($data)
    {
        return User::create($data);
    }

    public function authUser()
    {
        $user = Auth::user();
        return $user;
    }
    public function findAuthenticated()
    {
        $user_id = auth()->id();
        return User::find($user_id);
    }
    public function findById($id)
    {
        return User::findOrFail($id);
    }

    public function register($formData)
    {
        $isExist = $this->findBy('email', $formData['email'])->count();
        if($isExist) throw new UserException(__('api.register.email_not_unique'));

        $default = [
            'is_student' => 1,
        ];

        $data = array_merge($default, [
            'name' => $formData['name'],
            'email' => $formData['email'],
            'password' => bcrypt($formData['password'])
        ]);

        $user = $this->create($data);
        return $user;
    }

    public function findBy($column, $value)
    {
        return User::where($column, $value)->get();
    }

    public function forgotPassword($email)
    {
        $user = $this->findBy('email', $email)->first();

        if(!$user) throw new UserException(__('api.register.user_not_registered'));
        $user->notify(new UserResetPasswordNotification($user));

    }

    public function updatePassword(User $user, $password)
    {
        $user->password = bcrypt($password);
        $user->save();
    }

    public function findOrCreate($where, $data)
    {
        return User::firstOrCreate($where, $data);
    }

    public function datatableCustomer()
    {
        return User::whereIn('level',['Customer','Diamond'])
            ->orderBy('transaction_success','desc');
    }

    public function countCustomer()
    {
        return User::where('level','customer')->count();
    }

    public function countDiamond()
    {
        return User::where('level','diamond')->count();
    }

    public function banned($id)
    {
        $this->findById($id)->update(['banned' => true]);
    }

    public function unbanned($id)
    {
        $this->findById($id)->update(['banned' => false]);
    }

    public function all()
    {
        return User::orderBy('name','asc')->get();
    }

    public function updateBy($where, $where_value, $data)
    {
        User::where($where, $where_value)->update($data);
    }

    public function update($id, $formData)
    {
        if (isset($formData['ktp']) && $formData['ktp'] instanceof UploadedFile) {
            $formData['ktp'] = uploadCloud('ktp', $formData['ktp']);
        }
        $user = $this->findById($id);
        $default = [];
        $data = array_merge($default, $formData);
        $save = $user->update($data);
    }

}
