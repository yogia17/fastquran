<?php


namespace App\Repositories;


use App\Lesson;
use Illuminate\Support\Str;

class LessonRepository
{
    private $columns = [
        'id',
        'slug',
        'title',
        'description',
        'type',
        'level',
        'image',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function create($formData)
    {
        $default = ['sort_at' => now(), 'parent_id' => null];
        $slug = ['slug' => Str::slug($formData['title'] . '-' . Str::random(5))];
        $data = array_merge($default, $slug, $formData);
        Lesson::create($data);
    }

    public function update($id, $formData)
    {
        $kelas = $this->findById($id);
        $default = [];
        $data = array_merge($default, $formData);
        $kelas->update($data);
    }

    public function destroy($id)
    {
        $kelas = $this->findById($id);
        $kelas->delete();
    }

    public function push($id)
    {
        $this->findById($id)->update(['sort_at' => now()]);
    }

    public function getActiveLesson()
    {
        return Lesson::select($this->columns)->get();
    }

    public function pluckActiveLesson()
    {
        return Lesson::pluck('title', 'id');
    }

    public function findById($id)
    {
        return Lesson::findOrFail($id);
    }

    public function findBySlug($slug)
    {
        return Lesson::where('slug', $slug)->firstOrFail();
    }

    public function datatable()
    {
        return Lesson::orderBy('sort_at', 'desc');
    }

    public function getLessons($type = "", $id = "")
    {
        $lessons = Lesson::orderBy('order', 'asc');
        if ($type == "course") {
            return $lessons->where('course_id', $id)->get();
        } elseif ($type == "section") {
            return $lessons->where('section_id', $id)->get();
        } elseif ($type == "lesson") {
            return $lessons->where('id', $id)->get();
        }
    }
}

