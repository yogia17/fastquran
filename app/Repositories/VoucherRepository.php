<?php


namespace App\Repositories;

use App\Exceptions\VoucherException;
use App\Voucher;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class VoucherRepository
{
    public function create($data)
    {
        return Voucher::create($data);
    }

    public function findById($id)
    {
        return Voucher::findOrFail($id);
    }

    public function get($column, $value)
    {
        return Voucher::where($column, $value)->first();
    }

    public function findBy($column, $value)
    {
        return Voucher::where($column, $value)->get();
    }

    public function findOrCreate($where, $data)
    {
        return Voucher::firstOrCreate($where, $data);
    }

    public function all()
    {
        return Voucher::orderBy('voucher_name','asc')->get();
    }

    public function updateBy($where, $where_value, $data)
    {
        Voucher::where($where, $where_value)->update($data);
    }


}
