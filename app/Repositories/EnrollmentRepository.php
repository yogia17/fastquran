<?php


namespace App\Repositories;


use App\Enrollment;
use Illuminate\Support\Facades\Auth;

class EnrollmentRepository
{
    public function getUserCourse()
    {
        return Enrollment::where('user_id', Auth::id())->get();
    }
}
