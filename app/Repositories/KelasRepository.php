<?php


namespace App\Repositories;


use App\Course;
use App\ProductCourse;
use App\Section;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class KelasRepository
{
    private $columns = [
        'id',
        'slug',
        'title',
        'description',
        'type',
        'level',
        'image',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function create($formData)
    {
        $default = ['sort_at' => now(), 'parent_id' => null];
        $slug = ['slug' => Str::slug($formData['title'] . '-' . Str::random(5))];
        $data = array_merge($default, $slug, $formData);
        Course::create($data);
    }

    public function update($id, $formData)
    {
        $kelas = $this->findById($id);
        $default = [];
        $data = array_merge($default, $formData);
        $kelas->update($data);
    }

    public function destroy($id)
    {
        $kelas = $this->findById($id);
        $kelas->delete();
    }

    public function push($id)
    {
        $this->findById($id)->update(['sort_at' => now()]);
    }

    public function getActiveCourse()
    {
        return Course::select($this->columns)->get();
    }

    public function pluckActiveCourse()
    {
        return Course::pluck('title', 'id');
    }

    public function findById($id)
    {
        return Course::findOrFail($id);
    }

    public function findBySlug($slug)
    {
        return Course::where('slug', $slug)->firstOrFail();
    }

    public function datatable()
    {
        return Course::orderBy('sort_at', 'desc');
    }

    public function getActiveCourseTree()
    {
        return Course::select('id', 'title', 'slug')->get()->map(function ($kelas) {
            return [
                'id' => $kelas->id,
                'title' => $kelas->name,
                'slug' => $kelas->slug,
                'url' => route('api.products', ['kelas' => $kelas->id])
            ];
        });
    }

    public function pluck(string $name, string $id = null)
    {
        return Course::pluck($name, $id);
    }

    public function getSections($type_by, $id)
    {
        $sections = Section::orderBy('order', 'asc');
        if ($type_by == 'course') {
            return $sections->where('course_id', $id)->get();
        } elseif ($type_by == 'section') {
            return $sections->where('id', $id)->get();
        }
    }

    public function getLessonProgress($lesson_id = "", $user_id = "")
    {
        $user_details = Auth::user();
        $watch_history_array = json_decode($user_details->watch_history, true);
        for ($i = 0; $i < count($watch_history_array); $i++) {
            $watch_history_for_each_lesson = $watch_history_array[$i];
            if ($watch_history_for_each_lesson['lesson_id'] == $lesson_id) {
                return $watch_history_for_each_lesson['progress'];
            }
        }
        return 0;
    }

    public function setCourseProgress($course_id = "", $user_id = "")
    {
        $user_details = Auth::user();

        // this array will contain all the completed lessons from different different courses by a user
        $completed_lessons_ids = array();

        // this variable will contain number of completed lessons for a certain course. Like for this one the course_id
        $lesson_completed = 0;

        // User's watch history
        $watch_history_array = json_decode($user_details->watch_history, true);
        // desired course's lessons
        $lessons_for_that_course = (new LessonRepository())->getLessons('course', $course_id);
        // total number of lessons for that course
        $total_number_of_lessons = count($lessons_for_that_course);
        // arranging completed lesson ids
        for ($i = 0; $i < count($watch_history_array); $i++) {
            $watch_history_for_each_lesson = $watch_history_array[$i];
            if ($watch_history_for_each_lesson['progress'] == 1) {
                array_push($completed_lessons_ids, $watch_history_for_each_lesson['lesson_id']);
            }
        }

        foreach ($lessons_for_that_course->toArray() as $row) {
            if (in_array($row['id'], $completed_lessons_ids)) {
                $lesson_completed++;
            }
        }

        // calculate the percantage of progress
        $course_progress = ($lesson_completed / $total_number_of_lessons) * 100;
        return $course_progress;
    }

}

