<?php


namespace App\Repositories;


class ReorderRepository
{
    public function reorder($positions, $class, $column)
    {
        $lists = $class::all();
        $orders = [];
        foreach ($positions as $position) {
            $from = $position['from'];
            $to = $position['to'];

            $orders[$from] = $lists->firstWhere('id', $to)->$column;
        }
        foreach ($orders as $id => $time) {
            $lists->firstWhere('id', $id)->update([$column => $time]);
        }
    }
}
