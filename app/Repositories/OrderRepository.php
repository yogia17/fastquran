<?php


namespace App\Repositories;


use App\Order;

class OrderRepository
{
    public function create($data)
    {
        return Order::create($data);
    }
}
