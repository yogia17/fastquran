<?php

namespace App\Providers;

use App\Mixins\ResponseMixins;
use App\Mixins\StrMixins;
use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     * @throws \ReflectionException
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Str::mixin(new StrMixins());
        ResponseFactory::mixin(new ResponseMixins());
    }
}
