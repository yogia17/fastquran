<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Course extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'enrollments');
    }

    public function getSchedule()
    {
        $result =  Schedule::where('course_id', $this->course_id)->get()
        ->map(function ($item) {
            return ucfirst($item->day.' '.$item->starttime);
        });
        return $result;
    }

    public function countSiswa()
    {
        $result =  Enrollment::where('enrollments.course_id',$this->course_id)
            ->join('users as u','u.id','=','enrollments.user_id')
            ->where('u.type','murid')
            ->count();
        return $result;
    }

    public function getSection()
    {
        $result =  Attendance::where('attendances.course_id',$this->course_id)
            ->orderBy('attendances.id','desc')->first();
        return $result->section ?? '0';
    }

}
