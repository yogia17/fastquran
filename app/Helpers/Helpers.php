<?php

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

if (!function_exists('uploadCloud')) {
    function uploadCloud($path, UploadedFile $image)
    {
        $path = Storage::disk('public_uploads')->put($path, $image, ['visibility' => 'public']);
        return Storage::disk('public_uploads')->url($path);
    }
}

if (! function_exists('decrypt')) {
    /**
     * Decrypt the given value.
     *
     * @param  string  $value
     * @param  bool  $unserialize
     * @return mixed
     */
    function decrypt($value, $unserialize = true)
    {
        return app('encrypter')->decrypt($value, $unserialize);
    }
}
if (! function_exists('encrypt')) {
    /**
     * Encrypt the given value.
     *
     * @param  mixed  $value
     * @param  bool  $serialize
     * @return string
     */
    function encrypt($value, $serialize = true)
    {
        return app('encrypter')->encrypt($value, $serialize);
    }
}

if (!function_exists('rp')) {
    function rp($int)
    {
        return 'Rp. ' . number_format($int, 0, ',', '.');
    }
}

if (!function_exists('errorJson')) {
    function errorJson($message, $errorCode = 401)
    {
        return Response::errorJson($message, $errorCode);
    }
}
if (!function_exists('validateMail')) {
    function validateMail($email)
    {
        if (!preg_match('/^([a-z0-9\+\_\-\.]+)@([a-z0-9\+\_\-\.]{2,})(\.[a-z]{2,4})$/i', $email)) return true;
        $domains = array('gmail.com','yahoo.com','hotmail.com');
        list(, $email_domain) = explode('@', $email, 2);
        return in_array($email_domain, $domains);
    }
}
if (!function_exists('getAmount')) {
    function getAmount($money)
    {
        $cleanString = preg_replace('/([^0-9\.,])/i', '', $money);
        $onlyNumbersString = preg_replace('/([^0-9])/i', '', $money);

        $separatorsCountToBeErased = strlen($cleanString) - strlen($onlyNumbersString) - 1;

        $stringWithCommaOrDot = preg_replace('/([,\.])/', '', $cleanString, $separatorsCountToBeErased);
        $removedThousandSeparator = preg_replace('/(\.|,)(?=[0-9]{3,}$)/', '',  $stringWithCommaOrDot);

        return (int) str_replace(',', '.', $removedThousandSeparator);
    }
}

