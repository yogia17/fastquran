<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Order extends Model
{
    use Notifiable;

    protected $fillable = [
        'email', 'course_id', 'course_price', 'promo_code', 'promo_price', 'total_price'];
}
