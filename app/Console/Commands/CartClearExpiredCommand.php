<?php

namespace App\Console\Commands;

use App\Cart;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use App\Option;

class CartClearExpiredCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cart:clear_expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear Cart that Expire';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::whereNotNull('cart_expired_at')->get();

        foreach ($users as $user) {
            Auth::loginUsingId($user->id);

            if ($user->cart_expired_at == null) continue;

            if ($user->cart_expired_at <= now()) {
                $this->info("User '{$user->name}' cart is cleared");

                Cart::clearItems();

                $user->cart_expired_at = null;
                $user->cart_extends_count = 0;
                $user->save();
            }
        }
    }
}
