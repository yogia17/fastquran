<?php

namespace App\Console\Commands;

use App\Order;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ExpiredPaymentCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payment:cancel_expired_order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'cancel expired order';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::beginTransaction();
        Order::where('expired_at', '<', now())->where('transaction_status', 'pending')->get()->each(function ($order) {
            $order->update([
                'canceled_at' => now(),
                'transaction_status' => 'failure'
            ]);
            Order::restoreItems($order->id);
        });
        DB::commit();
    }
}
