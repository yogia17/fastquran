<?php

namespace App\Http\Controllers\Student;

use App\Http\Requests\UpdateProfileRequest;
use App\Repositories\EnrollmentRepository;
use App\Repositories\KelasRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\Auth;

class KelasController extends Controller
{
    private $kelasRepository;

    public function __construct(KelasRepository $kelasRepository)
    {
        $this->kelasRepository = $kelasRepository;
    }
    public function index()
    {
        $userCourse = (new EnrollmentRepository())->getUserCourse();
        return view('student.kelas.index',compact('userCourse'));
    }

    public function create()
    {
        # code...
    }

    public function store()
    {
        # code...
    }

    public function show(Request $request, $slug)
    {
//        dd($request->lesson);
        $kelas = $this->kelasRepository->findBySlug($slug);
        $sections = $this->kelasRepository->getSections('course',$kelas->id);
        return view('student.kelas.show',compact('kelas','sections'));
    }

    public function update(UpdateProfileRequest $request)
    {
        $request->validated();
        (new UserRepository())->update(Auth::id(), ['bank_name' => $request->bank_name, 'bank_no_rekening' => $request->bank_no_rekening, 'ktp' => $request->ktp]);
        return redirect()->route('partner.voucher.index')->with('success', 'Data berhasil disimpan');

    }

    public function destroy($id)
    {
        # code...
    }
}
