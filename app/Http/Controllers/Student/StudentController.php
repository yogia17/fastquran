<?php

namespace App\Http\Controllers\Student;

use App\Http\Requests\UpdateProfileRequest;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\Auth;

class StudentController extends Controller
{
    public function __construct()
    {
        # code...
    }

    public function index()
    {
        $partner = (new UserRepository())->findById(Auth::id());
        return view('partner.profile', compact('partner'));
    }

    public function create()
    {
        # code...
    }

    public function store()
    {
        # code...
    }

    public function edit($id)
    {
    }

    public function update(UpdateProfileRequest $request)
    {
        $request->validated();
        (new UserRepository())->update(Auth::id(), ['bank_name' => $request->bank_name, 'bank_no_rekening' => $request->bank_no_rekening, 'ktp' => $request->ktp]);
        return redirect()->route('partner.voucher.index')->with('success', 'Data berhasil disimpan');

    }

    public function destroy($id)
    {
        # code...
    }
}
