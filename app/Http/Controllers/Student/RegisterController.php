<?php


namespace App\Http\Controllers\Student;


use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    public function index()
    {
        return view('student.register.index');
    }
    public function store(RegisterRequest $request)
    {
        $request->validated();
        $user = (new UserRepository())->create([
            'name' => $request->name,
            'email' => $request->email,
            'handphone' => $request->handphone,
            'password' => bcrypt($request->password),
            'is_student' => 1
        ]);
        if ($user) {
            Auth::loginUsingId($user->id);
            return redirect()->route('student.kelas.index');
        }
        return back()->withErrors(["Pendaftaran gagal, silakan coba lagi!"]);

    }

}
