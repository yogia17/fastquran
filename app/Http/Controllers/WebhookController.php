<?php

namespace App\Http\Controllers;

use App\Webhook;
use App\Order;
use Veritrans_Config;
use Veritrans_Notification;

class WebhookController extends Controller
{
    private $filter = [];
    private $notification = [];

    public function __construct()
    {
        Veritrans_Config::$serverKey = env('MIDTRANS_SERVER_KEY', 'SB-Mid-server-J8GjAj69p80yM8Zda1-1oW28');
        Veritrans_Config::$isProduction = env('MIDTRANS_PROD', false);
        Veritrans_Config::$isSanitized = true;
        Veritrans_Config::$is3ds = true;
    }

    public function handleMidtransNotification()
    {
        $this->notification = $notification = new Veritrans_Notification();

        $transaction = $notification->transaction_status;
        $fraud = $notification->fraud_status;

        $this->saveNotification();

        $this->filter = [
            'order_id' => $notification->order_id,
            'transaction_id' => $notification->transaction_id
        ];

        if ($transaction == 'settlement' && $fraud == 'accept') {
            $this->transactionSuccess();
        } elseif ($transaction == 'settlement' && $fraud == 'challenge') {
            $this->transactionChallenge();
        } elseif ($transaction == 'cancel' || $transaction == 'deny' || $transaction == 'expire') {
            $this->transactionCanceled('failure');
        } elseif ($transaction == 'refund' || $transaction == 'partial_refund') {
            $this->transactionCanceled('refund');
        }
    }

    private function transactionSuccess()
    {
        Order::where($this->filter)->update([
            'transaction_status' => 'settlement',
            'settlement_at' => now(),
            'paid_at' => now(),
            'midtrans_response' => json_encode(collect($this->notification)->first()),
        ]);
    }

    private function transactionChallenge()
    {
        Order::where($this->filter)->update([
            'transaction_status' => 'challenge',
            'paid_at' => now(),
            'midtrans_response' => json_encode(collect($this->notification)->first()),
        ]);
    }

    public function transactionCanceled($status)
    {
        Order::where($this->filter)->update([
            'transaction_status' => $status,
            'canceled_at' => now(),
            'midtrans_response' => json_encode(collect($this->notification)->first()),
        ]);

        $this->restoreItems();
    }

    private function restoreItems()
    {
        $order = Order::where($this->filter)->first();
        if ($order) Order::restoreItems($order->id);
    }

    public function saveNotification()
    {
        Webhook::create([
            'type' => 'midtrans',
            'status' => $this->notification->transaction_status,
            'order_id' => $this->notification->order_id,
            'transaction_id' => $this->notification->transaction_id,
            'payment_type' => $this->notification->payment_type,
            'match_order' => Order::where($this->filter)->count() > 0 ? 'yes' : 'no',
            'response' => json_encode(collect($this->notification)->first()),
        ]);
    }
}
