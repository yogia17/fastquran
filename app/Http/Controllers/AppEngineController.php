<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;

class AppEngineController extends Controller
{
    public function scheduler()
    {
        Artisan::call('schedule:run');
    }
}
