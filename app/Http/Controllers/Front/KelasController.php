<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Repositories\KelasRepository;
use App\Repositories\UserRepository;
use App\Repositories\VoucherRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KelasController extends Controller
{
    private $kelasRepository;

    public function __construct(KelasRepository $kelasRepository)
    {
        $this->kelasRepository = $kelasRepository;
    }
    public function index()
    {
        $items = $this->kelasRepository->getActiveCourse();
        return view('frontend.kelas.index',compact('items'));
    }
    public function show($slug)
    {
        $kelas = $this->kelasRepository->findBySlug($slug);
        return view('frontend.kelas.show',compact('kelas'));
    }
    public function checkout($slug)
    {
        $kelas = $this->kelasRepository->findBySlug($slug);
        return view('frontend.kelas.checkout',compact('kelas'));
    }
    public function check_promo_code(Request $request)
    {
        $kelas = $this->kelasRepository->findBySlug($request->slug);
        $voucher = (new VoucherRepository())->get('voucher_name',$request->promo_code);
        if($voucher){
            $discount = $kelas->price * 0.1; // 10% diskon dari reseller
            $result = [
                'exist' => true,
                'price' => $kelas->price,
                'amount' => rp($kelas->price - $discount),
                'discount' => rp($discount),
                'code' => $voucher->voucher_name,
                'description' => 'Promo spesial <span class="green">10%</span>',
                'message' => ''

            ];

        }else{
            $result = [
                'exist' => false,
                'message' => 'Promo tidak ditemukan.'

            ];
        }

        return response()->json($result);
    }
}
