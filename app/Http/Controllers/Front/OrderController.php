<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Notifications\UserRegisterNotification;
use App\Repositories\KelasRepository;
use App\Repositories\OrderRepository;
use App\Repositories\UserRepository;
use App\Repositories\VoucherRepository;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    private $kelasRepository;

    public function __construct(KelasRepository $kelasRepository)
    {
        $this->kelasRepository = $kelasRepository;
    }

    public function check_email(Request $request)
    {
        $user = (new UserRepository())->findBy('email', $request->email)->first();
        if (validateMail($request->email)) {
            $result = [
                'exist' => $user ?? false,
                'message' => 'Gunakan email ini untuk konfirmasi pembayaran.',
                'valid_gmail' => 1,
                'promoNewMember' => null

            ];

        } else {
            $result = [
                'exist' => false,
                'message' => 'Alamat Email belum terdaftar. Silakan daftar akun terlebih dahulu.',
                'valid_gmail' => 0,
                'promoNewMember' => null

            ];
        }

        return response()->json($result);
    }

    public function order(Request $request)
    {
        if($request->email == ''){
            return errorJson('Email harus diisi',200);
        }
        $order = (new OrderRepository())->create([
            'email' => $request->email,
            'course_id' => $request->course_id,
            'course_price' => $request->course_price,
            'promo_code' => $request->promo_code,
            'promo_price' => getAmount($request->promo_price),
            'total_price' => getAmount($request->total_price)
        ]);
        if ($order) {
            $order->notify(new UserRegisterNotification($order));

            $result = [
                'status' => true,
                'message' => 'OK'
            ];

        } else {
            $result = [
                'status' => false,
                'message' => 'Fail'
            ];
        }

        return response()->json($result);
    }

}
