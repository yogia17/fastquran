<?php
namespace App\Http\Controllers;


use GuzzleHttp\Client;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        
    }
    public function login()
    {
        return view('frontend.' . $this->theme_name . '.login');
    }

}
