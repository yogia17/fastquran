<?php

namespace App\Http\Controllers\Partner;

use App\Http\Requests\CreateVoucherRequest;
use App\Repositories\VoucherRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\Auth;

class VoucherController extends Controller
{
    public function __construct()
    {
        # code...
    }

    public function index()
    {
        $voucher = (new VoucherRepository())->findBy('user_id',Auth::id());
        return view('partner.voucher', compact('voucher'));
    }

    public function create()
    {
        return view('partner.voucher.create');

    }

    public function store(CreateVoucherRequest $request)
    {
        $request->validated();
        $voucher = (new VoucherRepository())->create([
            'voucher_name' => $request->voucher_name,
            'user_id' => Auth::id()
        ]);
        if ($voucher) {
            return redirect()->route('partner.voucher.index')->with('success', 'Berhasil membuat kode voucher');

        }
        return back()->withErrors(["Gagal, silakan coba lagi!"]);
    }

    public function edit($id)
    {
    }

    public function update(Request $request)
    {
    }

    public function destroy($id)
    {
        # code...
    }
}
