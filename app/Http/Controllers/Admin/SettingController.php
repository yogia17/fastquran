<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Option;

class SettingController extends Controller
{
    private $cart_times = [
        10 => '10 Menit',
        15 => '15 Menit',
        20 => '20 Menit',
        25 => '25 Menit',
        30 => '30 Menit',
        60 => '1 Jam',
        120 => '2 Jam',
    ];

    private $payment_deadline = [
        10 => '10 Menit',
        15 => '15 Menit',
        20 => '20 Menit',
        25 => '25 Menit',
        30 => '30 Menit',
        60 => '1 Jam',
        120 => '2 Jam',
        300 => '5 Jam',
        720 => '12 Jam',
        1440 => '1 Hari'
    ];

    public function index()
    {
        $payment_deadline = $this->payment_deadline;
        $cart_times = $this->cart_times;
        return view('admin.setting.index', compact('cart_times', 'payment_deadline'));
    }

    public function store(Request $request)
    {
        foreach ($request->all() as $name => $value) {
            if ($name == '_token') continue;
            Option::_set($name, $value, 0);
        }

        return back()->with('success', 'Setting Updated');
    }
}
