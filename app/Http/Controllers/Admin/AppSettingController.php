<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Promotion;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\Auth;

class AppSettingController extends Controller
{
    private $statuses = [
        'active' => 'Active',
        'deactive' => 'Deactive'
    ];
    public function __construct()
    {
        # code...
    }

    public function index()
    {
        $slider = Promotion::where('type', 'slide')->orderBy('id', 'desc')->paginate(10);
        $promo = Promotion::where('type', 'promo')->orderBy('id', 'desc')->paginate(10);
        $info = Promotion::where('type', 'info')->orderBy('id', 'desc')->paginate(10);
        $testimonial = Promotion::where('type', 'testimonial')->orderBy('id', 'desc')->paginate(10);
        $youtube = Promotion::where('type', 'youtube')->orderBy('id', 'desc')->first();
        $popup = Promotion::where('type', 'popup')->orderBy('id', 'desc')->first();
        return view('admin.appsetting.index', compact('slider','promo','info','testimonial','youtube','popup'));
    }

    public function create()
    {
        # code...
    }

    public function store()
    {
        # code...
    }

    public function edit($id)
    {
        $statuses = $this->statuses;
        $item = Promotion::find($id);
        return view('admin.appsetting.edit', compact('statuses', 'item'));
    }

    public function update(Request $request, $id)
    {
        $validated = request()->validate([
            'name'    => 'nullable',
            'subtitle'    => 'nullable',
            'status'    => 'required',
            'image_url' => 'nullable',
            'image_url.*' => 'mimes:jpg,jpeg,png,bmp',
            'brand_id'  => 'nullable',
            'product_id'  => 'nullable',
            'type'      => 'required'
        ]);

        $validated['user_id'] = Auth::id();
        if ($request->hasFile('image_url')) {
            $file = $request->file('image_url');
            $save_path = "home/full/" . time() . $file->getClientOriginalName();
            Storage::disk('s3')->put($save_path, file_get_contents($file), [
                'visibility' => 'public'
            ]);
            $validated['image_url'] = Storage::disk('s3')->url($save_path);
        } else {
            unset($validated['image_url']);
        }
        Promotion::where('id', $id)->update($validated);

        return redirect()->route('admin.appsetting.index')->with('success', 'Updated');
    }

    public function destroy($id)
    {
        # code...
    }
}
