<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CreateVoucherRequest;
use App\Repositories\VoucherRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\Auth;

class VoucherController extends Controller
{
    public function __construct()
    {
        # code...
    }

    public function index()
    {
        $voucher = (new VoucherRepository())->all();
        return view('admin.voucher.voucher', compact('voucher'));
    }

    public function create()
    {

    }

    public function store(CreateVoucherRequest $request)
    {
    }

    public function edit($id)
    {
    }

    public function update(Request $request)
    {
    }

    public function destroy($id)
    {
        # code...
    }
}
