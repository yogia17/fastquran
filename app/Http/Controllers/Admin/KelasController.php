<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Requests\StoreKelasRequest;
use App\Http\Requests\UpdateKelasRequest;
use App\Repositories\KelasRepository;
use App\Repositories\ReorderRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Course;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class KelasController extends Controller
{
    /**
     * @var KelasRepository
     */
    private $kelasRepository;

    public function __construct(KelasRepository $kelasRepository)
    {
        $this->kelasRepository = $kelasRepository;
    }
    public function index(Request $request)
    {
        if($request->ajax()) {
            return datatables($this->kelasRepository->datatable())
                ->addColumn('action','admin.kelas.button')
                ->toJson();
        }
        return view('admin.kelas.index');
    }

    public function create()
    {
        return view('admin.kelas.create');
    }

    public function store(StoreKelasRequest $request)
    {
        $this->kelasRepository->create($request->validated());
        return back()->with(['success' => 'Created']);
    }

    public function edit($id)
    {
        $kelas = $this->kelasRepository->findById($id);
        return view('admin.kelas.edit', compact('kelas'));
    }

    public function update(UpdateKelasRequest $request, $id)
    {
        $this->kelasRepository->update($id, $request->validated());
        return back()->with('success','Updated');
    }

    public function show($id)
    {
        return view('admin.kelas.index');
    }

    public function destroy($id)
    {
        $this->kelasRepository->destroy($id);
        return back()->with('success','Deleted');
    }

    public function push($id)
    {
        $this->kelasRepository->push($id);
        return back()->with('success','Updated');
    }

    public function reorder(Request $request)
    {
        (new ReorderRepository())->reorder($request->positions, Category::class, 'sort_at');
    }
}
