<?php

namespace App\Http\Controllers\Auth;

use App\Exceptions\UserException;
use App\Mail\TestEmail;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\UpdatePasswordRequest;
use App\Notifications\UserActivatedNotification;
use App\Notifications\UserPasswordChangedNotification;
use App\Notifications\UserResetPasswordNotification;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index()
    {
        return view('login');
    }

    public function forceLogin()
    {
        Auth::loginUsingId(1);
        return redirect()->route('admin.kelas.index');
    }

    public function auth(LoginRequest $request)
    {
        if (Auth::attempt($request->only('email', 'password'))) {
            return redirect()->route('student.kelas.index');
            Auth::logout();
            return back()->withErrors(['Permission denied']);
        }
        return back()->withErrors(['Invalid Username or Password']);
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }

    public function resetPassword($token)
    {
        return view('reset-password', ['auth' => $token]);
    }

    public function findBy($column, $value)
    {
        return User::where($column, $value)->get();
    }

    public function resetForm(Request $request)
    {
        if ($request->isMethod('post')) {
            $user = $this->findBy('email', 'anggry.smart@gmail.com')->first();
            if (!$user) {
                return back()->withErrors(['Email tidak terdaftar']);
            }
            $user->notify(new UserResetPasswordNotification($user));
            return redirect('/login')->with(['success' => 'Silakan cek email Anda.']);
        }
        return view('reset-form');

    }

    public function updatePassword(UpdatePasswordRequest $request)
    {
        $userRepository = new UserRepository();

        try {
            $user = $userRepository->findById(decrypt($request->auth));
            (new UserRepository())->updatePassword($user, $request->password);
        } catch (DecryptException $exception) {
            abort(401, 'Whoops');
        }

        return back()->with(['success' => 'Kata sandi Anda berhasil diubah']);
    }
}
