<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FrontController extends Controller
{
    public function index()
    {
        return view('frontend.index');
    }
    public function user(Request $request)
    {

    }

    public function kelas()
    {
        return view('frontend.kelas');
    }
    public function partner()
    {
        return view('frontend.partner.register');
    }

    public function partnerRegister(RegisterRequest $request)
    {
        $request->validated();
        $user = (new UserRepository())->create([
            'name' => $request->name,
            'email' => $request->email,
            'handphone' => $request->handphone,
            'password' => bcrypt($request->password),
            'is_partner' => 1
        ]);
        if ($user) {
            Auth::loginUsingId($user->id);
            return redirect()->route('partner.profile.index');
        }
        return back()->withErrors(["Pendaftaran gagal, silakan coba lagi!"]);

    }
    public function privacyPolicy()
    {
        return view('frontend.privacy_policy');
    }
}
