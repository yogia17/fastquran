<?php

namespace App\Http\Controllers;

use App\User;
use App\Device;
use Auth;
use Socialite;
use Illuminate\Support\Str;
class SessionController extends Controller
{
    private $redirect_url;

    public function __construct()
    {
        $this->redirect_admin_url = 'admin/appsetting';
        $this->redirect_url = '/';
    }

    public function index()
    {
        return view('session.index');
    }

    public function forceLogin($id)
    {
        Auth::loginUsingId($id);

        $redir = (Auth::user()->status == 'admin') ? $this->redirect_admin_url : $this->redirect_url;
        return redirect()->to($redir);
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->to('/');
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();
        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::loginUsingId($authUser->id, true);
        
        if (session()->has("RE_URL")) {
            return redirect(session()->pull("RE_URL"));
        }else{
            $user = Auth::user();
            $device_name = request('device_name', 'Android Test');
            $device_id = request('device_id', Str::random(10));

            $device = Device::firstOrCreate([
                'user_id' => $user->id,
            ], [
                'device_name' => $device_name,
                'device_id' => $device_id,
                'token' => bcrypt(Auth::id() . $device_name . $device_id)
            ]);

            $token = $device->token;
            $redir= url('/').'/webstore/?token='.$token;
            return redirect()->to($redir);
        }
    }
    public function redirects()
    {
        return redirect(url('/').'?email=yogi.anggriawan@live.com');
    }

    public function findOrCreateUser($user, $provider)
    {
        if($provider == 'google'){
            $social = 'google_id';
        }else{
            $social = 'facebook_id';
        }
        /*
        //Step 1: Cek sudah punya akun? berdasarkan email
        //Step 2: Jika sudah, update sosial id
        //Step 3: Cari user berdasarkan email + sosial id
        //Step 4: Jika ketemu return, jika tidak, buat baru
        */

        //Step 1
        $uid = User::where(function($query) use($user) {
            $query->where('email', $user->email);
        })->first();
        //Step 2
        if ($uid) {
            User::where('id', $uid->id)->update([
                $social => $user->id
            ]);
            
        }

        //Step 3 
        $authUser = User::where(function($query) use($user, $social) {
            $query->where($social, $user->id)
                  ->orWhere('email', $user->email);
        })->first();   
        if ($authUser) {
            
            if(!$authUser->avatar){
                User::where('id', $uid->id)->update([
                    'avatar' => $user->getAvatar()
                ]);
            }
            return $authUser;
        }
        //Step 4
        return User::create([
            'name'     => $user->name,
            'email'    => $user->email,
            'avatar'    => $user->getAvatar(),
            $social => $user->id
        ]);
        

    }

}
