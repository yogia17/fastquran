<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $theme_name = 'default';

    protected $error_codes = [
        200 => 'Success',
        201 => 'Pending',
        202 => 'Denied',

        300 => 'Move Permanently',

        400 => 'Validation Error',
        401 => 'Access denied due to unauthorized transaction',
        402 => 'Merchant doesn’t have access for this payment type',
        403 => 'The requested resource is only capable of generating content not acceptable according to the accepting headers that sent in the request',
        404 => 'The requested resource is not found',
        405 => 'HTTP method is not allowed',
        406 => 'Duplicate order ID. Order ID has already been utilized previously',
        407 => 'Expired transaction',
        408 => 'Merchant sends the wrong data type',
        409 => 'Merchant has sent too many transactions for the same card number',
        410 => 'Merchant account is deactivated. Please contact Midtrans support',
        411 => 'Token id is missing, invalid, or timed out',
        412 => 'Merchant cannot modify status of the transaction',
        413 => 'The request cannot be processed due to malformed syntax in the request body',

        500 => 'Internal Server Error',
        501 => 'The feature has not finished yet, it will be available soon',
        502 => 'Internal Server Error: Bank Connection Problem',
        503 => 'Internal Server Error',
        504 => 'Internal Server Error: Fraud detection is unavailable'
    ];

    public function __construct()
    {
        $this->theme_name = config('shop.theme');
    }
}
