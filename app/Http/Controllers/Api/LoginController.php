<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Device;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function login()
    {
        $credentials = request()->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return $this->responseSuccess(Auth::user());
        }

        return $this->responseInvalidLogin();
    }

    private function responseSuccess($user)
    {
        $device_token = $this->getDeviceToken($user);

        return response()->json([
            'status'        => 'success',
            'message'       => 'Login Success',
            'token'         => $device_token,
            'user'          => $user,
            'permissions'   => $user->permissions(),
            'member'        => $user->status
        ], 200);
    }

    private function responseSocialError()
    {
        return response()->json([
            'status' => 'error',
            'message' => 'Invalid authentification'
        ], 401);
    }

    private function responseInvalidLogin()
    {
        return response()->json([
            'status' => 'error',
            'message' => 'Invalid username or password'
        ], 401);
    }

    public function socialLogin(Request $request, $provider)
    {
        $social = $provider == 'google' ? 'google_id' : 'facebook_id';

        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'email' => 'required|email',
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->responseSocialError();
        }

        $user = User::firstOrCreate([
            'email' => $request->email
        ], [
            $social => $request->id,
            'name' => $request->name
        ]);

        // match facebook login id with request
        $user = User::find($user->id);
        if (empty($user->$social)) {
            $user->$social = $request->id;
            $user->save();
        }
        if ($user->$social != $request->id) {
            return $this->responseSocialError();
        }

        return $this->responseSuccess($user);
    }

    private function getDeviceToken($user)
    {
        $device_name = request('device_name', 'Android Test');
        $device_id = request('device_id', Str::random(10));

        $device = Device::firstOrCreate([
            'user_id' => $user->id,
        ], [
            'device_name' => $device_name,
            'device_id' => $device_id,
            'token' => bcrypt(Auth::id() . $device_name . $device_id)
        ]);

        return $device->token;
    }
}
