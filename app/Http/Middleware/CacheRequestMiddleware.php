<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cache;
use function Aws\map;

class CacheRequestMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (request()->method('get')) {
            $key = Str::slug(request()->url());
            return Cache::remember($key, 5, function () use ($next, $request) {
                return $next($request);
            });
        }
        return $next($request);
    }
}
