<?php

namespace App\Http\Middleware;

use App\Device;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\App;

class ApiToken
{
    /**
     * Handle an incoming request
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = request()->header('X-Authorization');
        $app_env = App::environment();

        if ($token) {
            $device = Device::where('token', $token)->first();
            if ($device) {
                Auth::loginUsingId($device->user_id);
            } else {
                return $this->responseTokenError();
            }
        } elseif ($request->has('force_user') && ($app_env == 'local' || $app_env = 'testing')) {
            Auth::loginUsingId($request->force_user);
        } else {
            return $this->responseEmptyToken();
        }

        App::setLocale(Auth::user()->lang);

        $response = $next($request);
        Auth::logout();
        return $response;
    }

    public function responseEmptyToken()
    {
        return response()->json([
            'data' => null,
            'status' => 'error',
            'message' => 'Silahkan login dulu'
        ], 401);
    }

    public function responseTokenError()
    {
        return response()->json([
            'data' => null,
            'status' => 'error',
            'message' => 'Silahkan login dulu'
        ], 401);
    }
}
