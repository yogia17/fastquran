<?php
// /app/Http/Middleware/Cors.php 
namespace App\Http\Middleware;
 
use Closure;
 
class Cors {
    public function handle($request, Closure $next)
    {
        return $next($request)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Credentials', '*')
            ->header('Access-Control-Allow-Methods', '*')
            ->header('Access-Control-Max-Age', '*')
            ->header('Access-Control-Allow-Headers', '*');        
    }

}