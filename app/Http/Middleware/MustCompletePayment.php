<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Order;

class MustCompletePayment
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->inCompletedPayment()) {
            return $this->showJsonError();
        }

        return $next($request);
    }

    public function showJsonError()
    {
        return response()->json([
            'data' => null,
            'message' => 'Kamu harus menyelesaikan pembayaran terlebih dahulu',
            'status' => 'error'
        ], 401);
    }

    /**
     * Mengecek apakah transaksi sebelumnya sudah selesai apa belum
     *
     * @return bool
     */
    public function inCompletedPayment()
    {
        $user_id = Auth::id();

        $lastOrder = Order::where('user_id', $user_id)->orderBy('id', 'desc')->first();

        if (empty($lastOrder)) {
            return false;
        } else {
            if ($lastOrder->transaction_status == 'settlement') {
                return false;
            } elseif ($lastOrder->transaction_status == 'failure') {
                return false;
            } elseif ($lastOrder->transaction_status == 'created') {
                return false;
            }
            return true;
        }
    }
}
