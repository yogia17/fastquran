<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'handphone', 'is_admin', 'is_student', 'is_partner', 'is_teacher','bank_name','bank_no_rekening', 'ktp'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'email_verified_at',
    ];

    public function isStudent()
    {
        return $this->is_student;
    }
    public function isPartner()
    {
        return $this->is_partner;
    }

    public function isAdmin()
    {
        return $this->is_admin;
    }
    public function courses()
    {
        return $this->belongsToMany(Course::class, 'enrollments');
    }
}
