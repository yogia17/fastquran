<?php


namespace App\Mixins;


class ResponseMixins
{
    public function errorJson()
    {
        return function($message = '', $errorCode = 401){
            return \response()->json([
                'message' => $message,
                'status' => 'error'
            ], $errorCode);
        };
    }

    public function errorJsonRaw()
    {
        return function($message, $data = null, $errorCode = 401) {
            $data = array_merge(['message' => $message,'status' => 'success'], $data);
            return \response()->json($data, 401);
        };
    }

    public function successJson()
    {
        return function($message) {
            return \response()->json([
                'message' => $message,
                'status' => 'success'
            ], 200);
        };
    }

    public function successJsonData()
    {
        return function($message, $data = null) {
            return \response()->json([
                'data' => $data,
                'message' => $message,
                'status' => 'success'
            ]);
        };
    }

    public function successJsonRaw()
    {
        return function($message, $data = null) {
            $data = array_merge(['message' => $message,'status' => 'success'], $data);
            return \response()->json($data);
        };
    }
}
