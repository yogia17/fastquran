<?php


namespace App\Mixins;



use Illuminate\Support\Str;

class StrMixins
{
    public function generateOrderId()
    {
        return function($length = 16) {
            return 'HL' . Str::random($length);
        };
    }

    public function generateUniqueCode()
    {
        return function($length = 32){
            return 'HL' . now()->format('Ymd') . Str::random($length);
        };
    }
}
