<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    protected $fillable = [
        'voucher_name','user_id'
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
