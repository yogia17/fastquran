<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // tabel kelas
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->unique();
            $table->string('title');//kode
            $table->text('description');
            $table->string('type');//mini,besar,private,online
            $table->string('level');//tahsin,tartil
            $table->integer('meet');//pertemuan
            $table->integer('price');//harga
            $table->integer('before_price');//sebelum harga real
            $table->string('image');
            $table->json('materi');
            $table->json('fasilitas');
            $table->dateTime('sort_at');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
