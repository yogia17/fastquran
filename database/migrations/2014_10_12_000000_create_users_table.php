<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // table murid/guru
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('handphone')->unique();
            $table->string('password')->nullable();
            $table->integer('is_admin')->default(0);
            $table->integer('is_teacher')->default(0);
            $table->integer('is_student')->default(0);
            $table->integer('is_partner')->default(0);//1=yes,0=no
            $table->integer('is_active')->default(0);//1=active,0=nonactive
            $table->string('bank_name')->nullable();
            $table->string('bank_no_rekening')->nullable();
            $table->string('ktp')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
