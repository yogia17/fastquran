<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->runLocalSeeder();
        
    }

    public function runLocalSeeder()
    {
        $this->call([
            UserTableSeeder::class,
            CourseTableSeeder::class,
            EnrollmentsTableSeeder::class,
            SchedulesTableSeeder::class,
            AttendancesTableSeeder::class,
        ]);
    }
}
