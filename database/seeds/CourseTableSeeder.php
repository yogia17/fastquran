<?php
use App\Course;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CourseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $courses = ['Kelas Mini','Kelas Besar','Kelas Private'];

//        foreach ($courses as $key => $course) {
//            Course::create([
//                'title' => 'FQR1',
//                'type' => $course
//            ]);
//        }

        Course::create([
            'slug' => 'daily-quran',
            'title' => 'DAILY QURAN',
            'description' => '<h4>Serunya Kelas Online</h4><p>Program 30 Hari untuk tingkat kelas Tahsin ini fokus pada perbaikan bacaan yang berkaitan dengan kesempurnaan lafaz pengucapan huruf-huruf Al Quran dan penyempurnaan dalam pengucapan hukum hubungan di antara huruf dengan huruf yang lain.</p><p>Pada kelas online ini, pengajar dari FastQuran akan menggunakan fasilitas Meeting Online, jadi siapkan kuota internetnya ya :)</p>',
            'type' => 'Online',
            'level' => 'Tahsin',
            'meet' => 30,
            'price' => 100000,
            'before_price' => 250000,
            'image' => 'https://fastquran.id/assets/frontend/images/kelas/family.jpg',
            'materi' => json_encode(['Mengenal Huruf','Letak Keluarnya Huruf','Pengucapan Huruf','Ayo diayun','Ayo ditahan']),
            'fasilitas' => json_encode(['Konsultasi Gratis','Private Group','Modul Online','Sertifikat Kelulusan','Kesempatan Mengajar'])
        ]);
    }
}
