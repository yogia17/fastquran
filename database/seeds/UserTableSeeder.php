<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        User::create([
            'name' => 'Yogi',
            'email' => "yogi@tes.com",
            'handphone' => '082312225580',
            'password' => bcrypt('1234'),
            'is_admin' => 1,
            'is_student' => 1
        ]);
    }
}
