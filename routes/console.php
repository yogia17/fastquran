<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

// Artisan::command('update-ongkir', function () {
//     $provinces = (new RajaOngkir)->getProvince();
//     foreach ($provinces as $province) {
//         $cities = (new RajaOngkir)->getCity($province->province_id);
//         foreach ($cities as $city) {
//             $subdistricts = (new RajaOngkir)->getSubdistrict($city->city_id);
//             foreach ($subdistricts as $subdistrict) {
//                 $costs = (new RajaOngkir)->getCost(54, 'city', $subdistrict->subdistrict_id, 'subdistrict', 500, 'sicepat');
//                 \App\Shipping::createCache($subdistrict, $costs);
//                 $this->info("{$subdistrict->province} {$subdistrict->city} {$subdistrict->subdistrict_name}");
//             }
//         }
//     }
// });
