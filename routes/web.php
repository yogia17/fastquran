<?php

Route::get('/privacy', 'FrontController@privacyPolicy');

//AUTH
Route::get('forcelogin', 'Auth\LoginController@forceLogin')->name('forcelogin');
Route::get('login', 'Auth\LoginController@index')->name('login');
Route::post('login', 'Auth\LoginController@auth');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::any('reset-password', 'Auth\LoginController@resetForm')->name('reset_form');
Route::get('reset/{token}', 'Auth\LoginController@resetPassword')->name('reset');
Route::post('update_password', 'Auth\LoginController@updatePassword')->name('update_password');

//REGISTER
Route::get('daftar', 'Student\RegisterController@index')->name('daftar');
Route::post('daftar', 'Student\RegisterController@store');

Route::get('partner', 'FrontController@partner');
Route::post('partner', 'FrontController@partnerRegister');

//MENU
Route::get('/', 'FrontController@index');
Route::post('check_email', 'Front\OrderController@check_email')->name('check_email');
Route::post('order', 'Front\OrderController@order')->name('order');
Route::resource('kelas', 'Front\KelasController');
Route::post('kelas/check_promo_code', 'Front\KelasController@check_promo_code')->name('kelas.check_promo_code');
Route::get('checkout/{slug}', 'Front\KelasController@checkout');

// Student Auth Controller
Route::middleware(['auth'])->prefix('student')->namespace('Student')->name('student.')->group(function () {
    Route::resource('kelas', 'KelasController');
//    Route::get('kelas/{slug}/{lesson}', 'KelasController@lesson');
    Route::resource('lesson', 'LessonController');
});

// Partner Auth Controller
Route::middleware(['auth'])->prefix('partner')->namespace('Partner')->name('partner.')->group(function () {
    Route::resource('profile', 'PartnerController');
    Route::resource('voucher', 'VoucherController');
});

// Admin Auth Controller
Route::redirect('/admin', '/admin/kelas');
Route::middleware(['auth'])->prefix('admin')->namespace('Admin')->name('admin.')->group(function () {
    Route::resource('kelas', 'KelasController');
    Route::post('kelas/reorder','KelasController@reorder')->name('kelas.reorder');
});
