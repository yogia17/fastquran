<?php

Route::middleware('cors')->namespace('Api')->name('api.')->group(function () {
    Route::any('/login', 'LoginController@login');
    Route::post('/register', 'LoginController@doRegister');
    Route::get('/registration/activate/{code}', 'LoginController@activate');
    Route::any('/social/{provider}/login', 'LoginController@socialLogin');
    Route::post('/request/otp', 'AccountController@otp');
});
