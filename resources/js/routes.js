export default routes = [{
        path: '/cart',
        component: require('./components/CartPage.vue').default
    },
    {
        path: '/checkout',
        component: require('./components/CheckoutPage.vue').default
    },
    {
        path: '/products',
        component: require('./components/ProductPage.vue').default
    },
    {
        path: '/product/:id',
        component: require('./components/ProductDetailPage.vue').default
    }, {
        path: '/payment',
        component: require('./components/PaymentPage.vue').default
    }, {
        path: '*',
        component: require('./components/NotFoundPage.vue').default
    }
];
