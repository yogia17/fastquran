require('./bootstrap');

window.Vue = require('vue');

import firebase from 'firebase';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import Vuetify from 'vuetify'
import Framework7 from 'framework7/framework7.esm.bundle.js'
import Framework7Vue from 'framework7-vue/framework7-vue.esm.bundle.js'
Framework7.use(Framework7Vue);

Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(Vuetify);

export const store = new Vuex.Store({
    state: {
        products: [],
        product: null,
        user_token: null,
        dump: null,
    },
    mutations: {
        increment(state) {
            state.count++;
        }
    },
    getters: {
        dontTodos: state => {
            // return state.todo.filter(todo => todo.done);
        }
    },
    actions: {
        increment(context) {
            // context.increment('increment');
        }
    }
});

// routes = require('./routes');
// const routes = new VueRouter({
//     mode: 'history',
//     routes: routes
// });

let auth = document.head.querySelector('meta[name="authorization"]');

if (auth) {
    window.axios.defaults.headers.common['X-Authorization'] = auth.content;
}


var firebaseConfig = {
    apiKey: "AIzaSyB7NF9ccivpy9GYgKdsi23QMfAZN9QSrUA",
    authDomain: "test-2f2f9.firebaseapp.com",
    databaseURL: "https://test-2f2f9.firebaseio.com",
    projectId: "test-2f2f9",
    storageBucket: "test-2f2f9.appspot.com",
    messagingSenderId: "884684492678",
    appId: "1:884684492678:web:cd76a5c715a3c977"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);


Vue.component('page-home', require('./components/PageHome.vue').default);
const app = new Vue({
    el: '#zerina-banu',
    store,
    // routes,
    template: '<page-home></page-home>'
});


// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
