import Framework7 from 'framework7/framework7.esm.bundle.js';
window.$ = window.jQuery = require('jquery');

var app = new Framework7({
    'name': 'Zerina Banu'
});

var update_transaction = true;

var ac1 = app.actions.create({
    buttons: [{
            text: 'Ya, Batalkan transaksi',
            bold: true,
            color: 'red',
            onClick: function () {
                $('.action-delete').html('Updating...');

                var id_transaction = $('#id_transaction').html();
                $.post('/api/transaction/cancel', {
                    id: id_transaction
                }).done(function (response) {
                    app.dialog.alert(response.message);
                    if (response.status == 'success') {
                        update_transaction = false;
                        $('.action-delete').html('Transaksi Dibatalkan');
                    }
                }).fail(function () {
                    app.dialog.alert('Something Wrong');
                    $('.action-delete').html('Batalkan Transaksi');
                });
            }
        },
        {
            text: 'Tidak',
        }
    ]
});

$('.action-delete').click(function (e) {
    e.preventDefault();
    if (!update_transaction) {
        app.dialog.alert('Transaksi dibatalkan');
        return;
    }
    ac1.open();
});

$('.download-efaktur').click(function (e) {
    e.preventDefault();
    window.location = $(this).attr('href');
});
