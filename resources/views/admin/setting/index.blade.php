@extends('admin.layouts')

@section('title','Setting')

@section('header')
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Setting</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ route('admin.dashboard.index') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>
                    Home</a>
                <span class="breadcrumb-item active">Setting</span>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
@section('content')
<div class="card">
    <div class="card-header header-elements-inline bg-white">
        <h5 class="card-title">Setting</h5>
    </div>
    {{ Form::open(['route' => 'admin.setting.store','files' => true]) }}
    <div class="card-body">
        <div class="form-group">
            <label>Harga Potong</label>
            <div class="input-group">
                <span class="input-group-prepend">
                    <span class="input-group-text">Rp</span>
                </span>
                <input type="text" name="cut_price" value="{{ \App\Option::_get('cut_price', 15000) }}"
                    class="form-control" placeholder="">
            </div>
        </div>
        <div class="form-group">

            <label>Ukuran Potong</label>
            <div class="input-group">
                <input type="text" name="cut_range" value="{{ \App\Option::_get('cut_range', 144) }}"
                    class="form-control" placeholder="">

                <span class="input-group-append">
                    <span class="input-group-text">Cm</span>
                </span>
            </div>
        </div>
        <div class="form-group">

            <label>Nama Pengirim</label>
            <div class="input-group">
                <input type="text" name="sender" value="{{ \App\Option::_get('sender', 'ZerinaBanu.com') }}"
                    class="form-control" placeholder="">
            </div>
        </div>
        <div class="form-group">
            <label>Waktu Hapus Keranjang</label>
            {{ Form::select('cart_time', $cart_times, \App\Option::_get('cart_time',15), ['class' => 'form-control']) }}
        </div>

        <div class="form-group">
            <label>Batas Waktu Pembayaran</label>
            {{ Form::select('payment_deadline', $payment_deadline, \App\Option::_get('payment_deadline',15), ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary btn-sm">Update</button>
    </div>
    {{ Form::close() }}
</div>
@endsection

@endsection
