@extends('admin.layout.layout')

@section('title','Kelas')

@section('content')
    @include('admin.layout.validation')

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Kelas</h3>
            <div class="box-tools pull-right">
                <a href="{{ route('admin.kelas.create') }}" class="btn btn-primary btn-sm">Create</a>
            </div>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped datatable">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nama</th>
                        <th style="width: 180px">Action</th>

                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
        <div class="box-footer">
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function () {
            var table = $('.datatable').DataTable({
                rowReorder: {selector: 'tr', update: false, dataSrc: 'id'},
                processing: true,
                ordering: false,
                serverSide: true,
                ajax: {
                    url: "{{ route('admin.kelas.index') }}"
                },
                columns: [
                    {data: 'id'},
                    {data: 'title'},
                    {data: 'action'},
                ],
                columnDefs: [{targets: [0], visible: false}]
            });

            table.on('row-reorder', function (e, diff, edit) {
                var positions = [];
                diff.forEach(function (val, key) {
                    positions.push({'from': val.oldData, 'to': val.newData});
                });
                $.post("{{ route('admin.kelas.reorder') }}", {
                    positions: positions,
                });
            });
        });
    </script>
@endsection
