@extends('admin.layout.layout')

@section('title','Dashboard')

@section('content')
    @include('admin.layout.validation')

    {{ Form::open(['route' => ['admin.category.update', $category->id],'method' => 'PUT']) }}
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Edit Category</h3>
        </div>
        <div class="box-body">
            <div class="form-group">
                <label for="">Category Name</label>
                {{ Form::text('name', $category->name, ['class' => 'form-control','placeholder' => '']) }}
            </div>
        </div>
        <div class="box-footer">
            <a href="{{ route('admin.category.index') }}" class="btn btn-warning btn-sm">&laquo; Back</a>
            <button type="submit" class="btn btn-primary btn-sm">Update</button>
        </div>
    </div>
    {{ Form::close() }}
@endsection
