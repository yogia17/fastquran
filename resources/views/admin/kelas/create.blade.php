@extends('admin.layout.layout')

@section('title','Create Category')

@section('content')
    @include('admin.layout.validation')
    {{ Form::open(['route' => 'admin.category.store']) }}
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Create Category</h3>
        </div>
        <div class="box-body">
            <div class="row col-md-3">
                <div class="form-group">
                    <label for="">Category Name</label>
                    {{ Form::text('name', null, ['class' => 'form-control','placeholder' => '']) }}
                </div>
            </div>
        </div>
        <div class="box-footer">
            <a href="{{ route('admin.category.index') }}" class="btn btn-warning btn-sm">&laquo; Back</a>
            <button type="submit" class="btn btn-primary btn-sm">Create</button>
        </div>
    </div>
    {{ Form::close() }}
@endsection
