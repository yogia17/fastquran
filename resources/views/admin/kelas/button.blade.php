{{ Form::open(['route' => ['admin.kelas.destroy', $id], 'method' => 'DELETE']) }}
<a href="{{ route('admin.kelas.edit', $id) }}" class="btn btn-primary btn-sm">
    <i class="fa fa-pencil" aria-hidden="true"></i>
    Edit
</a>
<button type="submit" class="btn btn-danger btn-sm">
    <i class="fa fa-trash" aria-hidden="true"></i>
    Delete
</button>
{{ Form::close() }}
