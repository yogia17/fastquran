@extends('admin.layouts')

@section('title','Setting')

@section('header')
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Setting</span></h4>
        </div>
    </div>
</div>
@endsection

@section('content')

    <div class="card">
        <div class="card-body">

        <div class="card-title">
                Slider Home -  <a href="{{ route('admin.sliderapp.create') }}?type=slide">Add New</a>
            </div>
        <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Image</th>
                        
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($slider as $key => $item)
                        <tr>
                            <td>{{ $key+1  }}</td>
                            <td>{{ $item->name  }}</td>
                            <td><img src="{{ asset($item->image_url) }}" style="width:32px;height:32px" class="rounded-circle"></td>
                            
                            
                            <td>
                                {{ Form::open(['route' => ['admin.sliderapp.destroy', $item->id], 'method' => 'DELETE'])  }}
                                    <button class="btn btn-danger btn-sm">Delete</button>
                                {{ Form::close() }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $slider->links() }}
        </div>
    </div>
    <div class="card">
        <div class="card-body">

        <div class="card-title">
                Promo -  <a href="{{ route('admin.sliderapp.create') }}?type=promo">Add New</a>
            </div>
        <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Image</th>
                        
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($promo as $key => $item)
                        <tr>
                            <td>{{ $key+1  }}</td>
                            <td>{{ $item->name  }}</td>
                            <td><img src="{{ asset($item->image_url) }}" style="width:32px;height:32px" class="rounded-circle"></td>
                            
                            
                            <td>
                                {{ Form::open(['route' => ['admin.sliderapp.destroy', $item->id], 'method' => 'DELETE'])  }}
                                    <button class="btn btn-danger btn-sm">Delete</button>
                                {{ Form::close() }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $slider->links() }}
        </div>
    </div>
    <div class="card">
        <div class="card-body">

        <div class="card-title">
                Info -  <a href="{{ route('admin.sliderapp.create') }}?type=info">Add New</a>
            </div>
        <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Image</th>
                        
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($info as $key => $item)
                        <tr>
                            <td>{{ $key+1  }}</td>
                            <td>{{ $item->name  }}</td>
                            <td><img src="{{ asset($item->image_url) }}" style="width:32px;height:32px" class="rounded-circle"></td>
                            
                            
                            <td>
                                {{ Form::open(['route' => ['admin.sliderapp.destroy', $item->id], 'method' => 'DELETE'])  }}
                                    <button class="btn btn-danger btn-sm">Delete</button>
                                {{ Form::close() }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $slider->links() }}
        </div>
    </div>
    <div class="card">
        <div class="card-body">

        <div class="card-title">
                Testimonial -  <a href="{{ route('admin.sliderapp.create') }}?type=testimonial">Add New</a>
            </div>
        <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($testimonial as $key => $item)
                        <tr>
                            <td>{{ $key+1  }}</td>
                            <td>{{ $item->name  }}</td>
                            <td><img src="{{ asset($item->image_url) }}" style="width:32px;height:32px" class="rounded-circle"></td>
                            
                            
                            <td>
                                {{ Form::open(['route' => ['admin.sliderapp.destroy', $item->id], 'method' => 'DELETE'])  }}
                                    <button class="btn btn-danger btn-sm">Delete</button>
                                {{ Form::close() }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $slider->links() }}
        </div>
    </div>
    
<div class="card">
        {{ Form::open(['route' => ['admin.sliderapp.update', $youtube->id],'method' => 'PUT','files' => true]) }}
        {{ Form::hidden('type','youtube') }}
        
    <div class="card-header header-elements-inline bg-white">
        <h5 class="card-title">Youtube</h5>
    </div>

    <div class="card-body">
            <div class="form-group">
                    <label>Title</label>
                    {{ Form::text('name', $youtube->name, ['class' => 'form-control','placeholder' => 'Title']) }}
                </div>
            <div class="form-group">
                    <label>Link Youtube</label>
                    {{ Form::text('desc', $youtube->desc, ['class' => 'form-control','placeholder' => 'Link']) }}
                </div>
        {{ Form::file('image_url', ['class' => 'dropify','data-default-file'=>$youtube->image_url,'data-max-file-size'=>'100M','data-allowed-file-extensions'=>'jpg png jpeg']) }}
    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary btn-sm">Update</button>
    </div>
    {{ Form::close() }}
</div>
<div class="card">
        {{ Form::open(['route' => ['admin.sliderapp.update', $popup->id],'method' => 'PUT','files' => true]) }}
        {{ Form::hidden('type','popup') }}
        
    <div class="card-header header-elements-inline bg-white">
        <h5 class="card-title">Popup Intro</h5>
    </div>

    <div class="card-body">
            
            <div class="form-group">
                    <label>Title</label>
                    {{ Form::text('name', $popup->name, ['class' => 'form-control','placeholder' => 'Title']) }}
                </div>
            <div class="form-group">
                    <label>Description</label>
                    {{ Form::textarea('desc', $popup->desc, ['class' => 'form-control','id' => 'summernote','placeholder' => 'Description']) }}
                </div>
                <div class="row">
                <div class="col-md-4">
						<div class="card card-body border-top-warning text-center">
							<h6 class="m-0 font-weight-semibold">Background Colors</h6>

							<div class="d-inline-block">
                            <input name="background" type="text" class="form-control colorpicker-show-input" data-preferred-format="hex" value="{{explode('|',$popup->image_url)[0]}}" data-fouc>
							</div>
						</div>
					</div>
                <div class="col-md-4">
						<div class="card card-body border-top-warning text-center">
							<h6 class="m-0 font-weight-semibold">Text Colors </h6>

							<div class="d-inline-block">
								<input name="text" type="text" class="form-control colorpicker-show-input" data-preferred-format="hex" value="{{explode('|',$popup->image_url)[1]}}" data-fouc>
							</div>
						</div>
					</div>
					</div>
            </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary btn-sm">Update</button>
    </div>
    {{ Form::close() }}
</div>

@endsection

@section('script')
<script type="text/javascript">
    $(function () {
        $('.dropify').dropify();
    });

</script>
    <script>
        $(function(){
            $('#summernote').summernote();
                    // Display color formats
        $('.colorpicker-show-input').spectrum({
            showInput: true
        });
        });
    </script>
@endsection
