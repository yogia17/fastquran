@extends('admin.layouts')

@section('title','Edit Home Landing')

@section('content')
    <div class="card">
        {{ Form::open(['route' => ['admin.appsetting.update', $item->id],'method' => 'PUT','files' => true]) }}
        {{ Form::hidden('type','homelanding') }}
            <div class="card-body">
                <div class="card-title">Edit Home Landing</div>
                @include('admin.validation')
                <div class="form-group"  style="display:none">
                    <label>Name</label>
                    {{ Form::text('name', "homelanding", ['class' => 'form-control','placeholder' => 'Promotion Name']) }}
                </div>
                <div class="form-group"  style="display:none">
                    <label>Status</label>
                    {{ Form::text('status', "active") }}
                </div><div class="form-group">
                    <label>Image</label>
                    <div class="col-3">
                    {{ Form::file('image_url', ['class' => 'dropify','data-default-file'=>$item->image_url,'data-max-file-size'=>'100M','data-allowed-file-extensions'=>'jpg png jpeg']) }}
                    </div>
                </div>
                <div class="form-group" style="display:none">
                    <label>Page</label>
                    {{ Form::text('page_url', $item->page_url, ['class' => 'form-control','placeholder' => 'Page Url']) }}
                </div>

            </div>
            <div class="card-footer">
                <a class="btn btn-warning btn-sm" href="{{ route('admin.appsetting.index') }}">&laquo; Kembali</a>
                <button type="submit" class="btn btn-primary btn-sm">Update</button>
            </div>
        {{ Form::close() }}
    </div>
@endsection


@section('script')
<script type="text/javascript">
    $(function () {
        $('.dropify').dropify();
    });

</script>
@endsection
