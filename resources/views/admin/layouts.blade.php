<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>@yield('title')</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('layouts/admin/global_assets/css/icons/icomoon/styles.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('layouts/admin/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('layouts/admin/assets/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('layouts/admin/assets/css/layout.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('layouts/admin/assets/css/components.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('layouts/admin/assets/css/colors.min.css') }}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<link href="{{ asset('layouts/admin/global_assets/css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('layouts/admin/assets/css/dropify.min.css') }}" rel="stylesheet" >

	<!-- Core JS files -->
	<script src="{{ asset('layouts/admin/global_assets/js/main/jquery.min.js') }}"></script>
	<script src="{{ asset('layouts/admin/global_assets/js/main/bootstrap.bundle.min.js') }}"></script>
	<script src="{{ asset('layouts/admin/global_assets/js/plugins/loaders/blockui.min.js') }}"></script>
	<script src="{{ asset('layouts/admin/global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="{{ asset('layouts/admin/assets/js/app.js') }}"></script>
	<script src="https://cdn.jsdelivr.net/npm/countup@1.8.2/dist/countUp.min.js"></script>
	<script src="{{ asset('layouts/admin/global_assets/js/plugins/forms/styling/switch.min.js') }}"></script>
	<script src="{{ asset('layouts/admin/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
	<script src="{{ asset('layouts/admin/global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
	<script src="{{ asset('layouts/admin/global_assets/js/plugins/uploaders/dropify/dropify.min.js') }}"></script>
	<script src="{{ asset('layouts/admin/global_assets/js/plugins/ui/moment/moment.min.js') }}"></script>
	<script src="{{ asset('layouts/admin/global_assets/js/plugins/pickers/daterangepicker.js') }}"></script>
	<script src="{{ asset('layouts/admin/global_assets/js/plugins/forms/tags/tagsinput.min.js') }}"></script>
	<script src="{{ asset('layouts/admin/global_assets/js/plugins/forms/tags/tokenfield.min.js') }}"></script>
	<script src="{{ asset('layouts/admin/global_assets/js/plugins/uploaders/dropzone.min.js') }}"></script>
	<script src="{{ asset('layouts/admin/global_assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
	<script src="{{ asset('layouts/admin/global_assets/js/plugins/pickers/color/spectrum.js') }}"></script>
	<script src="{{ asset('layouts/admin/assets/js/jquery.inputmask.bundle.js') }}"></script>
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<script>
		$(() => {
			$.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
            });
		});
    </script>
    @yield('head')

</head>

<body>
	<!-- Main navbar -->
	<div class="navbar navbar-expand-md navbar-dark">
		<div class="navbar-brand">
			<a href="{{ url('admin') }}" class="d-inline-block">
				<img src="{{ asset('logo_zb.png') }}" alt="">
			</a>
		</div>

		<div class="d-md-none">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
				<i class="icon-tree5"></i>
			</button>
			<button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
				<i class="icon-paragraph-justify3"></i>
			</button>
		</div>

		<div class="collapse navbar-collapse" id="navbar-mobile">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
						<i class="icon-paragraph-justify3"></i>
					</a>
				</li>
			</ul>

			<ul class="navbar-nav ml-auto">
				{{-- <li class="nav-item">
					<a href="#" class="navbar-nav-link">
						Text link
					</a>
				</li>

				<li class="nav-item dropdown">
					<a href="#" class="navbar-nav-link">
						<i class="icon-bell2"></i>
						<span class="d-md-none ml-2">Notifications</span>
						<span class="badge badge-mark border-white ml-auto ml-md-0"></span>
					</a>
				</li> --}}

				<li class="nav-item dropdown dropdown-user">
					<a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
						{{-- <img src="../../../../global_assets/images/image.png" class="rounded-circle mr-2" height="34" alt=""> --}}
						<span>{{ ucwords(Auth::user()->name) }}</span>
					</a>

					<div class="dropdown-menu dropdown-menu-right">
						<a href="#" class="dropdown-item"><i class="icon-user-plus"></i> My profile</a>
						{{-- <a href="#" class="dropdown-item"><i class="icon-coins"></i> My balance</a> --}}
						{{-- <a href="#" class="dropdown-item"><i class="icon-comment-discussion"></i> Messages <span class="badge badge-pill bg-blue ml-auto">58</span></a> --}}
						<div class="dropdown-divider"></div>
						<a href="#" class="dropdown-item"><i class="icon-cog5"></i> Account settings</a>
						<a href="{{ route('logout') }}" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page content -->
	<div class="page-content">

		<!-- Main sidebar -->
		<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

			<!-- Sidebar mobile toggler -->
			<div class="sidebar-mobile-toggler text-center">
				<a href="#" class="sidebar-mobile-main-toggle">
					<i class="icon-arrow-left8"></i>
				</a>
				Navigation
				<a href="#" class="sidebar-mobile-expand">
					<i class="icon-screen-full"></i>
					<i class="icon-screen-normal"></i>
				</a>
			</div>
			<!-- /sidebar mobile toggler -->


			<!-- Sidebar content -->
			<div class="sidebar-content">

				<!-- User menu -->
				<div class="sidebar-user">
					{{-- <div class="card-body">
						<div class="media">
							<div class="mr-3">
								<a href="#"><img src="../../../../global_assets/images/image.png" width="38" height="38" class="rounded-circle" alt=""></a>
							</div>

							<div class="media-body">
								<div class="media-title font-weight-semibold">Victoria Baker</div>
								<div class="font-size-xs opacity-50">
									<i class="icon-pin font-size-sm"></i> &nbsp;Santa Ana, CA
								</div>
							</div>

							<div class="ml-3 align-self-center">
								<a href="#" class="text-white"><i class="icon-cog3"></i></a>
							</div>
						</div>
					</div> --}}
				</div>
				<!-- /user menu -->

				<!-- Main navigation -->
				<div class="card card-sidebar-mobile">
					<ul class="nav nav-sidebar" data-nav-type="accordion">

						<!-- Main -->
						<li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>
						<li class="nav-item">
							<a href="{{ route('admin.course.index') }}" class="nav-link {{ request()->is('admin/course*') ? 'active' : '' }}"><i class="fa fa-users" aria-hidden="true"></i><span>Kelas</span></a>
						</li>
						<li class="nav-item">
							<a href="{{ route('admin.murid.index') }}" class="nav-link {{ request()->is('admin/murid*') ? 'active' : '' }}"><i class="fa fa-users" aria-hidden="true"></i><span>Murid</span></a>
						</li>
						<li class="nav-item">
							<a href="{{ route('admin.guru.index') }}" class="nav-link {{ request()->is('admin/guru*') ? 'active' : '' }}"><i class="fa fa-users" aria-hidden="true"></i><span>Guru</span></a>
						</li>
						<li class="nav-item nav-item-submenu {{ request()->is(['admin/setting*', 'admin/appsetting*'],['admin/terms*']) ? 'nav-item-expanded nav-item-open' : '' }}">
							<a href="#" class="nav-link"> <i class="icon-price-tags"></i> <span>Settings</span></a>
							<ul class="nav nav-group-sub" data-submenu-title="Products">
                               <li class="nav-item"><a href="{{ route('admin.appsetting.index') }}" class="nav-link {{ request()->is('admin/appsetting*') ? 'active' : '' }}">Apps</a></li>
							</ul>
                        </li>
						<!-- /main -->
					</ul>
				</div>
				<!-- /main navigation -->
			</div>
			<!-- /sidebar content -->
		</div>
		<!-- /main sidebar -->

		<!-- Main content -->
		<div class="content-wrapper">
			<!-- Page header -->
			@yield('header')
			<!-- /page header -->

			<!-- Content area -->
			<div class="content">
				@include('admin.validation')
				@yield('content')
			</div>
			<!-- /content area -->

			<!-- Footer -->
			<div class="navbar navbar-expand-lg navbar-light">
				<div class="text-center d-lg-none w-100">
					<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
						<i class="icon-unfold mr-2"></i>
						Footer
					</button>
				</div>
			</div>
			<!-- /footer -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->
	<script>
		$(function(){
			$(".bs-onoff").bootstrapSwitch({});
			$('.select2').select2({});
		});
	</script>
	@yield('script')
</body>
</html>
