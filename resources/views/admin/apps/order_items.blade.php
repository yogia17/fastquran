<!DOCTYPE html>
<html lang="en" class="ios device-pixel-ratio-2 device-retina device-desktop device-macos">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Detail Order</title>
    <script src="{{ mix('assets/js/order.js') }}"></script>
    <link rel="stylesheet" href="{{ mix('assets/css/order.css') }}">
</head>
<body>
    <div id="app" class="framework7-root">
        <div class="view view-main">
            <div class="page">
                <div class="page-content">
                    {{-- card items --}}
                    <div class="card">
                        <div class="card-header">
                            Items
                        </div>
                        <div class="card-content">
                            <div class="list media-list">
                                <ul>
                                    @foreach($carts as $item)
                                    <div class="item-content">
                                        <div class="item-media">
                                            <img src="{{ $item->image() }}" style="width:44px;height:66px" class="lazy">
                                        </div>
                                        <div class="item-inner">
                                            <div class="item-title-row">
                                                <div class="item-title">{{ $item->product->name }} ({{ $item->product->brand->name }})</div>
                                                @if($item->product->preorder == 1)
                                                    <div class="item-after">
                                                        <span class="badge">PO</span>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="item-subtitle">
                                                {{ $item->sku->getModel() }} x {{ $item->qty }}
                                                @if($item->product->preorder == 1)
                                                    <div style="float:right">
                                                        <small>
                                                            Dikirim {{ $item->product->preorder_date->format('d M y') }}
                                                        </small>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="item-subtitle">
                                                @php
                                                    $cut_p = $item->cut ? $item->cut * \App\Option::_get('cut_price') : 0;
                                                @endphp
                                                Rp. {{ number_format($item->sku->price_sale,0,'.','.') }}
                                                @if($item->cut)
                                                    <small>(+cut Rp. {{ number_format($cut_p,0,'.','.') }})</small>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    {{-- end: card items --}}
                </div>
            </div>
        </div>
    </div>
</body>