<!DOCTYPE html>
<html lang="en" class="ios device-pixel-ratio-2 device-retina device-desktop device-macos">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Detail Order</title>

    <link rel="stylesheet" href="{{ mix('assets/css/order.css') }}">
</head>

<body>
    <div id="app" class="framework7-root">
        <div class="view view-main">
            <div class="page">
                <div class="page-content">
                    {{-- <div class="block">
                        <div class="row">
                            <a href="{{ route('efaktur', encrypt($order->id)) }}" class="button button-fill color-blue download-efaktur">
                                Download eFaktur
                            </a>
                        </div>
                    </div> --}}
                    <div class="block-title">
                        No. Pemesanan <span style="float:right" id="id_transaction">{{ $order->order_id }}</span>
                    </div>
                    {{-- card nomor va / cstore --}}
                    @if($order->va_number && $order->transaction_status == 'pending')
                        <div class="card">
                            <div class="card-header">Metode Pembayaran</div>
                            <div class="card-footer">
                                <span>Nomor Virtual Account</span>
                                <span>
                                    <span style="display:none;color:green" id="copied">Copied!</span>
                                    <a href="#" id="va" onclick="copyme()">
                                        {{ $order->va_number }}
                                    </a>
                                </span>
                            </div>
                            <div class="card-footer">
                                <span>Total Pembayaran</span>
                                <span>Rp. {{ number_format($order->gross_amount,0,'.','.') }}</span>
                            </div>
                        </div>
                        @if($order->payment_method)
                        <div class="card">
                            <div class="card-header">Cara Pembayaran</div>
                            <div class="card-content card-content-padding">
                                @foreach(\App\Payment\PaymentMethod::describe($order->payment_method) as $method)
                                <strong>{{ $method['title'] }}</strong>
                                <p>{!! str_replace('|',"<br>",$method['content']) !!}</p>
                                @endforeach
                            </div>
                        </div>
                        @endif
                    @endif
                    {{-- end : card nomor va / cstore --}}
                    {{-- card belanja --}}
                    <div class="card">
                        <div class="card-header">Belanja</div>
                        <div class="card-content">
                            <div class="list media-list">
                                <ul>
                                    @foreach($order->orderItems as $item)
                                    <li class="item-content">
                                        <div class="item-media">
                                            <img src="{{ $item->image() }}" style="width:44px;height:66px" class="lazy">
                                        </div>
                                        <div class="item-inner">
                                            <div class="item-title-row">
                                                <div class="item-title">{{ $item->name }}
                                                    ({{ $item->product->brand->name }})</div>
                                                @if($item->product->preorder == 1)
                                                <div class="item-after">
                                                    <span class="badge">PO</span>
                                                </div>
                                                @endif
                                            </div>
                                            <div class="item-subtitle">
                                                {{ $item->model }} x {{ $item->qty }}
                                                @if($item->product->preorder == 1)
                                                <div style="float:right">
                                                    <small>
                                                        Dikirim {{ $item->product->preorder_date->format('d M y') }}
                                                    </small>
                                                </div>
                                                @endif
                                            </div>
                                            <div class="item-subtitle">
                                                @php
                                                $cut_p = $item->cut ? $item->cut * \App\Option::_get('cut_price') : 0;
                                                @endphp
                                                Rp. {{ number_format($item->price - $cut_p,0,'.','.') }}
                                                @if($item->cut)
                                                <small>(+cut Rp. {{ number_format($cut_p,0,'.','.') }})</small>
                                                @endif
                                            </div>
                                        </div>
                                    </li>
                                    @php
                                    $total = $total ?? 0;
                                    $total += $item->price;
                                    @endphp
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="card-footer"> <span>Total</span><span>Rp.
                                {{ number_format($total,0,',','.') }}</span></div>
                    </div>
                    {{-- end card belanja --}}

                    <div class="card">
                        <div class="card-header">Pengiriman</div>
                        <div class="card-footer"> <span>Kurir</span><span><img
                                    src="{{ asset('assets/images/logo/sicepat.png') }}" alt=""
                                    style="width:80px"></span></div>
                        <div class="card-footer"> <span>Layanan</span><span>{{ $order->shipping_service }}</span></div>
                        <div class="card-footer"> <span>Durasi Pengiriman</span><span>{{ $order->shipping_etd }}
                                Hari</span></div>
                        <div class="card-footer"> <span>Ongkos Kirim</span><span>Rp.
                                {{ number_format($order->shipping_cost,0,'.','.') }}</span></div>
                    </div>

                    <div class="card">
                        <div class="card-header">Informasi Pembayaran</div>
                        <div class="card-footer"> <span>Subtotal Produk</span><span> Rp.
                                {{ number_format($total,0,'.','.') }} </span></div>
                        <div class="card-footer"> <span>Pengiriman - SiCepat
                                ({{ $order->shipping_service }})</span><span> Rp.
                                {{ number_format($order->shipping_cost,0,'.','.') }} </span></div>
                        <div class="card-footer"> <span>Discount</span><span> Rp.
                                {{ number_format(-1 * $order->discount,0,'.','.') }} </span></div>
                        <div class="card-footer"> <span>Total</span><span> Rp.
                                {{ number_format($total + $order->shipping_cost - $order->discount,0,'.','.') }} </span>
                        </div>

                    </div>

                    <div class="card">
                        <div class="card-header">Penerima</div>
                        <div class="card-content card-content-padding">
                            <p><strong>{{ $order->ship_first_name }} ({{ $order->ship_phone }})</strong></p>
                            <p>
                                {{ $order->ship_address }}
                                {{ $order->ship_subdistrict }},
                                {{ $order->ship_city }},
                                {{ $order->ship_province }}
                            </p>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">Pelacakan Pengiriman</div>
                        @empty($order->waybill)
                        <div class="card-footer"> <span>Menunggu Resi</span></div>
                        @else
                        @foreach($manifest as $item)
                        <div class="card-content card-content-padding" style="padding-top:3px;padding-bottom:3px">
                            <p>
                                {{ $item->manifest_description }} <br>
                                <small>{{ $item->manifest_date }} {{ $item->manifest_time }}</small>
                            </p>
                        </div>
                        @endforeach
                        @endempty
                    </div>

                    @if($order->va_number && $order->transaction_status == 'pending')
                        <div class="list inset">
                            <ul>
                                <li><a href="#" class="button button-fill color-red action-delete">Batalkan Transaksi</a></li>
                            </ul>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
    <script src="{{ mix('assets/js/order.js') }}"></script>
    <script>
        function copyme() {
            var range = document.createRange();
            range.selectNode(document.getElementById("va"));
            window.getSelection().removeAllRanges(); // clear current selection
            window.getSelection().addRange(range); // to select text
            document.execCommand("copy");
            window.getSelection().removeAllRanges(); // to deselect
            $('#copied').fadeIn(200).fadeOut(2000);
        }
    </script>
</body>
