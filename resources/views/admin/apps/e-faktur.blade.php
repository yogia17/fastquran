<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title>Zerina Banu</title>
    <style>
        /* .invoice-box {
            max-width: 800px;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, .15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        } */

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr td:nth-child(2) {
            text-align: right;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }

        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
        }

        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.item td {
            border-bottom: 1px solid #eee;
        }

        .invoice-box table tr.item.last td {
            border-bottom: none;
        }

        .invoice-box table tr.total td:nth-child(2) {
            border-top: 2px solid #eee;
            font-weight: bold;
        }

        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }

        /** RTL **/
        .rtl {
            direction: rtl;
            font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }

        .rtl table {
            text-align: right;
        }

        .rtl table tr td:nth-child(2) {
            text-align: left;
        }

    </style>
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title">
                                <img src="{{ asset('assets/images/logo/zb-black.png') }}"
                                    style="width:auto; max-width:300px;max-height:100px">
                            </td>

                            <td>
                                Order ID #: {{ $order->order_id }}<br>
                                Created: {{ $order->created_at }}<br>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                {{ $order->ship_first_name}} <small>({{ $order->ship_phone }})</small><br>
                                {{ $order->ship_address }}<br>
                                {{ $order->ship_subdistrict }}, {{ $order->ship_city }},
                                {{ $order->ship_province }}
                            </td>

                            <td>
                                Zerina Banu<br>
                                Karawang<br>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>


            <tr class="heading">
                <td>
                    Item
                </td>

                <td>
                    Price
                </td>
            </tr>

            @foreach($order->orderItems as $item)
            <tr class="item">
                <td>
                    {{ $item->name }}
                    <small>
                        {{ $item->model }}
                        @if($item->cut)
                        cut {{ $item->cut_notes }}
                        @endif
                    </small>
                </td>

                <td>
                    Rp. {{ number_format($item->price,0,'.','.') }}
                </td>
            </tr>
            @endforeach

            <tr class="item last">
                <td>
                    Biaya Pengiriman
                </td>

                <td>
                    Rp. {{ number_format($order->shipping_cost,0,'.','.') }}
                </td>
            </tr>


            <tr class="total">
                <td></td>

                <td>
                    Total: Rp. {{ number_format($order->gross_amount,0,'.','.') }}
                </td>
            </tr>
        </table>
    </div>
</body>

</html>
