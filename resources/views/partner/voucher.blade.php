@extends('partner.layout.layout')

@section('title','Voucher - FastQuran')
@section('script')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/bootstrap-tokenfield.min.css">
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/bootstrap3-wysihtml5.all.min.js"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/bootstrap-tokenfield.min.js"></script>
    <script>
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5();
            $('.select2').select2();
            $('#color').tokenfield();
            $('#size').tokenfield();
        })
    </script>
@endsection
@section('content')

    @include('partner.layout.validation')
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Voucher</h3>
                    <div class="box-tools pull-right">
                        <a href="{{ route('partner.voucher.create') }}" class="btn btn-primary btn-sm">Voucher Baru</a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Kode Voucher</th>
                        </tr>
                        @foreach($voucher as $key => $item)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td><code>{{strtoupper($item->voucher_name)}}</code></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
            <!-- /.box -->

        </div>

    </div>

@endsection
