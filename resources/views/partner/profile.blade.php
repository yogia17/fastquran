@extends('partner.layout.layout')

@section('title','Partner - FastQuran')
@section('script')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/bootstrap-tokenfield.min.css">
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/bootstrap3-wysihtml5.all.min.js"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/bootstrap-tokenfield.min.js"></script>
    <script>
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5();
            $('.select2').select2();
            $('#color').tokenfield();
            $('#size').tokenfield();
        })
    </script>
@endsection
@section('content')

    @include('partner.layout.validation')
    {{ Form::open(['route' => ['partner.profile.update',999],'files' => 'true','method' => 'PUT']) }}

    <div class="row">
        <div class="col-lg-12 col-sm-12">
            <div class="box">
                <div class="box-header with-border ">
                    <h3 class="box-title">Lengkapi Data Anda</h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label for="">Nama Bank</label>
                        {{ Form::text('bank_name', $partner->bank_name, ['class' => 'form-control','placeholder' => 'Nama Bank...']) }}
                    </div>
                    <div class="form-group">
                        <label for="">No. Rekening</label>
                        {{ Form::text('bank_no_rekening', $partner->bank_no_rekening, ['class' => 'form-control','placeholder' => 'No Rekening...']) }}
                    </div>
                    <div class="form-group">
                        <label for="">Foto KTP</label>
                        {{ Form::file('ktp', null, ['class' => 'form-control','placeholder' => '']) }}
                        @if($partner->ktp)
                            <br>
                            <img width="100" src="{{$partner->ktp}}">
                        @endif
                    </div>
                    <div class="checkbox">
                        <label>
                            <input name="agreement" {{$partner->bank_no_rekening>0?'checked ':''}}type="checkbox">
                            Dengan ini saya menyetujui syarat dan ketentuan yang berlaku di FastQuran
                        </label>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}

@endsection
