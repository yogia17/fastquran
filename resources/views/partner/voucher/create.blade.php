@extends('partner.layout.layout')

@section('title','Voucher')
@section('script')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/bootstrap-tokenfield.min.css">
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/bootstrap3-wysihtml5.all.min.js"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/bootstrap-tokenfield.min.js"></script>
    <script>
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5();
            $('.select2').select2();
            $('#color').tokenfield();
            $('#size').tokenfield();
        })
        $(document).ready(function(){
            $('.voucher').keyup(function(){
                $(this).val($(this).val().toUpperCase());
            });
        });
    </script>
@endsection
@section('content')

    @include('partner.layout.validation')

    <div class="row">
        <div class="col-lg-6 col-sm-6">
            {{ Form::open(['route' => 'partner.voucher.store']) }}
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Voucher Baru</h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label for="">Kode Voucher</label>
                        {{ Form::text('voucher_name', null, ['class' => 'form-control voucher','placeholder' => 'Kode Voucher...']) }}
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-sm btn-primary">Simpan</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@endsection
