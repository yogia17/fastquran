<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="id" xml:lang="id">

<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title','Fastquran.id')</title>
    <!-- Main theme -->
    <link
        href="https://fonts.googleapis.com/css?family=Merriweather:300,400,700|Montserrat:300,400,500,700&display=swap"
        rel="stylesheet">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css"> -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{url('assets/frontend')}}/css/front.css">

</head>

<body>

<section
    @if(Route::current()->getName() == 'kelas.show')class="bg-indigo bg-kelas-detail" style="height: 600px;"
    @else
    class="bg-indigo" style="height: 450px;"
    @endif >
    <nav class="navbar navbar-bwa navbar-expand-lg navbar-dark">
        <div class="container">
            <a class="navbar-brand" href="{{url('')}}">
                <span class="sr-only">Fastquran.id</span>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse navbar-mobile-bwa" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item ">
                        <a class="nav-link" href="{{url('')}}">Home</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{url('')}}/info">Info</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link active" href="{{url('')}}/kelas">Kelas</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{url('')}}/aplikasi">Aplikasi</a>
                    </li>
                    <li class="nav-item">
                        <span class="nav-divider"></span>
                    </li>
                    @if(auth()->check())
                        <li class="nav-item dropdown dropdown-front">
                            <img src="{{url('')}}/assets/student/images/default-avatar.svg" height="40"
                                 alt="">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Akun Saya
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{url('student/kelas')}}">Kelas Saya</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{route('logout')}}">Logout</a>
                            </div>
                        </li>
                    @else
                        <li class="nav-item ">
                            <a class="nav-link" target="_blank" href="{{route('daftar')}}">Daftar</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link btn btn-masuk-bwa" target="_blank" href="{{route('login')}}">Masuk</a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    <main>
        @yield('header')
    </main>
</section>
@yield('content')
<section class="footer my-5 border-top mt-7">
    <div class="container mt-5 pt-4">
        <div class="row justify-content-center align-items-center mt-5 pt-4">
            <div class="col-auto text-gray-500 font-weight-light">© 2019 - 2020 FastQuran • All rights
                reserved •
                Yogyakarta
            </div>
        </div>
    </div>
</section>

<script src="{{url('assets/frontend')}}/scripts/jquery-latest.min.js" type="text/javascript"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="{{url('assets/frontend')}}/scripts/bootstrap.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/simplebar@latest/dist/simplebar.min.js"></script>

<script src="{url('assets/frontend')}}/scripts/courses.js"></script>

<script src="{{url('assets/frontend')}}/scripts/main.js" type="text/javascript"></script>
<script src="{{url('assets/frontend')}}/scripts/lightbox.js" type="text/javascript"></script>
@yield('script')

</body>

</html>
