
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Fastquran</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <style>
        body {
            background: #fff;
            color: #72545e;
            font-family: 'Roboto', sans-serif;
        }

        .container {
            /*padding-top: 30px;*/
            max-width: 1024px;
        }

        .header {
            text-align: center;
        }

        .content {
            /*padding: 20px;*/
        }

        h1 {
            font-size: 24px;
            text-align: center;
            padding-bottom: 20px;
        }

        h3 {
            font-size: 20px;
        }

        p {
            text-align: justify;
        }


        .logo {
            max-width: 100px;
        }

        .btn-outline-primary {
            color: #72545e;
            border-color: #72545e;
        }

        .btn-outline-primary:hover {
            color: #fff;
            border-color: #fff;
            background: #72545e;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="header" style="text-align: center">

    </div>
    <div class="content">
        <h1>Kebijakan Privasi</h1>

        <p>Kebijakan Privasi Fastquran</p>

        <p>Kebijakan Privasi ini menjelaskan bagaimana informasi pribadi Anda dikumpulkan, digunakan, dan dibagikan
            ketika Anda mengunjungi atau melakukan pembelian dari https://Fastquran.id ("Situs").</p>

        <p>INFORMASI PRIBADI YANG KAMI KUMPULKAN<br>
            Ketika Anda mengunjungi Situs ini, kami secara otomatis mengumpulkan informasi tertentu tentang perangkat
            Anda, termasuk informasi tentang browser web Anda, alamat IP, zona waktu, dan beberapa cookie yang diinstal
            pada perangkat Anda. Selain itu, ketika Anda menelusuri Situs, kami mengumpulkan informasi tentang halaman
            web &nbsp;atau produk yang Anda lihat, situs web atau kata kunci pencarian apa yang merujuk Anda ke Situs,
            dan informasi tentang bagaimana Anda berinteraksi dengan Situs. Kami mengacu pada informasi yang dikumpulkan
            secara otomatis ini sebagai "Informasi Perangkat / Device Information".</p>

        <p>Kami mengumpulkan Informasi Perangkat menggunakan teknologi berikut:<br>
            - “Cookies” &nbsp;adalah file data yang ditempatkan di perangkat atau komputer Anda dan sering kali
            menyertakan pengenal unik anonim. Untuk informasi lebih lanjut tentang cookie, dan cara menonaktifkan
            cookie, kunjungi http://www.allaboutcookies.org.<br>
            - “Log files” melacak tindakan yang terjadi di Situs, dan mengumpulkan data termasuk alamat IP Anda, jenis
            peramban, penyedia layanan Internet, halaman yang Mereferensikan / keluar, dan &nbsp;tanggal / waktu.<br>
            - “Web beacons”, “tags”, dan “pixels” adalah file elektronik yang digunakan untuk mencatat informasi tentang
            bagaimana Anda menelusuri Situs.</p>

        <p>Selain itu ketika Anda melakukan pembelian atau mencoba melakukan pembelian melalui Situs, kami mengumpulkan
            informasi tertentu dari Anda, termasuk nama Anda, alamat penagihan, alamat pengiriman, informasi pembayaran
            (termasuk nomor kartu kredit dan Rekening Bank), alamat email, dan nomor telepon . Kami menyebut informasi
            ini sebagai "Informasi Pesanan / Order Information".</p>

        <p>Ketika kita berbicara tentang "Informasi Pribadi" dalam Kebijakan Privasi ini, kita berbicara baik tentang
            Informasi Perangkat ( Device Information ) dan Informasi Pesanan ( Order Information ).</p>

        <p>BAGAIMANA KAMI MENGGUNAKAN INFORMASI PRIBADI ANDA?<br>
            Kami menggunakan Informasi Pesanan yang kami kumpulkan secara umum untuk memenuhi pesanan apa pun yang
            ditempatkan melalui Situs (termasuk memproses informasi pembayaran Anda, mengatur pengiriman, dan memberikan
            Anda Invoice dan / atau konfirmasi pemesanan). Selain itu, kami menggunakan Informasi Pesanan ini untuk:<br>
            - Berkomunikasi Dengan Anda<br>
            - Menyaring pesanan kami untuk potensi risiko atau penipuan; dan<br>
            - Ketika sejalan dengan preferensi yang Anda bagikan kepada kami, berikan Anda informasi atau iklan yang
            berkaitan dengan produk atau layanan kami.</p>

        <p>Kami menggunakan Informasi Perangkat yang kami kumpulkan untuk membantu kami menyaring potensi risiko dan
            penipuan (khususnya, alamat IP Anda), dan lebih umum untuk meningkatkan dan mengoptimalkan Situs kami
            (misalnya, dengan membuat analisis tentang bagaimana pelanggan kami menjelajah dan berinteraksi dengan
            Situs, dan untuk menilai keberhasilan kampanye pemasaran dan iklan kami).</p>

        <p>MEMBAGIKAN INFORMASI ANDA<br>
            Kami membagikan Informasi Pribadi Anda dengan pihak ketiga untuk membantu kami menggunakan Informasi Pribadi
            Anda, seperti yang dijelaskan di atas. Sebagai contoh, Kami &nbsp;menggunakan Google Analytics untuk
            membantu kami memahami bagaimana pelanggan kami menggunakan Situs - Anda dapat membaca lebih lanjut tentang
            bagaimana Google menggunakan Informasi Pribadi Anda di sini:
            https://www.google.com/intl/en/policies/privacy/. Anda juga dapat memilih keluar dari Google Analytics di
            sini: https://tools.google.com/dlpage/gaoptout. Kami juga Menggunakan Facebook Pixel untuk membantu kami
            melakukan Optimasi Iklan Situs - Anda dapat membaca lebih lanjut bagaimana Facebook Menggunakan Informasi
            Anda disini &nbsp;: https://developers.facebook.com/docs/privacy/.</p>

        <p><br>
            Terakhir, kami juga dapat membagikan Informasi Pribadi Anda untuk mematuhi hukum dan peraturan yang berlaku,
            untuk menanggapi panggilan pengadilan, surat perintah penggeledahan, atau permintaan sah lainnya untuk
            informasi yang kami terima, atau untuk melindungi hak kami.</p>

        <p>KEBIJAKAN PERIKLANAN<br>
            Seperti yang dijelaskan di atas, kami menggunakan Informasi Pribadi Anda untuk memberi Anda iklan bertarget
            atau komunikasi pemasaran yang kami yakini mungkin menarik bagi Anda. Untuk informasi lebih lanjut tentang
            cara kerja iklan bertarget, Anda dapat mengunjungi halaman pendidikan Network Advertising Initiative (“NAI”)
            di http://www.networkadvertising.org/understanding-online-advertising/how-does-it-work.</p>

        <p>Anda dapat memilih keluar dari iklan yang ditargetkan dengan menggunakan tautan di bawah ini:<br>
            - Facebook: https://www.facebook.com/settings/?tab=ads<br>
            - Google: https://www.google.com/settings/ads/anonymous<br>
            - Bing: https://advertise.bingads.microsoft.com/en-us/resources/policies/personalized-ads</p>

        <p>Selain itu, Anda dapat menghindari dari beberapa layanan ini dengan mengunjungi portal penyisihan Aliansi
            Digital Periklanan di: http://optout.aboutads.info/.</p>

        <p>PENGECUALIAN<br>
            Harap perhatikan bahwa kami tidak mengubah pengumpulan data dan praktik penggunaan Situs Anda ketika kami
            melihat sinyal Do Not Track dari browser Anda.</p>

        <p>HAK ANDA<br>
            Jika Anda penduduk Eropa, Anda memiliki hak untuk mengakses informasi pribadi yang kami miliki tentang Anda
            dan meminta agar informasi pribadi Anda diperbaiki, diperbarui, atau dihapus. Jika Anda ingin menggunakan
            hak ini, silakan hubungi kami melalui informasi kontak di bawah ini.</p>

        <p>Selain itu, jika Anda adalah penduduk Eropa, kami mencatat bahwa kami memproses informasi Anda untuk memenuhi
            kontrak yang mungkin kami miliki dengan Anda (misalnya jika Anda membuat pesanan melalui Situs), atau untuk
            mengejar kepentingan bisnis sah kami yang tercantum di atas. Selain itu, harap dicatat bahwa informasi Anda
            akan ditransfer ke luar Eropa, termasuk ke Kanada dan Amerika Serikat.</p>

        <p>RETENSI DATA<br>
            Ketika Anda melakukan pemesanan melalui Situs, kami akan menyimpan Informasi Pesanan Anda untuk catatan kami
            kecuali dan sampai Anda meminta kami untuk menghapus informasi ini.</p>

        <p>PERUBAHAN<br>
            Kami dapat memperbarui kebijakan privasi ini dari waktu ke waktu untuk mencerminkan, misalnya, perubahan
            pada praktik kami atau untuk alasan operasional, hukum, atau peraturan lainnya.</p>

        <p>HUBUNGI KAMI<br>
            Untuk informasi lebih lanjut tentang praktik privasi kami, jika Anda memiliki pertanyaan, atau jika Anda
            ingin mengajukan keluhan, silakan hubungi kami melalui e-mail di info@Fastquran.co atau melalui surat
            menggunakan rincian yang diberikan di bawah ini:</p>

        <p>Fastquran<br>
            [Re: Divisi Kebijakan Privasi]<br>
            Jakarta</p>

    </div>
    <div class="footer">

    </div>
</div>
</body>
</html>
