@extends('frontend.layout.layout')
@section('title',$kelas->title.' Kelas '.$kelas->type.' '.$kelas->level.' — FastQuran')
@section('header')
    <div class="container h-100">
        <div class="row align-items-center justify-content-center h-50 pt-5">
            <div class="col-12 col-lg-12 text-center">
                <h1 class="text-merriweather mb-3">{{$kelas->title}}<br>Kelas {{$kelas->type}} {{$kelas->level}}</h1>
                <hr class="worm"/>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <section style="min-height: 600px;" class="overlap overlap__2 gradient-orange-white pb-5">
        <div class="container checkout-section">
            <div class="row justify-content-center">
                <div class="col col-12 col-lg-7">
                    <div class="card card__compact card__border h-100">
                        <div class="d-flex justify-content-start">
                            <img class="thumbnail-kelas mr-3"
                                 src="{{$kelas->image}}"
                                 alt="">
                            <div class="info-kelas">
                                <h6 class="line-height-1 mb-0 mt-3">{{$kelas->title}}</h6>
                                <p class="h7 text-gray-500 mb-0">Kelas {{$kelas->type}}</p>
                                <span class="badge-kelas">{{$kelas->level}}</span>
                            </div>
                        </div>
                        <hr>
                        <h6 class="h6 mb-0 line-height-1 mb-3">Kelas ini diakses menggunakan email:</h6>
                        <form>
                            <div class="form-group form-group__icon">
                                <label class="sr-only" for="email">Alamat Email</label>
                                <i class="material-icons mr-2">email</i>
                                <input id="email" type="email" name="email" required autocomplete="email" autofocus
                                       class="form-control " aria-describedby="emailHelp" placeholder="Alamat Email"
                                       value="">

                                <small class="form-text text-danger error-email"></small>

                            </div>
                            <div class="form-group form-group__icon d-none">
                                <label class="sr-only" for="name">Nama</label>
                                <i class="material-icons mr-2">favorite</i>
                                <input id="name" type="text" name="name" class="form-control "
                                       aria-describedby="namaUserHelp" placeholder="Nama">

                                <small class="form-text text-danger error-name"></small>

                            </div>

                            <div class="form-group form-group__icon d-none">
                                <label class="sr-only" for="password">Password</label>
                                <i class="material-icons mr-2">https</i>
                                <input id="password" type="password" name="password" class="form-control "
                                       aria-describedby="namaUserHelp" placeholder="Password">

                                <small class="form-text text-danger error-password"></small>

                            </div>
                        </form>
                        <p class="h7 text-gray-500 mb-0 info-email" style="line-height: 1.2;"></p>
                        {{--                    <form>--}}
                        {{--                        <div class="form-group form-group__icon">--}}
                        {{--                            <label class="sr-only" for="email">Alamat Email</label>--}}
                        {{--                            <i class="material-icons mr-2">email</i>--}}
                        {{--                            <input id="email" type="email" name="email" required="" autocomplete="email" autofocus=""--}}
                        {{--                                   class="form-control " aria-describedby="emailHelp" placeholder="Alamat Email"--}}
                        {{--                                   value="anggry.smart@gmail.com" readonly="">--}}

                        {{--                            <small class="form-text text-danger error-email"></small>--}}

                        {{--                        </div>--}}
                        {{--                        <div class="form-group form-group__icon d-none">--}}
                        {{--                            <label class="sr-only" for="name">Nama</label>--}}
                        {{--                            <i class="material-icons mr-2">favorite</i>--}}
                        {{--                            <input id="name" type="text" name="name" class="form-control "--}}
                        {{--                                   aria-describedby="namaUserHelp" placeholder="Nama">--}}

                        {{--                            <small class="form-text text-danger error-name"></small>--}}

                        {{--                        </div>--}}

                        {{--                        <div class="form-group form-group__icon d-none">--}}
                        {{--                            <label class="sr-only" for="password">Password</label>--}}
                        {{--                            <i class="material-icons mr-2">https</i>--}}
                        {{--                            <input id="password" type="password" name="password" class="form-control "--}}
                        {{--                                   aria-describedby="namaUserHelp" placeholder="Password">--}}

                        {{--                            <small class="form-text text-danger error-password"></small>--}}

                        {{--                        </div>--}}
                        {{--                    </form>--}}
                        {{--                    <p class="h7 text-gray-500 mb-0 info-email" style="line-height: 1.2;">Kelas akan diberikan kepada--}}
                        {{--                        Yogi ANggriawan</p>--}}
                    </div>
                </div>
                <div class="wrap-materi-preview col col-12 col-lg-4">
                    <div class="card card__compact card__border h-100">
                        <div class="card-body">
                            <h6 class="h6 mb-0 line-height-1 mb-3">Pembayaran</h6>
                            <div class="d-flex justify-content-between mb-3">
                                <p class="line-height-1 h7 text-gray-500 mb-0">Harga Kelas</p>
                                <h6 class="line-height-1 mb-0 price-text">{{rp($kelas->price)}}</h6>
                            </div>
                            <div class="d-flex justify-content-between mb-2">
                                <p class="line-height-1 h7 text-gray-500 mb-0">Diskon</p>
                                <h6 class="line-height-1 mb-0 discount-text">-</h6>
                            </div>
                            <form>
                                <div class="form-group form-group__icon">
                                    <label class="sr-only" for="kodePromo">Kode Promo</label>
                                    <i class="material-icons mr-2">local_activity</i>
                                    <input id="promo_code" type="text" name="promo_code" class="form-control "
                                           aria-describedby="kodePromoHelp" placeholder="Kode Promo" autocomplete="off">

                                    <small class="form-text text-danger d-none error-promo_code"></small>

                                </div>
                            </form>

                            <div class="d-flex justify-content-between mb-2">
                                <p id="label-available-promotion" class="line-height-1 h7 text-gray-500 mb-0">Promo
                                    Tersedia:</p>
                            </div>


                            <!-- SPECIAL PROMO -->
                            <div class="special-promo">

                            </div>
                            <!-- ./SPECIAL PROMO -->

                            <div class="d-flex justify-content-between mb-3">
                                <p class="line-height-1 h7 font-weight-medium mb-0">Harga Total</p>
                                <h6 class="line-height-1 mb-0 amount-text"><strong>{{rp($kelas->price)}}</strong></h6>
                            </div>
                            <div class="info-bank">
                                <hr>
                                <h6 class="h6 mb-0 line-height-1 mb-3">Transfer Pembayaran:</h6>
                                <div class="item-bank-kelas">
                                    <div class="d-flex justify-content-between mb-3">
                                        <p class="line-height-1 h7 text-gray-500 mb-0">Bank</p>
                                        <h6 class="line-height-1 mb-0">BRI Syariah</h6>
                                    </div>
                                    <div class="d-flex justify-content-between mb-3">
                                        <p class="line-height-1 h7 text-gray-500 mb-0">No. Rekening</p>
                                        <h6 class="line-height-1 mb-0">1022567353</h6>
                                    </div>
                                    <div class="d-flex justify-content-between mb-3">
                                        <p class="line-height-1 h7 text-gray-500 mb-0">Penerima</p>
                                        <h6 class="line-height-1 mb-0">Anggraeni Dewi K.</h6>
                                    </div>
                                    <hr>
                                </div>
                                <h6 class="h6 mb-0 line-height-1 mb-3">Konfirmasi Pembayaran:</h6>
                                <div class="item-bank-kelas">
                                    <div class="d-flex justify-content-between mb-3">
                                        <img height="45" src="https://buildwithangga.com/images/logo_whatsapp.png">
                                    </div>
                                    <div class="d-flex justify-content-between mb-3">
                                        <p class="line-height-1 h7 text-gray-500 mb-0">Yogi Anggriawan</p>
                                        <h6 class="line-height-1 mb-0">6282312225580</h6>
                                    </div>
                                    <hr>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer mt-4">
                            <a href="#" style="font-size: 16px !important;"
                               class="btn btn-primary btn-block font-weight-medium btn-lanjutkan-bayar">LANJUTKAN
                                PEMBAYARAN</a>
                            <a href="https://api.whatsapp.com/send?phone=6282312225580&amp;text=Halo,%20Saya%20sudah%20melakukan%20pembayaran%20kelas%20Tahsin Family.%20Berikut%20saya%20lampirkan%20foto%20bukti%20pembayaran:"
                               style="font-size: 16px !important; display: none;"
                               class="mt-0 btn btn-primary btn-block font-weight-medium btn-confirmation">KONFIRMASI
                                PEMBAYARAN</a>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </section>
@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <script type="text/javascript">
        function capitalizeFirstLetter(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }

        function delayDoneTyping(callback, ms) {
            var timer = 0;
            return function () {
                var context = this,
                    args = arguments;
                clearTimeout(timer);
                timer = setTimeout(function () {
                    callback.apply(context, args);
                }, ms || 0);
            };
        }

        function toggleLabelAvailablePromotion() {
            var promotions = $('.item-voucher').length;

            console.log("toggleLabelAvailablePromotion")

            if (promotions > 0) {
                $('#label-available-promotion').show();
            } else {
                $('#label-available-promotion').hide();
            }
        }

        $(document).ready(function () {
            $('#email').change(function (e) {
                $('.special-promo').html("");

                $('.info-email').html("");
                $('.form-text').html("");
                $('#name').closest('div').addClass('d-none').fadeOut();
                $('#password').closest('div').addClass('d-none').fadeOut();

                var email = $('#email').val();

                toggleLabelAvailablePromotion();

                if (email.length > 0) {
                    var data = {};
                    data['_token'] = "{{csrf_token()}}";
                    data['email'] = email;
                    data['course_id'] = "{{$kelas->id}}";

                    $.ajax({
                        url: "{{route('check_email')}}",
                        method: "POST",
                        data: data,
                    }).done(function (response) {
                        if (response.exist === false) {
                            if (response.valid_gmail == false) {
                                Swal.fire(
                                    'Email tidak valid!',
                                    'Kamu harus menggunakan email dari gmail atau yahoo.',
                                    'error'
                                )
                            } else {
                                $('.info-email').text(response.message).fadeIn();
                            }
                        } else {
                            $('.info-email').text(response.message).fadeIn();
                        }

                        if (response.promoNewMember != null) {
                            var promoNewMember = response.promoNewMember;
                            $('.special-promo').html(`
                                    <div class="col-lg-12 item-voucher mb-2">
                                        <div class="row">
                                            <div class="col-lg-12 col-12">
                                                <p class="line-height-1 h7 mb-1"><span class="red">` + promoNewMember.code + `</span></p>
                                                <h6 class="line-height-1 font-weight-light h7 mb-0 code-text">` + promoNewMember.description + `</h6>
                                            </div>
                                        </div>
                                    </div>
                                `);
                        }

                        toggleLabelAvailablePromotion();

                        $('#promo_code').trigger('keyup');

                    }).fail(function (errors) {
                        var errors = errors.responseJSON.errors;

                        //show errors
                        $.each(errors, function (inputName, err) {
                            //show text errors
                            $.each(err, function (i, text) {
                                $('.error-email').append(capitalizeFirstLetter(text));
                            });
                        })
                    })
                }
            });

            $('#promo_code').keyup(delayDoneTyping(function (e) {
                $('.error-promo_code').addClass('d-none').html("");
                $('.error-email').html("");
                $('.price-text').removeClass('harga-diskon');
                $('.discount-text').text("-");
                $('.amount-text').text("{{rp($kelas->price)}}");

                var promoCode = $(this).val();

                if (promoCode.length > 0) {

                    if ($('#email').val() == "") {
                        Swal.fire(
                            '',
                            'Email harus diisi dulu.',
                            'error'
                        )

                        return;
                    }

                    var data = {};
                    data['_token'] = "{{ csrf_token() }}";
                    data['promo_code'] = promoCode;
                    data['slug'] = "{{$kelas->slug}}";
                    data['email'] = $('#email').val();

                    $.ajax({
                        url: "{{route('kelas.check_promo_code')}}",
                        method: "POST",
                        data: data,
                    }).done(function (response) {
                        if (response.exist === false) {
                            $('.error-promo_code').removeClass('d-none').text(response.message).fadeIn();
                        } else {
                            $('.price-text').addClass('harga-diskon');
                            $('.discount-text').text(response.discount);
                            $('.amount-text').text(response.amount);
                            $('.special-promo').html(`
                                <div class="col-lg-12 item-voucher mb-2">
                                    <div class="row">
                                        <div class="col-lg-12 col-12">
                                            <p class="line-height-1 h7 mb-1"><span class="red">` + response.code + `</span></p>
                                            <h6 class="line-height-1 font-weight-light h7 mb-0 code-text">` + response.description + `</h6>
                                        </div>
                                    </div>
                                </div>
                            `);
                        }
                    }).fail(function (errors) {
                        var errors = errors.responseJSON.errors;

                        //show errors
                        $.each(errors, function (inputName, err) {
                            //show text errors
                            $.each(err, function (i, text) {
                                $('.error-' + inputName).append(capitalizeFirstLetter(text));
                            });
                        })
                    })
                }
            }, 500));


            $('.btn-confirmation').css('display', 'none');

            $('.btn-lanjutkan-bayar').click(function (e) {
                $('.info-bank').fadeIn();
                $('.btn-lanjutkan-bayar').css('display', 'none');
                $('.btn-confirmation').fadeIn();
                e.preventDefault();
            });

            $('.btn-confirmation').click(function (e) {
                e.preventDefault();
                //reset error message
                $('.form-text').html("");

                Swal.fire({
                    title: '',
                    text: "Apakah Anda sudah melakukan pembayaran?",
                    icon: 'info',
                    showCancelButton: true,
                    confirmButtonColor: '#1abc9c',
                    cancelButtonColor: '#bebebe',
                    confirmButtonText: 'Sudah',
                    cancelButtonText: 'Belum',
                }).then((result) => {
                    if (result.value) {
                        var data = {};
                        data['_token'] = "{{csrf_token()}}";
                        data['course_id'] = "{{$kelas->id}}";
                        data['course_price'] = "{{$kelas->price}}";
                        data['promo_code'] = $('#promo_code').val();
                        data['promo_price'] = $('.discount-text').text();
                        data['total_price'] = $('.amount-text').text();
                        data['slug'] = "{{$kelas->slug}}";
                        data['email'] = $('#email').val();

                        $.ajax({
                            url: "{{route('order')}}",
                            method: "POST",
                            data: data,
                        }).done(function(response) {
                            if (response.status === true) {
                                //send whatsapp
                                var win = window.open('https://api.whatsapp.com/send?phone=6282312225580&text=Halo,%20Saya%20sudah%20melakukan%20pembayaran%20kelas%20{{$kelas->title}}.%20Berikut%20saya%20lampirkan%20foto%20bukti%20pembayaran:', '_blank');
                                win.focus();
                                //reload
                                location.href = "{{$kelas->slug}}";

                            } else {
                                $('.info-email').text(response.message).fadeIn();
                            }
                        }).fail(function(errors) {
                            var errors = errors.responseJSON.errors;

                            //show errors
                            $.each(errors, function(inputName, err) {
                                //show text errors
                                $.each(err, function(i, text) {
                                    $('.error-' + inputName).append(text);
                                });
                            })
                        })

                    }
                })
            });
        });
    </script>
@endsection
