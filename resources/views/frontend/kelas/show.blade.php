@extends('frontend.layout.layout')
@section('title',$kelas->title.' Kelas '.$kelas->type.' '.$kelas->level.' — FastQuran')
@section('class','kelas')
@section('header')
    <div class="container h-100">
        <div class="row align-items-center justify-content-center h-50 pt-5">
            <div class="col-12 col-lg-12 text-center">
                <h1 class="text-merriweather mb-3">{{$kelas->title}}<br>Kelas {{$kelas->type}} {{$kelas->level}}
                </h1>
                <p class="font-weight-light">oleh FastQuran</p>
                <hr class="worm"/>
                <div class="row">
                    <div class="col-lg-8 col-12 offset-lg-2">
                        <div class="row">
                            <div class="info-kelas col-6 col-lg-3 text-center">
                                <p class="header-title">JENIS KELAS</p>
                                <p class="sub-title">ONLINE</p>
                            </div>
                            <div class="info-kelas col-6 col-lg-3 text-center">
                                <p class="header-title">TINGKAT</p>
                                <p class="sub-title">TAHSIN</p>
                            </div>
                            <div class="info-kelas col-6 col-lg-3 text-center">
                                <p class="header-title">KONSULTASI</p>
                                <i class="material-icons yes">
                                    check_circle
                                </i>
                            </div>
                            <div class="info-kelas col-6 col-lg-3 text-center">
                                <p class="header-title">SERTIFIKAT</p>
                                <i class="material-icons yes">
                                    check_circle
                                </i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <section style="min-height: 600px;" class="overlap overlap__2 gradient-orange-white pb-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col col-12 col-lg-7">
                    <div class="card card__compact card__border h-100">
                        <div class="info-kelas-play mt-3 font-weight-light">
                            {!! $kelas->description !!}
                        </div>
                    </div>
                </div>
                <div class="wrap-materi-preview col col-12 col-lg-3">
                    <div class="card card__compact card__border h-100">
                        <div class="card-body">
                            <h6 class="h6 mb-0 line-height-1">Materi</h6>


                            <ul class="nav flex-column nav-pills nav-list">
                                @foreach(json_decode($kelas->materi) as $materi)
                                    <li class="nav-item">
                                        <a class="nav-link">{{$materi}}</a>
                                    </li>
                                @endforeach
                                <li class="nav-item ">
                                    <a href="#" class="nav-link disabled">Kuis & Ujian<i
                                            class="material-icons">lock</i></a>
                                </li>
                            </ul>

                        </div>
                        <div class="card-footer">
                            <a href="#pricing" style="font-size: 16px !important;"
                               class="btn btn-primary btn-block font-weight-medium">IKUTI KELAS</a>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </section>

    <!-- Reviews -->
    <section>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-11 col-lg-12 text-center">
                    <h2 class="text-merriweather text-indigo-3">Yuk mulai</h2>
                    <p class="font-weight-light mb-3">
                        Langkah awal yang tepat untuk perbaikan bacaan Al-Qur'an
                    </p>
                </div>
            </div>
            <div class="row justify-content-center mt-5">
                <div class="col col-12 col-lg-12">

                    <div class="row justify-content-center">

                        <div class="col-11 col-lg-4 mb-4 " data-aos="fade-up">
                            <div class="course-card card card__compact card__border">
                                <div class="card-body">
                                    <h6 class="text-review">Sudah berjalan degan baik dan sangat bermanfaat. Materi yang diberikan mudah dihafal dan dipahami, baik buku dan penjelasan langsung...</h6>
                                    <hr/>
                                    <div class="row align-items-center no-gutters">

                                        <div class="col pl-2">
                                            <h6 class="mb-0 line-height-1">@aulia_rwy</h6>
                                            <p class="h7 mb-0 text-gray-500">Kelas Reguler Maret 2019</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-11 col-lg-4 mb-4 " data-aos="fade-up">
                            <div class="course-card card card__compact card__border">
                                <div class="card-body">
                                    <h6 class="text-review">Alhamdulillah tempat berpindah-pindah sehingga tidak bosan. Dikemas dengan bahasa yang mudah dimengerti, Asyik. Tidak ada masalah. Asyik, enjoy, masook.</h6>
                                    <hr/>
                                    <div class="row align-items-center no-gutters">

                                        <div class="col pl-2">
                                            <h6 class="mb-0 line-height-1">Kusumo Aji Saputra</h6>
                                            <p class="h7 mb-0 text-gray-500">Kelas Reguler Maret 2019</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-11 col-lg-4 mb-4 " data-aos="fade-up">
                            <div class="course-card card card__compact card__border">
                                <div class="card-body">
                                    <h6 class="text-review">Sangat efektif dan efisien. Hanya 16x pertemuan Alhamdulillah bisa mengetahui/praktek cara baca quran yang benar. Fasilitasnya nyaman, ada AC, cara ngajar juga pakai multimedia lagi.</h6>
                                    <hr/>
                                    <div class="row align-items-center no-gutters">

                                        <div class="col pl-2">
                                            <h6 class="mb-0 line-height-1">@ibni_ilham93</h6>
                                            <p class="h7 mb-0 text-gray-500">Kelas Reguler Maret 2019</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ./Reviews -->
    <section class="pricing-kelas mt-7" id="pricing" style="margin-bottom: 0">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-11 col-lg-12 text-center">
                    <h2 class="text-merriweather text-indigo-3">Join Sekarang!</h2>
                    <p class="font-weight-light mb-3">
                        Dengan harga <span
                            style="text-decoration:line-through">{{rp($kelas->before_price)}}</span> {{rp($kelas->price)}}
                        saja
                    </p>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-11 col-lg-8">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <div class="featured text-center">
                                <h5 class="text-merriweather text-indigo-3 mb-3">
                                    {{$kelas->title}}
                                </h5>
                                <p class="price font-weight-medium mb-2">
                                    {{rp($kelas->price)}}
                                </p>
                                <p class="akses font-weight-light">
                                    atau <span style="font-weight: bold">{{rp(ceil($kelas->price/$kelas->meet))}}</span>
                                    per
                                    pertemuan
                                </p>
                                <hr class="mt-4 mb-4" style="1px solid #f1f1f1">
                                @foreach(json_decode($kelas->fasilitas) as $fasilitas)
                                    <div class="item mb-2">
                                        <p class="name">
                                            {{$fasilitas}}
                                        </p>
                                        <p class="icon">
                                            <i class="material-icons">
                                                check_circle
                                            </i>
                                        </p>
                                    </div>
                                @endforeach
                                <hr class="mb-4">
                                <a href="{{url('')}}/checkout/{{$kelas->slug}}" target="_blank"
                                   style="font-size: 16px !important;"
                                   class="btn btn-primary btn-block font-weight-medium">Ya,
                                    Saya mau gabung!</a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container mt-5 pt-4">
            <div class="row justify-content-center">
                <div class="col-11 col-lg-12 text-center">
                    <img src="https://www.hujanpelangi.com/wp-content/uploads/2014/11/MBG.jpg" width="150px">
                    <p class="font-weight-light mb-3">
                        Garansi 100% money back, bila tidak mendapatkan pelayanan yang menjadi hak Anda.
                    </p>
                    <h5 class="text-indigo-3">Office</h5>
                    <ul class="list-unstyled sitemap">
                        <li class="line-height-2 font-weight-light">admin@fastquran.id<br> Dero, Condongcatur, Kec.
                            Depok,
                            Kabupaten Sleman, Daerah Istimewa Yogyakarta 55281 <br>
                            Whatsapp: 0823-1222-5580
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript">

        $(document).ready(function (e) {
            $(".dropdown-toggle").dropdown();
        })

    </script>
    <script type="text/javascript">
        $('.owl-carousel').owlCarousel({
            loop: true,
            margin: 20,
            nav: false,
            autoplay: true,
            autoplayTimeout: 3000,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 3
                },
                1000: {
                    items: 4
                }
            }
        })
    </script>
@endsection
