@extends('frontend.layout.layout')
@section('title','Kelas — FastQuran')
@section('class','kelas')
@section('header')
    <div class="container h-100">
        <div class="row align-items-center justify-content-center h-50 pt-5">
            <div class="col-12 col-lg-5 text-center">
                <h1 class="text-merriweather mb-3">Katalog Kelas</h1>
                <hr class="worm"/>
                <p class="font-weight-light">Berikut ini adalah katalog kelas yang bisa kamu ikuti di
                    FastQuran.id </p>

            </div>
            <!-- Force next columns to break to new line -->
            <div class="w-100"></div>
            <div id="app" class="col-lg-12 col-12">
                <courses-component/>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <section class="overlap overlap__2 gradient-orange-white">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-11 col-lg-11">
                    <div id="load-more-wrapper" class="row justify-content-center">
                        @foreach($items as $kelas)
                            <div class="col-11 col-lg-4 mb-4 d-none hide">
                                <div class="course-card card card__compact card__border">
                                    <div class="card-body pb-0">

                                        <a href="{{url('')}}/kelas/{{$kelas->slug}}" class="stretched-link"><span
                                                class="sr-only">title for screen</span></a>
                                        <div class="thumbnail embed-responsive embed-responsive-16by9"
                                             style="height:170px;">
                                            <div class="embed-responsive-item">
                                                <img src="{{url('')}}/assets/frontend/images/kelas/family.jpg"
                                                     class="img img__cover">
                                            </div>
                                            <a href="{{url('')}}/kelas/family" target="_blank">
                                                <div class="hover-content">
                                                    <div class="icon-wrapper icon-wrapper__bigger">
                                                        <div class="icon">
                                                            <i class="material-icons">remove_red_eye</i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <h6 class="line-height-1 mb-0 mt-3">{{$kelas->title}}</h6>
                                        <span class="h7 text-gray-500">Kelas {{$kelas->type}}</span>
                                        <span class="badge-kelas">{{$kelas->level}}</span>
                                        <hr/>
                                        <div class="row no-gutters justify-content-between">

                                            <div class="col-auto d-flex align-items-center">
                                                <div class="mr-2 embed-responsive embed-responsive-1by1"
                                                     style="width:32px;">
                                                    <div class="embed-responsive-item">
                                                        <img
                                                            src="{{$kelas->image}}"
                                                            class="p-1 img__contain" alt="">
                                                    </div>
                                                </div>
                                                <p class="h7 mb-0 font-weight-light">
                                                    <span class="font-weight-medium">Sertifikat</span>
                                                </p>
                                            </div>

                                            <div class="col-auto d-flex align-items-center">
                                                <div class="mr-2 embed-responsive embed-responsive-1by1"
                                                     style="width:32px;">
                                                    <div class="embed-responsive-item">
                                                        <img
                                                            src="{{url('')}}/assets/frontend/images/ic-konsultasi.png"
                                                            class="p-1 img__contain" alt="">
                                                    </div>
                                                </div>
                                                <p class="h7 mb-0 font-weight-light">
                                            <span class="font-weight-medium">

                                            Konsultasi</span>
                                                </p>
                                            </div>


                                        </div>
                                        <hr/>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>


                </div>
            </div>
        </div>
    </section>
@endsection
