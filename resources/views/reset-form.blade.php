
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Reset - Fastquran.id</title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.18/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.18/css/AdminLTE.min.css">


    {{--    <link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">--}}

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    @yield('head')
    <style>
        .skin-blue .main-header .navbar{background: #72545e}
        .skin-blue .main-header .logo:hover{background: #846770}
        .skin-blue .main-header .navbar .sidebar-toggle:hover{background: #846770}
        .skin-blue .main-header li.user-header{background: #72545e}
        .skin-blue .main-header .logo{background: #72545e}
        .content-wrapper{background: #f1f1f1}
        .btn-primary {background: #72545e;border-color: #71545e}
        .btn-primary:hover {background: #846770;border-color: #8f6770}
        .btn-primary:focus {background: #846770;border-color: #8f6770}
        .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {background: #72545e;border-color: #71545e}
        tbody tr td{vertical-align: middle !important;}
        .box.box-primary{border-top-color:#72545e}
        .select2-container{display: block}
        @media (-webkit-min-device-pixel-ratio: 1.5) {
            .dropzone .dz-default.dz-message {
                background-size:auto !important;
            }
        }
        .bg-green, .callout.callout-success, .alert-success, .label-success, .modal-success .modal-body {
            background-color: #72545e !important;
        }
        .bg-green-gradient {
            background: #72545e !important;
            background: -webkit-gradient(linear, left bottom, left top, color-stop(0, #72545e), color-stop(1, #987581)) !important;
            background: -ms-linear-gradient(bottom, #72545e, #987581) !important;
            background: -moz-linear-gradient(center bottom, #72545e 0%, #987581 100%) !important;
            background: -o-linear-gradient(#987581, #72545e) !important;
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#987581', endColorstr='#72545e', GradientType=0) !important;
            color: #fff;
        }

    </style>
</head>
<body class="hold-transition register-page">
<div class="register-box">
    <div class="register-logo">
        <a href="#"><b>Reset Password</b></a>
    </div>

    <div class="register-box-body">
        <p class="login-box-msg">Masukan email Anda</p>
        @include('partner.layout.validation')

        {{ Form::open(['url' => 'reset-password','method' => 'POST']) }}
        <div class="form-group has-feedback">
            <input name="email" type="email" class="form-control" placeholder="Email">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="row">
            <div class="col-xs-8">
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Reset</button>
            </div>
            <!-- /.col -->
        </div>
        {{ Form::close() }}

    </div>
    <!-- /.form-box -->
</div>
<!-- /.register-box -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.full.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.18/js/adminlte.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.min.js"></script>
</body>
</html>
