<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Atur ulang kata sandi</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <style>
        body {
            background: #fff;
            color: #72545e;
            font-family: 'Roboto', sans-serif;
        }

        .container {
            padding-top: 30px;
            max-width: 600px;
        }

        .header {
            text-align: center;
        }

        .content {
            padding: 40px;
        }

        .content p, content h1 {
            text-align: center;
        }

        h1 {
            font-size: 24px;
            text-align: center;
        }

        .logo {
            max-width: 100px;
        }

        .btn-outline-primary {
            color: #72545e;
            border-color: #72545e;
        }

        .btn-outline-primary:hover {
            color: #fff;
            border-color: #fff;
            background: #72545e;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        @if ($errors->any())
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>
                {{ $errors->first() }}
            </div>
        @endif
        @if(Session::has('success'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert">×</button>
                {{ Session::get('success') }}
            </div>
        @endif
        <h1>Atur ulang kata sandi kamu</h1>
        <p>Silakan buat kata sandi baru</p>
        {{ Form::open(['route' => 'update_password']) }}
        {{ Form::hidden('auth', $auth) }}
        <div class="form-group">
            <label for="">Password</label>
            {{ Form::password('password', ['class' => 'form-control','placeholder' => '']) }}
        </div>
        <div class="form-group">
            <label for="">Password Confirmation</label>
            {{ Form::password('password_confirmation', ['class' => 'form-control','placeholder' => '']) }}
        </div>
        <div class="form-group">
            <button class="btn btn-outline-primary">Reset Password</button>
        </div>
        {{ Form::close() }}
    </div>
    <div class="footer">

    </div>
</div>
</body>
</html>
