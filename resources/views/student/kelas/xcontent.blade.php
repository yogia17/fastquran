<div class="row progress-data mt-5">

    <div class="col-lg-6">
        <div class="row ">
            <div class="col-lg-12">
                <p class="title">
                    Konsep Belajar
                </p>
                <p class="sub-title text-secondary">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                    aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                    culpa qui officia deserunt mollit anim id est laborum.
                </p>
            </div>
        </div>
    </div>
    <div class="col-lg-6">

        <div class="row mb-2">
            <div class="col-lg-12">
                <p class="title">
                    Gabung Grup Whatsapp
                </p>
                <p class="sub-title text-secondary">
                    Lanjutkan proses belajarmu lebih giat lagi. Gabung dengan grup whatsapp ya..
                    <br>
                    <a href="#" class="btn btn-success font-weight-medium">
                       <i class="fab fa-whatsapp"></i> JOIN GRUP
                    </a>
                </p>
            </div>
        </div>

    </div>

    <div class="col-lg-6">
        <div class="row ">
            <div class="col-lg-12">
                <p class="title">
                    Terapkan Jadi Shortcut
                </p>
                <p class="sub-title text-secondary">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                    aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                    culpa qui officia deserunt mollit anim id est laborum.
                </p>
            </div>
        </div>
    </div>
    <div class="col-lg-6">

        <div class="row mb-2">
            <div class="col-lg-12">
                <p class="title">
                    Video Habits
                </p>
                <p class="sub-title text-secondary">
                    Lanjutkan proses belajarmu lebih giat lagi. Gabung dengan grup whatsapp ya..
                    <br>
                    <a href="#" class="btn btn-success font-weight-medium">
                       <i class="fab fa-whatsapp"></i> JOIN GRUP
                    </a>
                </p>
            </div>
        </div>

    </div>
    <div class="col-lg-6">

        <div class="row mb-2">
            <div class="col-lg-12">
                <p class="title">
                    Dokument Habits
                </p>
                <p class="sub-title text-secondary">
                    Lanjutkan proses belajarmu lebih giat lagi. Gabung dengan grup whatsapp ya..
                    <br>
                    <a href="#" class="btn btn-success font-weight-medium">
                       <i class="fab fa-whatsapp"></i> JOIN GRUP
                    </a>
                </p>
            </div>
        </div>

    </div>
    <div class="col-lg-6">

        <div class="row mb-2">
            <div class="col-lg-12">
                <p class="title">
                    Pre-Examp
                </p>
                <p class="sub-title text-secondary">
                    Lanjutkan proses belajarmu lebih giat lagi. Gabung dengan grup whatsapp ya..
                    <br>
                    <a href="#" class="btn btn-success font-weight-medium">
                       <i class="fab fa-whatsapp"></i> JOIN GRUP
                    </a>
                </p>
            </div>
        </div>

    </div>
</div>
