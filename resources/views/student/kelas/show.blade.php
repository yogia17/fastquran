<!DOCTYPE html>
<html lang="id-ID.utf8">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>
        {{$kelas->title}}
    </title>

    <!-- Main theme -->
    <link
        href="https://fonts.googleapis.com/css?family=Merriweather:300,400,700|Montserrat:300,400,500,700&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{url('assets/student')}}/css/style.css">

</head>

<body>

<!-- begin sidebar -->
<aside class="sidebar d-none d-sm-block" data-simplebar id="sidebar">
    <div class="navigator">
        <a href="{{route('student.kelas.index')}}" class="d-flex hover-no-underline">
            <i class="material-icons mr-2">arrow_back</i>
            <span>Kembali ke Home</span>
        </a>

        <button style="height: 50px;" class="hamburger hamburger--3dy is-active" id="toggle-collapse" type="button">
                <span class="hamburger-box">
                    <!-- <span class="hamburger-inner"></span> -->
                    <i class="material-icons" style="font-size: 22px; color: white;">
                        menu
                    </i>
                </span>
        </button>
    </div>
    @foreach($sections as $section)
        <ul class="nav nav__sub-menu flex-column nav-pills ">
            <li class="nav-item">
                <h6 class="nav-heading">{{$section->title}}</h6>
            </li>
            <li class="nav-item ">
                @foreach($section->lessons() as $lesson)
                    <a class="nav-link" href="{{route('student.kelas.show', $kelas->slug)}}?lesson={{$lesson->id}}">{{$lesson->title}}</a>
                @endforeach
            </li>
        </ul>
    @endforeach

    {{--    <ul class="nav nav__sub-menu flex-column nav-pills ">--}}
    {{--        <li class="nav-item">--}}
    {{--            <h6 class="nav-heading">Introduction</h6>--}}
    {{--        </li>--}}
    {{--        <li class="nav-item ">--}}
    {{--            <a class="nav-link" target="_blank" href="kelas/micro-interaction">Intro</a>--}}
    {{--        </li>--}}
    {{--    </ul>--}}

    {{--    <ul class="nav nav__sub-menu flex-column nav-pills ">--}}
    {{--        <li class="nav-item">--}}
    {{--            <h6 class="nav-heading">STAGE 1</h6>--}}
    {{--        </li>--}}
    {{--        <li class="nav-item">--}}
    {{--            <a class="nav-link" href="1">--}}
    {{--                STAGE 1--}}
    {{--            </a>--}}
    {{--        </li>--}}
    {{--        <li class="nav-item">--}}
    {{--            <a class="nav-link" href="2">--}}
    {{--                Challenge 1--}}
    {{--            </a>--}}
    {{--        </li>--}}
    {{--        <li class="nav-item">--}}
    {{--            <a class="nav-link" href="2">--}}
    {{--                Challenge 2--}}
    {{--            </a>--}}
    {{--        </li>--}}
    {{--        <li class="nav-item">--}}
    {{--            <a class="nav-link" href="2">--}}
    {{--                Challenge 3--}}
    {{--            </a>--}}
    {{--        </li>--}}
    {{--        <li class="nav-item">--}}
    {{--            <a class="nav-link" href="2">--}}
    {{--                Challenge 4--}}
    {{--            </a>--}}
    {{--        </li>--}}
    {{--        <li class="nav-item">--}}
    {{--            <div id="materi_aktif" name="materi_aktif">--}}
    {{--                <a class="nav-link active" href="3">--}}
    {{--                    Special Challenge--}}
    {{--                </a>--}}
    {{--            </div>--}}
    {{--        </li>--}}
    {{--        <li class="nav-item">--}}
    {{--            <a class="nav-link" href="2">--}}
    {{--                Bonus Challenge--}}
    {{--            </a>--}}
    {{--        </li>--}}

    {{--        <!-- add link quiz if exist -->--}}
    {{--    </ul>--}}


</aside>
<!-- end of sidebar -->

<!-- mobile layout goes here -->
<main class="header-mobile-dashboard d-block d-sm-none">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-4">
                        <a href="https://class.buildwithangga.com/kelas_saya" class="d-flex hover-no-underline">
                            <i class="material-icons mr-2">arrow_back</i> Kembali
                        </a>
                    </div>

                </div>
                <hr>
                <div class="row justify-content-center">
                    <div class="col-7">
                        <div class="user-info text-center">
                            <p class="merriweather name mb-1">
                                Micro Interaction
                            </p>
                            <p class="font-weight-light">
                                oleh Angga Risky
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</main>

<main class="main-wrapper mobile-video-player">
    <div class="container mobile-video-wrapper">

        <div class="row justify-content-center">
            <div class="col-lg-11">
                <div class="heading">

                    <div class="row">
                        <div class="col-lg-6 col-12">
                            <h1 class="h4 mb-1">Pre-Class</h1>
                            <span>Persiapan</span>
                        </div>

                        {{--                        <div class="col align-items-center justify-content-end d-flex text-right">--}}
                        {{--                            <a href="#" title="MATERI BELUM SELESAI" style="cursor: not-allowed !important;" class="btn d-none d-sm-block btn-success font-weight-medium">SELESAI BELAJAR</a>--}}
                        {{--                        </div>--}}
                    </div>
                </div>
                @include('student.kelas.content');
                <div class="embed-responsive embed-responsive-16by9">
                    <div class="plyr__video-embed" id="player">
                        {{--                        <iframe src="https://www.youtube.com/embed/N6wlKwZRnQc?origin=https://plyr.io&amp;iv_load_policy=3&amp;modestbranding=1&amp;playsinline=1&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&autoplay=1" allowfullscreen allowtransparency allow="autoplay"></iframe>--}}
                    </div>
                </div>
            </div>
        </div>

        <a href="#" title="MATERI BELUM SELESAI" style="cursor: not-allowed !important;"
           class="d-block btn-block d-sm-none btn btn-success font-weight-medium mt-3">SELESAI BELAJAR</a>

        <div class="materi-video-mobile d-block d-sm-none">
            <ul class="nav nav__sub-menu flex-column nav-pills ">
                <li class="nav-item">
                    <h6 class="nav-heading">Micro Interaction</h6>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="1">
                        Membuat Artboard U...


                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="2">
                        UI Animation


                    </a>
                </li>
                <li class="nav-item">
                    <div id="materi_aktif" name="materi_aktif">
                        <a class="nav-link active" href="3">
                            Real Project

                        </a>
                    </div>
                </li>

                <!-- add link quiz if exist -->
            </ul>
        </div>

    </div>
</main>


<script src="{{url('assets/student')}}/scripts/jquery-latest.min.js" type="text/javascript"></script>
<script src="{{url('assets/student')}}/scripts/bootstrap.min.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/simplebar@latest/dist/simplebar.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<script type="text/javascript">
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
        $("#toggle-collapse").click(function () {
            $(this).toggleClass("is-active")
            $(this).parents("aside").toggleClass("collapsed")
            $(this).parents("aside").find("ul").toggle()
        })
    });

    var recentStatus = "";
    var startTime = parseFloat("0");

    $('.btn-download-gagal').click(function (e) {
        e.preventDefault();

        Swal.fire(
            'Gagal download!',
            'Kamu wajib memiliki setidaknya 1 kelas Premium.',
            'error'
        )

    });


    $(document).ready(function () {
        //scroll to active menu
        setTimeout(function () {
            var lastPosition = $('#materi_aktif').offset().top - 300;
            $('.simplebar-content-wrapper').scrollTop(lastPosition)
        }, 200)


        $('.btn-done').click(function (e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            var currentTime = 0;
            var status = 2;
            var urlNextVideo = $('.btn-next-video').attr('href');

            $.ajax({
                url: "https://class.buildwithangga.com/materi_kelas/set_hasil",
                method: "POST",
                data: {
                    _token: "yIGu1QKadBo6jy3DrPw9BNrG1AtCDQB51KobWsWA",
                    id: id,
                    current_time: currentTime,
                    status: status
                },
            }).done(function (response) {
                if (response) {
                    window.location.href = urlNextVideo;
                }
            }).fail(function (errors) {

            })
        });

    });
</script>

</body>

</html>
