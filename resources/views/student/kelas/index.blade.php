@extends('student.layout.layout')
@section('title','Kelas Saya')
@section('content')
    @if(count($userCourse)>0)
        <main class="d-none d-sm-block main-wrapper">
            <div class="container">

                <!-- include verify notification -->
                <!-- ./include verify notification -->

                <div class="heading">
                    <h1 class="h6">Kelas Saya</h1>
                    <span class="text-secondary">{{count($userCourse)}} kelas tersedia</span>
                </div>

                <ul class="nav nav-pills mb-3" id="detail-kelas" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="semua-tab" data-toggle="pill" href="#semua" role="tab"
                           aria-controls="semua" aria-selected="true">SEMUA</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " id="premium-tab" data-toggle="pill" href="#progress" role="tab"
                           aria-controls="premium" aria-selected="false">Dalam Proses</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " id="finished-tab" data-toggle="pill" href="#finished" role="tab"
                           aria-controls="finished" aria-selected="false">Selesai</a>
                    </li>
                </ul>

                <div class="tab-content mt-4" id="detail-kelasContent">

                    <div class="tab-pane fade show active" id="semua" role="tabpanel" aria-labelledby="semua-tab">
                        <div class="row row__gallery">
                            @foreach($userCourse as $item)
                                <div class="col-auto" style="width: 300px;">
                                    <div class="card">
                                        <div class="thumbnail embed-responsive embed-responsive-16by9"
                                             style="height: 160px;">
                                            <img src="{{$item->course->image}}"
                                                 class="img img__cover"/>
                                            <a class="hover-content"
                                               href="{{url('student/kelas/')}}/{{$item->course->slug}}">
                                                <div class="icon-wrapper">
                                                    <div class="icon">
                                                        <img src="https://class.buildwithangga.com/images/play.svg"
                                                             alt="play">
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="card-body mt-3">
                                            <h5 class="">{{$item->course->title}}</h5>
                                            <span class="text-secondary">Kelas {{$item->course->type}} {{$item->course->level}} </span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="tab-pane fade" id="progress" role="tabpanel" aria-labelledby="premium-tab">
                        <div class="row row__gallery">


                            <div class="col-auto">
                                <p>Kamu belum mengikuti kelas.</p>
                                <a href="{{url('kelas')}}"
                                   class="btn btn-info font-weight-medium mr-2" target="_blank">LIHAT KELAS TERSEDIA</a>
                            </div>


                        </div>
                    </div>

                    <div class="tab-pane fade" id="finished" role="tabpanel" aria-labelledby="finished-tab">
                        <div class="row row__gallery">


                        </div>
                    </div>

                </div>


            </div>
        </main>
    @else
        <main class="main-wrapper h-100vh d-none d-sm-block">
            <div class="container w-800px mx-auto h-100">

                <div class="row align-items-center justify-content-center h-100">
                    <div class="col-7 text-center">
                        <h1 class="h2 font-weight-bold merriweather mt-5">Bismillah, Yuk dimulai!</h1>
                        <p class="h8 mt-3 mb-1 mx-5 px-5 line-height-150">Investasikan waktumu untuk akhirat yang lebih
                            baik. Insyaa Allah.</p>
                        <a href="{{url('')}}/kelas" class="btn btn-info font-weight-medium mt-4">LIHAT KELAS
                            TERSEDIA</a>
                    </div>

                </div>


            </div>
        </main>
    @endif
@endsection
@section('mobile')
    <!-- mobile layout goes here -->
    <main class="header-mobile-dashboard d-block d-sm-none">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-12">
                    <img src="{{url('')}}/assets/student/images/default-avatar.svg" alt="avatar buildwith angga class"
                         class="user-picture">
                    <div class="user-info">
                        <p class="merriweather name mb-1">
                            Yogi Anggriawan
                        </p>
                        <p class="font-weight-light">
                            Student
                        </p>
                    </div>
                    <a href="{{url('')}}/logout" class="btn-logout-mobile font-weight-light"
                       aria-label="logout">Keluar</a>
                </div>
            </div>
        </div>
    </main>

    <main class="mt-5 body-mobile-dashboard d-block d-sm-none">
        <div class="container">
            <div class="row text-center">
                <div class="col-12 col-lg-12">
                    <h1 class="title-empty-state h2 font-weight-bold merriweather mt-5">Bismillah, Yuk dimulai!</h1>
                    <p class="subtitle-empty-state h8 mt-3 mb-1 mx-5 px-2 line-height-150">Investasikan waktumu untuk
                        akhirat yang lebih baik. Insyaa Allah.</p>
                    <div class="text-center">
                        <a href="{{url('')}}/kelas" class="btn btn-info font-weight-medium mt-4">LIHAT KELAS
                            TERSEDIA</a>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <main class="bottom-nav-mobile-dashboard d-block d-sm-none fixed-bottom pt-2 py-2">
        <div class="container">
            <div class="row justify-content-center">


                <div class="col-10">
                    <div class="row">
                        <div class="col-lg-4 col-4">
                            <a href="{{url('')}}/progress_belajar">
                                <div class="item-menu text-center">
                                    <p class="mb-0">
                                        <i class="material-icons">
                                            assessment
                                        </i>
                                    </p>
                                    <p class="mb-0 mt-0">
                                        Progress
                                    </p>
                                </div>
                            </a>
                        </div>

                        <div class="col-lg-4 col-4">
                            <a href="{{url('')}}/kelas_saya">
                                <div class="item-menu text-center active">
                                    <p class="mb-0">
                                        <i class="material-icons">
                                            folder_shared
                                        </i>
                                    </p>
                                    <p class="mb-0 mt-0">
                                        Kelas
                                    </p>
                                </div>
                            </a>
                        </div>

                        <div class="col-lg-4 col-4">
                            <a href="{{url('')}}/kelas">
                                <div class="item-menu text-center">
                                    <p class="mb-0">
                                        <i class="material-icons">
                                            class
                                        </i>
                                    </p>
                                    <p class="mb-0 mt-0">
                                        Library
                                    </p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </main>
@endsection
