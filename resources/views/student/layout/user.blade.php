<li class="dropdown user user-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <span class="hidden-xs">{{ auth()->user()->name }}</span>
    </a>
    <ul class="dropdown-menu">
        <li class="user-header">
            <p>
                {{ auth()->user()->name }}
                <small>Member since {{ auth()->user()->created_at->format('M Y') }}</small>
            </p>
        </li>
        <li class="user-footer">
            <div class="pull-left">
                <a href="{{ route('partner.profile.index') }}" class="btn btn-default btn-flat">Profile</a>
            </div>
            <div class="pull-right">
                <a href="{{ url('logout') }}" class="btn btn-default btn-flat">Sign out</a>
            </div>
        </li>
    </ul>
</li>
