<!-- begin sidebar -->
<aside class="sidebar d-none d-sm-block">
    <div class="card">
        <div class="card-body text-center">
            <div class="avatar embed-responsive embed-responsive-1by1 mx-auto" style="width:100px; height: 120px;">
                <div class="frame frame__active ">
                    <img class="img-avatar" src="{{url('')}}/assets/student/images/default-avatar.svg" alt="avatar" />
                </div>
            </div>
            <h5 class="card-name mt-2">{{auth()->user()->name}}</h5>

            <span class="">Student</span>
            <!-- <span class="mt-2 d-flex align-items-center badge-pro-bwa badge-primary">PRO <i class="material-icons">
                stars
              </i></span> -->
        </div>
    </div>

    <ul class="nav nav__main-menu flex-column">
        <li class="nav-item">
            <h6 class="nav-heading">Main Menu</h6>
        </li>
{{--        <li class="nav-item">--}}
{{--            <a class="nav-link " href="{{url('')}}/student/progress_belajar">Progress Belajar</a>--}}
{{--        </li>--}}
        <li class="nav-item ">
            <a class="nav-link active" href="{{url('')}}/student/kelas">Kelas Saya</a>
        </li>
        <li class="nav-item">
            <a class="nav-link " target="_blank" href="{{url('')}}/kelas">Katalog Kelas</a>
        </li>
    </ul>

    <ul class="nav nav__main-menu flex-column">
{{--        <li class="nav-item">--}}
{{--            <h6 class="nav-heading">Akun Saya</h6>--}}
{{--        </li>--}}
{{--        <li class="nav-item">--}}
{{--            <a class="nav-link " href="{{url('')}}/edit_profile">Edit Profil</a>--}}
{{--        </li>--}}
{{--        <li class="nav-item">--}}
{{--            <a class="nav-link " href="{{url('')}}/edit_password">Security Password</a>--}}
{{--        </li>--}}
        <li class="nav-item">
            <a class="nav-link" href="{{url('')}}/logout">Keluar</a>
        </li>
    </ul>
</aside>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>  <!-- end of sidebar -->
