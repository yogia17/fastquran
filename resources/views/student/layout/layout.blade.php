
<!DOCTYPE html>
<html lang="id-ID.utf8">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>@yield('title')</title>

    <!-- Main theme -->
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300,400,700|Montserrat:300,400,500,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{url('assets/student')}}/css/style.css">
    <link rel="stylesheet" href="{{url('assets/student')}}/css/plyr.css">
</head>

<body>
@include('student.layout.sidebar');
@yield('content')
@yield('mobile')
<script src="{{url('assets/student')}}/scripts/jquery-latest.min.js" type="text/javascript"></script>
<script src="{{url('assets/student')}}/scripts/bootstrap.min.js" type="text/javascript"></script>
<script src="{{url('assets/student')}}/scripts/main.js" type="text/javascript"></script>

<script type="text/javascript"></script>

</body>

</html>
