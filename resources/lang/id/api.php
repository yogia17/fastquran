<?php

return [
    // validate
    'v_failed' => 'Data tidak sesuai',
    // product
    // cart
    'ca_store_success' => "Berhasil ditambah ke keranjang. Kamu punya :minute menit untuk menyelesaikan pembelian",
    'ca_store_failed' => 'Terdapat kesalahan penambahan keranjang',

    'ca_store_not_fond' => 'Produk tidak tersedia',
    'ca_store_user_max_item' => 'Maksimal pembelian tiap customer adalah :value barang',
    'ca_store_product_max_item' => 'Maksimal pembelian tiap produk adalah :value barang',
    'ca_store_variant_not_found' => 'Varian tidak tersedia',
    'ca_store_variant_out_stock' => 'Produk jenis ini sudah habis',
    'ca_store_variant_over' => 'Pembelian melebihi ketersediaan',
    'ca_store_variant_max_order' => 'Maksimal pembelian tiap model adalah :value barang',

    'ca_update_success' => 'Pesanan berhasil di rubah',
    'ca_update_failed' => 'Pesanan gagal di rubah',
    'ca_destroy_success' => 'Berhasil di hapus dari keranjang',
    'ca_destroy_failed' => 'Gagal di hapus dari keranjang',
    'ca_clear_success' => 'Keranjang berhasil di bersihkan',
    'ca_clear_failed' => 'Keranjang gagal di bersihkan',
    'ca_get_success' => 'Sukses',
    'ca_get_failed' => 'failed',

    'address' => [
        'success_list' => 'Daftar alamat',
        'error_validation' => 'Validasi gagal',
        'error_thirdparty' => '',
        'error_not_updated' => 'Alamat tidak berubah',
        'error_not_deleted' => 'Gagal dapat menghapus alamat',
        'updated' => 'Alamat kamu berhasil di ubah',
        'deleted' => 'Alamat berhasil di hapus',
        'created' => 'Alamat berhasil di buat',
    ],

    'payment' => [
        'error_validation' => 'Validasi Permintaan Gagal',
        'error_calc_amount' => 'Eh, kami salah hitung, coba lagi',
        'error_invalid_address' => 'Alamat tujuan tidak benar',
        'empty_cart' => 'Keranjang kamu kosong, mungkin kamu melebihi waktu belanja kamu'
    ],

    'checkout' => [
        'shipping_cost_confirm' => 'Butuh Konfirmasi',
        'shipping_etd' => 'hari'
    ],

    'product' => [
        'list_product' => 'Daftar Produk'
    ],

    'user' => [
        'near_timeout' => 'Waktu belanja Anda hampir habis. Segera checkout atau barang di keranjang akan dikembalikan',
        'unpaid_transaction' => 'Kamu punya transaksi yang belum kamu bayar, selesaikan pembayaran sebelum membuat pembelian baru'
    ]

    // checkout
    // address
];
