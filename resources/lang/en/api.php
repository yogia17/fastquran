<?php

return [
    // validate
    'v_failed' => 'Request is not valid',
    // product
    // cart
    'ca_store_success' => "Added to Cart. You have :minute minutes to complete order",
    'ca_store_failed' => 'Failed to Add Cart',

    'ca_store_not_fond' => 'Product not found',
    'ca_store_user_max_item' => 'Maximum order each user is :value items',
    'ca_store_product_max_item' => 'Maximum order each product is :value items',
    'ca_store_variant_not_found' => 'Variant not found',
    'ca_store_variant_out_stock' => 'Product is out of stock',
    'ca_store_variant_over' => 'You order mode than stock',
    'ca_store_variant_max_order' => 'Maximum order each variant is :value items',

    'ca_update_success' => 'Order Changed',
    'ca_update_failed' => 'Pesanan gagal di rubah',
    'ca_destroy_success' => 'Item restored',
    'ca_destroy_failed' => 'Failed to restore item',
    'ca_clear_success' => 'Cart cleared',
    'ca_clear_failed' => 'Failed to clear cart',
    'ca_get_success' => 'Success',
    'ca_get_failed' => 'failed',

    'address' => [
        'success_list' => 'List Address',
        'error_validation' => 'Validation Failed',
        'error_thirdparty' => '',
        'error_not_updated' => 'Nothing changes to address',
        'error_not_deleted' => 'Cannot remove address',
        'updated' => 'Address successfully updated',
        'deleted' => 'Address deleted',
        'created' => 'Address created',
    ],

    'payment' => [
        'error_validation' => 'Request validation failed',
        'error_calc_amount' => 'Whoops, 1+1 = 11, we will calculate again',
        'error_invalid_address' => 'Destination address is invalid',
        'empty_cart' => 'Check your cart, because cart is empty, maybe timeout'
    ],

    'checkout' => [
        'shipping_cost_confirm' => 'On Confirmation',
        'shipping_etd' => 'day(s)'
    ],

    'product' => [
        'list_product' => 'List Product'
    ],

    'user' => [
        'near_timeout' => 'Your cart time is running out, finish your payment before your cart item is erased',
        'unpaid_transaction' => 'You have unpaid transaction, finish payment before create new order'
    ]

    // checkout
    // address
];
