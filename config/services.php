<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */
    'telegram-bot-api' => [
        'token' => env('TELEGRAM_BOT_TOKEN', '966662784:AAEfBoV1evSJyZNAssYvjLBV2hBQlkAGhws')
    ],
    'sendgrid' => [
        'api_key' => env('SENDGRID_API_KEY', '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'),
    ],
    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET', '98fe9cc5b2cca329ce2891a4738650f6aff8b057'),
        'guzzle' => [
            'verify' => true,
            'decode_content' => true,
        ],
        'options' => [
            'open_tracking' => false,
            'click_tracking' => false,
            'transactional' => true,
        ],
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
        'webhook' => [
            'secret' => env('STRIPE_WEBHOOK_SECRET'),
            'tolerance' => env('STRIPE_WEBHOOK_TOLERANCE', 300),
        ],
    ],
    /* Social Media */
    'facebook' => [
        'client_id' => '2052779698152643',
        'client_secret' => '1502bfea2bfcd8a3f2a86b76720c9bc4',
        'redirect' => 'https://beta.devsandbox.me/auth/facebook/callback',
    ],

    'twitter' => [
        'client_id' => env('TWITTER_ID'),
        'client_secret' => env('TWITTER_SECRET'),
        'redirect' => env('TWITTER_URL'),
    ],

    'google' => [
        'client_id' => '406460525596-bic1pd11d90msvki2iie9e7vqu6okegc.apps.googleusercontent.com',
        'client_secret' => 'Jt-SM8QZ6NeFIfXlDrgkNxHx',
        'redirect' => 'https://beta.devsandbox.me/auth/google/callback',
    ],

];
