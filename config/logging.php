<?php

use Monolog\Handler\StreamHandler;
use Monolog\Handler\SyslogUdpHandler;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Log Channel
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that gets used when writing
    | messages to the logs. The name specified in this option should match
    | one of the channels defined in the "channels" configuration array.
    |
    */

    'default' => env('LOG_CHANNEL', 'stack'),

    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "monolog",
    |                    "custom", "stack"
    |
    */

    'channels' => [
        'stack' => [
            'driver' => 'stack',
            'channels' => ['daily'],
        ],

        'single' => [
            'driver' => 'single',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
        ],

        // 'cloudwatch' => [
        //     'name' => env('CLOUDWATCH_LOG_NAME', 'zerinabanu_log'),
        //     'region' => env('CLOUDWATCH_LOG_REGION', 'ap-southeast-1'),
        //     'credentials' => [
        //         'key' => env('CLOUDWATCH_LOG_KEY', 'AKIATEBOPMP7LE5CSHBK'),
        //         'secret' => env('CLOUDWATCH_LOG_SECRET', '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')
        //     ],
        //     'stream_name' => env('CLOUDWATCH_LOG_STREAM_NAME', 'zerina_banu'),
        //     'retention' => env('CLOUDWATCH_LOG_RETENTION_DAYS', 14),
        //     'group_name' => env('CLOUDWATCH_LOG_GROUP_NAME', 'zerina_banu'),
        //     'version' => env('CLOUDWATCH_LOG_VERSION', 'latest'),
        //     'formatter' => \Monolog\Formatter\JsonFormatter::class
        // ],

        'daily' => [
            'driver' => 'daily',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
            'days' => 14,
        ],

        // 'slack' => [
        //     'driver' => 'slack',
        //     'url' => env('LOG_SLACK_WEBHOOK_URL'),
        //     'username' => 'Laravel Log',
        //     'emoji' => ':boom:',
        //     'level' => 'critical',
        // ],

        // 'papertrail' => [
        //     'driver' => 'monolog',
        //     'level' => 'debug',
        //     'handler' => SyslogUdpHandler::class,
        //     'handler_with' => [
        //         'host' => env('PAPERTRAIL_URL'),
        //         'port' => env('PAPERTRAIL_PORT'),
        //     ],
        // ],

        // 'stderr' => [
        //     'driver' => 'monolog',
        //     'handler' => StreamHandler::class,
        //     'formatter' => env('LOG_STDERR_FORMATTER'),
        //     'with' => [
        //         'stream' => 'php://stderr',
        //     ],
        // ],

        'syslog' => [
            'driver' => 'syslog',
            'level' => 'debug',
        ],

        'errorlog' => [
            'driver' => 'errorlog',
            'level' => 'debug',
        ],
    ],

];
