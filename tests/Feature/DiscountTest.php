<?php

namespace Tests\Feature;

use App\Order;
use App\Cuppon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Payment\DiscountCalc;

class DiscountTest extends TestCase
{
    use DatabaseTransactions;

    public function testFlatDiscount()
    {
        $order = Order::create(['gross_amount' => 2000000]);
        $cuppon = Cuppon::create(['code' => 'PROMO200', 'amount' => 200000, 'type' => 'flat']);

        $discount = new DiscountCalc($order, $cuppon);

        $this->assertSame(200000, $discount->getDiscount());
    }

    public function testPercentDiscount()
    {
        $order = Order::create(['gross_amount' => 200000]);
        $cuppon = Cuppon::create(['code' => 'PER20', 'amount' => 50000, 'type' => 'percent', 'percentage' => '20']);

        $discount = new DiscountCalc($order, $cuppon);

        // diskon 20% dari 200.000 adalah 40.000
        $this->assertSame(40000, $discount->getDiscount());
    }

    public function testPercentDiscount2()
    {
        $order = Order::create(['gross_amount' => 1000000]);
        $cuppon = Cuppon::create(['code' => 'PER20', 'amount' => 50000, 'type' => 'percent', 'percentage' => '20']);

        $discount = new DiscountCalc($order, $cuppon);

        // diskon 20% (max 50.000)  dari 2.000.000 adalah 50.000
        $this->assertSame(50000, $discount->getDiscount());
    }
}
