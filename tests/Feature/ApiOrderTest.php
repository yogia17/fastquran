<?php

namespace Tests\Feature;

use App\User;
use App\Address;
use App\Product;
use Tests\TestCase;
use function GuzzleHttp\json_decode;
use Illuminate\Foundation\Testing\WithFaker;

class ApiOrderTest extends TestCase
{
    use WithFaker;
    // use DatabaseTransactions;

    private function getUserToken($user_id)
    {
        $user = User::find($user_id);

        $response = $this->json('POST', '/api/login', [
            'email' => $user->email,
            'password' => 'secret'
        ]);

        $response = json_decode($response->getContent());

        return $response->token;
    }

    /**
     * @test
     */
    public function userAddCart()
    {
        $product = Product::inRandomOrder()->first();

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('POST', 'api/cart/store', [
            'product_id' => $product->id,
            'sku_id' => $product->skus->first()->id,
            'qty' => 2,
            'notes' => 'test note',
            'cut' => 2,
            'cut_notes' => '111|222',
        ]);

        $response->assertJson(['status' => 'success'])->assertStatus(200);
    }

    /**
     * @test
     */
    public function userOrderItemsWithInvalidParam()
    {
        $product = Product::find(1);

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('POST', 'api/cart/store', [
            'product_id' => $product->id,
            'sku_id' => $product->skus->first()->id,
            'qty' => 'satu',
        ]);

        $response->assertJson(['status' => 'error'])->assertStatus(401);
    }

    /**
     * @test
     */
    public function errorWhenUserOrderMoreThanStock()
    {
        $product = Product::find(1);

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('POST', 'api/cart/store', [
            'product_id' => $product->id,
            'sku_id' => $product->skus->first()->id,
            'qty' => 9999,
            'notes' => 'yang bagus ya',
            'cut' => 1,
            'cut_notes' => 'potong 5cm',
        ]);

        $response
            ->assertJson(['status' => 'error'])
            ->assertJsonStructure(['status', 'message', 'qty_stock'])
            ->assertStatus(401);
    }

    /**
     * @test
     */
    public function errorWhenUserOrderNotOnRightVariant()
    {
        $product = Product::find(1);

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('POST', 'api/cart/store', [
            'product_id' => $product->id,
            'sku_id' => 999,
            'qty' => 6,
            'notes' => 'yang bagus ya',
            'cut' => 1,
            'cut_notes' => 'potong 5cm',
        ]);

        $response->assertJson(['status' => 'error'])->assertStatus(401);
    }

    /**
     * @test
     */
    public function userChangeOrder()
    {
        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('GET', 'api/cart/clear');

        $response->assertStatus(200);

        $product = Product::InRandomOrder()->first();

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('POST', 'api/cart/store', [
            'product_id' => $product->id,
            'sku_id' => $product->skus->first()->id,
            'qty' => 1,
            'notes' => $this->faker->sentence(),
            'cut' => 1,
            'cut_notes' => 'potong 5cm',
        ]);

        $response->assertJson(['status' => 'success'])->assertStatus(200);

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('GET', 'api/cart/get');

        $response->assertJson(['status' => 'success'])->assertStatus(200);

        $get_cart = json_decode($response->getContent());

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('POST', 'api/cart/update/' . $get_cart->data[0]->group_id, [
            'qty' => 2,
            'notes' => 'Updated',
            'is_cut' => 0,
            'cut_notes' => 'Updated',
        ]);

        $response->assertJson(['status' => 'success'])->assertStatus(200);
    }

    /**
     * @test
     */
    public function userClearCart()
    {
        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('GET', 'api/cart/clear');

        $response
            ->assertJson(['status' => 'success'])
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function userGetCart()
    {
        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('GET', 'api/cart/clear');

        $response->assertStatus(200);

        $product = Product::InRandomOrder()->first();

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('POST', 'api/cart/store', [
            'product_id' => $product->id,
            'sku_id' => $product->skus->first()->id,
            'qty' => 2,
            'notes' => 'pesan ganteng',
            'cut' => 1,
            'cut_notes' => 'potong 100cm',
        ]);

        $response
            ->assertJson(['status' => 'success'])
            ->assertStatus(200);

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('GET', 'api/cart/get');

        $response
            ->assertJson(['status' => 'success'])
            ->assertSee('pesan ganteng')
            ->assertSee(5)
            ->assertSee('potong 100cm')
            ->assertJsonStructure(['status', 'data', 'message'])
            ->assertStatus(200);
    }

    // Disable Function
    public function UploadBuktiTransver()
    {
        $user_token = $this->getUserToken(4);

        $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('GET', 'api/cart/clear')->assertStatus(200);

        $product = Product::InRandomOrder()->first();

        $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('POST', 'api/cart/store', [
            'product_id' => $product->id,
            'sku_id' => $product->skus->first()->id,
            'qty' => 2,
            'notes' => '',
            'cut' => 1,
            'cut_notes' => '100',
        ])->assertStatus(200);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('GET', 'api/checkout/get');

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('POST', 'api/checkout/commit', [
            'payment_id' => 'midtrans_va_permata',
            'dropshipper' => 1,
            'dropship_name' => 'Robbyn',
            'dropship_phone' => '1234567890'
        ]);

        $json_response = json_decode($response->getContent());
        $base64_image = base64_encode(file_get_contents(storage_path('test/buktitransver.jpg')));

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('POST', 'api/checkout/upload_transver', [
            'order_id' => $json_response->order_id,
            'image' => $base64_image
        ]);

        $response->assertJson(['status' => 'success'])->assertStatus(200);
    }


    public function testUserCheckout()
    {
        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('GET', 'api/cart/clear');

        $response
            ->assertJson(['status' => 'success'])
            ->assertStatus(200);

        $product = Product::InRandomOrder()->first();
        $user_token = $this->getUserToken(4);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('POST', 'api/cart/store', [
            'product_id' => $product->id,
            'sku_id' => $product->skus->first()->id,
            'qty' => 3,
            'notes' => 'pesan ganteng',
            'cut' => 1,
            'cut_notes' => '180',
        ]);

        $response
            ->assertJson(['status' => 'success'])
            ->assertStatus(200);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('GET', 'api/checkout/get');

        $response
            ->assertJsonStructure([
                'address',
                'items',
                'shipping_service',
                'shipping_etd',
                'subtotal_items',
                'subtotal_shipping',
                'subtotal_shipping_text',
                'total',
                'total_text',
                'address_ok',
                'shipping_ok',
                'create_order'
            ])
            ->assertStatus(200);
    }

    public function testOneCycle()
    {
        $user_id = Address::first()->user_id;
        $user_token = $this->getUserToken($user_id);
        $product = Product::InRandomOrder()->first();

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('GET', 'api/cart/clear');

        $response
            ->assertJson(['status' => 'success'])
            ->assertStatus(200);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('POST', 'api/cart/store', [
            'product_id' => $product->id,
            'sku_id' => $product->skus->first()->id,
            'qty' => 3,
            'notes' => 'pesan ganteng',
            'cut' => 3,
            'cut_notes' => '160|150|175',
        ]);

        $response
            ->assertJson(['status' => 'success'])
            ->assertStatus(200);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('GET', 'api/checkout/get');


        $response->assertJson([
            'create_order' => true,
            'address_ok' => true,
            'shipping_ok' => true,
        ]);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('POST', 'api/checkout/update/address', [
            'id' => 2
        ]);

        $response
            ->assertJson(['status' => 'success', 'message' => 'Address updated'])
            ->assertStatus(200);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('POST', 'api/checkout/commit', [
            'payment_id' => 'midtrans_va_bni',
            'dropshipper' => 1,
            'dropship_name' => 'Robbyn',
            'dropship_phone' => '082221114471'
        ]);

        $response->assertJson(['status' => 'success'])->assertStatus(200);
    }

    public function testExtendsCart()
    {
        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('GET', 'api/cart/extends');

        $response->assertStatus(401); // only 200 when minutes at 10-11
    }

    public function testUserDeleteOrder()
    {
        $user_token = $this->getUserToken(4);
        $product = Product::find(1);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('GET', 'api/cart/clear');

        $response
            ->assertJson(['status' => 'success'])
            ->assertStatus(200);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('POST', 'api/cart/store', [
            'product_id' => $product->id,
            'sku_id' => $product->skus->first()->id,
            'qty' => 3,
            'notes' => 'tidak di potong',
            'cut' => 2,
            'cut_notes' => '111|222',
        ]);

        $response->assertJson(['status' => 'success'])->assertStatus(200);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('GET', '/api/cart/get');

        $response->assertJson(['status' => 'success'])->assertStatus(200);

        $json = $response->getContent();
        $json = json_decode($json);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('POST', $json->data[0]->remove_url);

        $response
            ->assertJson(['status' => 'success', 'message' => 'Items removed'])
            ->assertJsonStructure(['status', 'message', 'total', 'total_text', 'item_count'])
            ->assertStatus(200);
    }

    public function testAppGetBadge()
    {
        $user_token = $this->getUserToken(4);
        $product = Product::find(1);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('GET', 'api/cart/clear');

        $response
            ->assertJson(['status' => 'success'])
            ->assertStatus(200);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('POST', 'api/cart/store', [
            'product_id' => $product->id,
            'sku_id' => $product->skus->first()->id,
            'qty' => 3,
            'notes' => 'tidak di potong',
            'cut' => 2,
            'cut_notes' => '111|222',
        ]);

        $response->assertJson(['status' => 'success'])->assertStatus(200);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('GET', '/api/badge');

        $response
            ->assertJson(['item_count' => 3, 'status' => 'success'])
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function userGetOrder()
    {
        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('GET', 'api/cart/clear');

        $response
            ->assertJson(['status' => 'success'])
            ->assertStatus(200);

        $product = Product::InRandomOrder()->first();
        $user_token = $this->getUserToken(4);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('POST', 'api/cart/store', [
            'product_id' => $product->id,
            'sku_id' => $product->skus->first()->id,
            'qty' => 2,
            'notes' => 'pesan ganteng',
            'cut' => 1,
            'cut_notes' => '100',
        ]);

        $response
            ->assertJson(['status' => 'success'])
            ->assertStatus(200);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('GET', 'api/checkout/get');

        $response->assertJson([
            'create_order' => true,
            'address_ok' => true,
            'shipping_ok' => true,
        ]);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('POST', 'api/checkout/update/address', [
            'id' => 2
        ]);

        $response
            ->assertJson(['status' => 'success', 'message' => 'Address updated'])
            ->assertStatus(200);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('POST', 'api/checkout/commit', [
            'payment_id' => 'midtrans_va_permata'
        ]);

        $response->assertJson(['status' => 'success'])->assertStatus(200);

        // START CHECK HERE
        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('GET', 'api/orders');

        $response
            ->assertJson(['status' => 'success'])
            ->assertJsonStructure(['status', 'data', 'message'])
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function userGetOrderHistory()
    {
        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('GET', 'api/cart/clear');

        $response
            ->assertJson(['status' => 'success'])
            ->assertStatus(200);

        $product = Product::InRandomOrder()->first();
        $user_token = $this->getUserToken(4);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('POST', 'api/cart/store', [
            'product_id' => $product->id,
            'sku_id' => $product->skus->first()->id,
            'qty' => 2,
            'notes' => 'test',
            'cut' => 1,
            'cut_notes' => '100',
        ]);

        $response
            ->assertJson(['status' => 'success'])
            ->assertStatus(200);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('GET', 'api/checkout/get');

        $response->assertJson([
            'create_order' => true,
            'address_ok' => true,
            'shipping_ok' => true,
        ]);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('POST', 'api/checkout/update/address', [
            'id' => 2
        ]);

        $response
            ->assertJson(['status' => 'success', 'message' => 'Address updated'])
            ->assertStatus(200);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('POST', 'api/checkout/commit', [
            'payment_id' => 'midtrans_va_permata'
        ]);

        $response->assertJson(['status' => 'success'])->assertStatus(200);

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('GET', 'api/orders');

        $response
            ->assertJson(['status' => 'success'])
            ->assertJsonStructure(['status', 'data', 'message'])
            ->assertStatus(200);

        // START CHECK HERE

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('GET', 'api/order_history/pending');

        $response
            ->assertJsonStructure(['data', 'links', 'meta'])
            ->assertStatus(200);
    }

    public function testUserUpdateCart()
    {
        $user_token = $this->getUserToken(4);
        $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('GET', 'api/cart/clear')
            ->assertStatus(200);

        $product = Product::InRandomOrder()->first();

        $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('POST', 'api/cart/store', [
            'product_id' => $product->id,
            'sku_id' => $product->skus->first()->id,
            'qty' => 3,
            'notes' => 'pesan ganteng',
            'cut' => 2,
            'cut_notes' => '111|222',
        ])
            ->assertJson(['status' => 'success'])
            ->assertStatus(200);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('GET', 'api/cart/get');

        $data = $response->getContent();
        $data = json_decode($data);

        $group_id = '';
        foreach ($data->data as $item) {
            if ($item->is_cut == 0) {
                $group_id = $item->group_id;
            }
        }

        $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('POST', 'api/cart/update/' . $group_id, [
            'product_id' => $product->id,
            'sku_id' => $product->skus->first()->id,
            'qty' => 2,
            'notes' => 'pesan ganteng',
            'is_cut' => 1,
            'cut_notes' => '444|555',
        ])
            ->assertJson(['status' => 'success'])
            ->assertStatus(200);
    }

    public function testUpdateCartWithCut()
    {
        $user_token = $this->getUserToken(4);
        $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('GET', 'api/cart/clear')
            ->assertStatus(200);

        $product = Product::InRandomOrder()->first();

        $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('POST', 'api/cart/store', [
            'product_id' => $product->id,
            'sku_id' => $product->skus->first()->id,
            'qty' => 1,
            'notes' => '',
            'cut' => 0,
            'cut_notes' => '',
        ])
            ->assertJson(['status' => 'success'])
            ->assertStatus(200);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('GET', 'api/cart/get');

        $json = json_decode($response->getContent());
        $group_id = $json->data[0]->group_id;

        $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('POST', 'api/cart/update/' . $group_id, [
            'product_id' => $product->id,
            'sku_id' => $product->skus->first()->id,
            'qty' => 4,
            'notes' => '',
            'is_cut' => 1,
            'cut_notes' => '111|222|333|444',
        ])
            ->assertJson(['status' => 'success'])
            ->assertStatus(200);
    }

    public function testOngkir()
    {
        $order  = \App\Order::whereNotNull('address_id')->first();
        $ongkir = new \App\Payment\ShippingList($order);

        dump($ongkir->get('service'));
    }
}
