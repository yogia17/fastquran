<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ApiLoginTest extends TestCase
{
    /**
     *
     * @test
     */
    public function successLogin()
    {
        $user = User::find(1);

        $response = $this->json('POST', '/api/login', [
            'email' => $user->email,
            'password' => 'secret'
        ]);

        dump($response->getContent());

        $response
            ->assertStatus(200)
            ->assertJson(['status' => 'success']);
    }

    /**
     * @test
     */
    public function cannotAccessProfile()
    {
        $response = $this->json('GET', '/api/profile/get');

        $response
            ->assertStatus(401)
            ->assertJson(['status' => 'error', 'message' => 'Token not provided']);
    }

    /**
     *
     * @test
     */
    public function cannotAccessWithWrongToken()
    {
        $response = $this->withHeaders([
            'X-Authorization' => 'this.is.wrong.token'
        ])->json('GET', '/api/profile/get');

        $response
            ->assertStatus(401)
            ->assertJson(['status' => 'error', 'message' => 'Silahkan login dulu ya']);
    }

    /**
     * @test
     */
    public function adminLogin()
    {
        $user = User::where('status', 'admin')->first();

        $response = $this->json('POST', '/api/login', [
            'email' => $user->email,
            'password' => 'secret'
        ]);

        $response
            ->assertStatus(200)
            ->assertJson(['status' => 'success'])
            ->assertSee('admin');
    }

    /**
     * @test
     */
    public function memberLogin()
    {
        $user = User::where('status', 'member')->first();

        $response = $this->json('POST', '/api/login', [
            'email' => $user->email,
            'password' => 'secret'
        ]);

        $response
            ->assertStatus(200)
            ->assertJson(['status' => 'success'])
            ->assertSee('member');
    }

    /**
     * @test
     */
    public function memberVipLogin()
    {
        $user = User::where('status', 'vip')->first();

        $response = $this->json('POST', '/api/login', [
            'email' => $user->email,
            'password' => 'secret'
        ]);

        $response
            ->assertStatus(200)
            ->assertJson(['status' => 'success'])
            ->assertSee('vip');
    }
}
