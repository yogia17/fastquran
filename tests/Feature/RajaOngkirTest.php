<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\External\Ongkir\RajaOngkir;

class RajaOngkirTest extends TestCase
{
    /**
     * harusnya ada 34 provinsi yang di dapat
     * @test
     */
    public function getAllProvince()
    {
        $courier = new RajaOngkir();
        $response = $courier->getProvince();

        $this->assertCount(34, $response);
    }

    /**
     * @test
     */
    public function getBaliProvince()
    {
        $courier = new RajaOngkir();
        $response = $courier->getProvince(1);
        $this->assertContains("Bali", $response);
    }

    /**
     * @test
     */
    public function getErrorProvince()
    {
        $courier = new RajaOngkir();
        $response = $courier->getProvince('999');
        $this->assertCount(0, $response);
    }

    /**
     * harusnya ada 501 provinsi yang di dapat
     * @test
     */
    public function getAllCity()
    {
        $courier = new RajaOngkir();
        $response = $courier->getCity();
        $this->assertCount(501, $response);
    }

    /**
     * @test
     */
    public function getCityofProvinceBali()
    {
        $courier = new RajaOngkir();
        $response = $courier->getCity(1);
        $this->assertCount(9, $response);
    }

    /**
     * @test
     */
    public function getCityofProvinceBaliWithId()
    {
        $courier = new RajaOngkir();
        $response = $courier->getCity(1, 447);
        $this->assertArrayHasKey('postal_code', $response->toArray());
    }

    /**
     * test Tracking Resi
     *
     * @test
     */
    public function trackingResi()
    {
        $courier = new RajaOngkir();
        $response = $courier->track('016060045445919', 'jne');

        $this->assertContains("ROBBYN RAHMANDARU", $response);
        $this->assertContains("DELIVERED", $response);
    }
}
