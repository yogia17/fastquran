<?php

namespace Tests\Feature;

use App\Sku;
use App\User;
use App\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiCartTest extends TestCase
{
    private function getUserToken($user_id)
    {
        $user = User::find($user_id);

        $response = $this->json('POST', '/api/login', [
            'email' => $user->email,
            'password' => 'secret'
        ]);

        $response = json_decode($response->getContent());

        return $response->token;
    }

    public function testAddCart()
    {
        $item = Sku::inRandomOrder()
            ->select('skus.*')
            ->join('products', 'products.id', '=', 'skus.product_id')
            ->where('products.status', 'publish')
            ->where('skus.qty_stock', '>', 0)
            ->first();

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(31)
        ])->json('POST', '/api/cart/store', [
            'product_id' => $item->product->id,
            'sku_id' => $item->id,
            'qty' => 3,
            'cut' => 2,
            'cut_notes' => '140|140'
        ]);

        $response
            ->assertJson(['status' => 'success'])
            ->assertStatus(200);
    }

    public function getCart()
    {
        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(31)
        ])->json('GET', '/api/cart/get');

        return json_decode($response->getContent());
    }

    public function testUpdateCart()
    {
        $cart = $this->getCart();
        $group_id = $cart->data[0]->group_id;

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(31)
        ])->json('POST', '/api/cart/update/' . $group_id, [
            'qty' => 1,
            'cut' => 1,
            'cut_notes' => '140'
        ]);

        $cart = $this->getCart();
        $group_id = $cart->data[0]->group_id;

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(31)
        ])->json('POST', '/api/cart/update/' . $group_id, [
            'qty' => 3,
            'cut' => 2,
            'cut_notes' => '140|140'
        ]);

        $cart = $this->getCart();
        $group_id = $cart->data[0]->group_id;

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(31)
        ])->json('POST', '/api/cart/update/' . $group_id, [
            'qty' => 3,
            'cut' => 3,
            'cut_notes' => '140|140|140'
        ]);

        dump($response->getContent());
    }
}
