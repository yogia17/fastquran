<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Str;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MidtransTest extends TestCase
{
    public function __construct()
    {
        parent::__construct();
        \Veritrans_Config::$serverKey = 'SB-Mid-server-J8GjAj69p80yM8Zda1-1oW28';
        \Veritrans_Config::$isProduction = false;
        \Veritrans_Config::$isSanitized = true;
        \Veritrans_Config::$is3ds = true;
    }

    public function testVaPermata()
    {
        $order_id = Str::random(10);

        $params = [
            'payment_type' => 'bank_transfer',
            'bank_transfer' => [
                'bank' => 'permata',
                'permata' => [
                    'recipient_name' => 'SUDARSONO'
                ]
            ],
            'transaction_details' => [
                'order_id' => $order_id,
                'gross_amount' => 145000
            ]
        ];

        $response = \Veritrans_VtDirect::charge($params);

        $this->assertSame($response->order_id, $order_id);
        $this->assertSame($response->status_code, "201");
        $this->assertSame($response->payment_type, "bank_transfer");
        $this->assertSame($response->transaction_status, "pending");
        $this->assertSame($response->fraud_status, "accept");
        $this->assertSame(sprintf("%d.00", 145000), $response->gross_amount);
        $this->assertContains('82800', $response->permata_va_number);
    }

    public function testVaBca()
    {
        $order_id = Str::random(10);

        $params = [
            'payment_type' => 'bank_transfer',
            'bank_transfer' => [
                'bank' => 'bca',
                'va_number' => '1212'
            ],
            'transaction_details' => [
                'order_id' => $order_id,
                'gross_amount' => 145000
            ]
        ];

        $response = \Veritrans_VtDirect::charge($params);

        $this->assertSame($response->order_id, $order_id);
        $this->assertSame($response->status_code, "201");
        $this->assertSame($response->payment_type, "bank_transfer");
        $this->assertSame($response->transaction_status, "pending");
        $this->assertSame($response->fraud_status, "accept");
        $this->assertSame(sprintf("%d.00", 145000), $response->gross_amount);
        // $this->assertContains('1212', $response->va_numbers[0]->va_number);
        $this->assertSame('bca', $response->va_numbers[0]->bank);
    }

    public function testVaMandiriBill()
    {
        $order_id = Str::random(10);

        $params = [
            'payment_type' => 'echannel',
            'echannel' => [
                'bill_info1' => 'Payment For:',
                'bill_info2' => 'debt'
            ],
            'transaction_details' => [
                'order_id' => $order_id,
                'gross_amount' => 145000
            ]
        ];

        $response = \Veritrans_VtDirect::charge($params);

        $this->assertSame($response->order_id, $order_id);
        $this->assertSame($response->status_code, "201");
        $this->assertSame($response->payment_type, "echannel");
        $this->assertSame($response->transaction_status, "pending");
        $this->assertSame($response->fraud_status, "accept");
        $this->assertSame(sprintf("%d.00", 145000), $response->gross_amount);
        $this->assertObjectHasAttribute('biller_code', $response);
        $this->assertObjectHasAttribute('bill_key', $response);
    }

    public function testVaBni()
    {
        $order_id = Str::random(10);

        $params = [
            'payment_type' => 'bank_transfer',
            'bank_transfer' => [
                'bank' => 'bni',
                'va_number' => '121210' // ini adalah random va numbers
            ],
            'transaction_details' => [
                'order_id' => $order_id,
                'gross_amount' => 145000
            ]
        ];

        $response = \Veritrans_VtDirect::charge($params);

        $this->assertSame($response->order_id, $order_id);
        $this->assertSame($response->status_code, "201");
        $this->assertSame($response->payment_type, "bank_transfer");
        $this->assertSame($response->transaction_status, "pending");
        $this->assertSame($response->fraud_status, "accept");
        $this->assertSame(sprintf("%d.00", 145000), $response->gross_amount);
        $this->assertObjectHasAttribute('bank', $response->va_numbers[0]);
        $this->assertObjectHasAttribute('va_number', $response->va_numbers[0]);
        // $this->assertContains('121210', $response->va_numbers[0]->va_number);
    }

    public function testIbBcaKlikpay()
    {
        $order_id = Str::random(10);

        $params = [
            'payment_type' => 'bca_klikpay',
            'bca_klikpay' => [
                'description' => 'pembelian barang',
            ],
            'transaction_details' => [
                'order_id' => $order_id,
                'gross_amount' => 145000
            ]
        ];

        $response = \Veritrans_VtDirect::charge($params);

        $this->assertSame($response->order_id, $order_id);
        $this->assertSame($response->status_code, "201");
        $this->assertSame($response->payment_type, "bca_klikpay");
        $this->assertSame($response->transaction_status, "pending");
        $this->assertSame($response->fraud_status, "accept");
        $this->assertSame(sprintf("%d.00", 145000), $response->gross_amount);
        $this->assertObjectHasAttribute('redirect_url', $response);
        $this->assertObjectHasAttribute('redirect_data', $response);
    }

    public function testIbBcaKlikbca()
    {
        $order_id = Str::random(10);

        $params = [
            'payment_type' => 'bca_klikbca',
            'bca_klikbca' => [
                'description' => 'testing',
                'user_id' => 'midtrans1012'
            ],
            'transaction_details' => [
                'order_id' => $order_id,
                'gross_amount' => 145000
            ]
        ];

        $response = \Veritrans_VtDirect::charge($params);

        $this->assertSame($response->order_id, $order_id);
        $this->assertSame($response->status_code, "201");
        $this->assertSame($response->payment_type, "bca_klikbca");
        $this->assertSame($response->transaction_status, "pending");
        // $this->assertSame($response->fraud_status, "accept");
        $this->assertSame($response->approval_code, $order_id);
        $this->assertSame(sprintf("%d.00", 145000), $response->gross_amount);
        $this->assertObjectHasAttribute('redirect_url', $response);
        // $this->assertObjectHasAttribute('redirect_data', $response);
    }

    public function testCounterIndomaret()
    {
        $order_id = Str::random(10);

        $params = [
            'payment_type' => 'cstore',
            'cstore' => [
                'store' => 'Indomaret',
                'message' => 'Tiket1 transaction'
            ],
            'transaction_details' => [
                'order_id' => $order_id,
                'gross_amount' => 145000
            ]
        ];

        $response = \Veritrans_VtDirect::charge($params);

        $this->assertSame($response->order_id, $order_id);
        $this->assertSame($response->status_code, "201");
        $this->assertSame($response->payment_type, "cstore");
        $this->assertSame($response->transaction_status, "pending");
        $this->assertSame($response->store, 'indomaret');
        $this->assertSame(sprintf("%d.00", 145000), $response->gross_amount);
        $this->assertObjectHasAttribute('payment_code', $response);
        // $this->assertObjectHasAttribute('redirect_data', $response);
    }

    public function testCounterAlfamart()
    {
        return 'alfamart endak bisa di akses jika belum di aktifkan';

        $order_id = Str::random(10);

        $params = [
            'payment_type' => 'cstore',
            'cstore' => [
                'store' => 'alfamart',
                'alfamart_free_text_1' => 'Tiket1 transaction',
                'alfamart_free_text_2' => 'Tiket1 transaction',
                'alfamart_free_text_3' => 'Tiket1 transaction',
            ],
            'transaction_details' => [
                'order_id' => $order_id,
                'gross_amount' => 145000
            ]
        ];

        $response = \Veritrans_VtDirect::charge($params);

        $this->assertSame($response->order_id, $order_id);
        $this->assertSame($response->status_code, "201");
        $this->assertSame($response->payment_type, "cstore");
        $this->assertSame($response->transaction_status, "pending");
        $this->assertSame($response->store, 'alfamart');
        $this->assertSame(sprintf("%d.00", 145000), $response->gross_amount);
        $this->assertObjectHasAttribute('payment_code', $response);
        // $this->assertObjectHasAttribute('redirect_data', $response);
    }

    public function customExpiry()
    {
        return [
            'custom_expiry' => [
                "order_time" => "2016-12-07 11:54:12 +0700",
                "expiry_duration" => 60,
                "unit" => "minute"
            ]
        ];
    }
}
