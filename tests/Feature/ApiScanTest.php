<?php

namespace Tests\Feature;

use App\Product;
use App\Sku;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ApiScanTest extends TestCase
{
    use DatabaseTransactions;

    private function getUserToken($user_id)
    {
        $user = User::find($user_id);

        $response = $this->json('POST', '/api/login', [
            'email' => $user->email,
            'password' => 'secret'
        ]);

        $response = json_decode($response->getContent());

        return $response->token;
    }

    /**
     * @test
     */
    public function printSampleQuickResponseCode()
    {
        $response = $this->get('/api/scan/sample');
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function scanWarehouseHasVariations()
    {
        $sku = Sku::orderby('id','desc')->first();
        $json_code = json_encode(['code' => $sku->sku, 'unique' => '00001']);

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1),
        ])->json('POST', '/api/scan/warehouse', [
            'code' => $json_code
        ]);

        $response
            ->assertJson(['status' => 'success'])
            ->assertJsonStructure(['status', 'message'])
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function scanWarehouseNonVariations()
    {
        $sku = Product::whereNotNull('sku')->orderby('id','desc')->first();

        $json_code = json_encode(['code' => $sku->sku, 'unique' => '00001']);

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1),
        ])->json('POST', '/api/scan/warehouse', [
            'code' => $json_code
        ]);

        $response
            ->assertJson(['status' => 'success'])
            ->assertJsonStructure(['status', 'message'])
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function cannotMultipleScanWarehouse()
    {
        $sku = Sku::find(1);
        $user_token = $this->getUserToken(1);

        $json_code = json_encode(['code' => $sku->sku, 'unique' => '00001']);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token,
        ])->json('POST', '/api/scan/warehouse', [
            'code' => $json_code
        ]);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token,
        ])->json('POST', '/api/scan/warehouse', [
            'code' => $json_code
        ]);

        $response
            ->assertJson(['status' => 'error', 'message' => 'Entry Duplicated'])
            ->assertJsonStructure(['status', 'message'])
            ->assertStatus(401);
    }

    /**
     * @test
     */
    public function warehouseCannotScanWithInvalidSku()
    {
        $json_code = json_encode(['code' => 'ZB123456', 'unique' => '00001']);

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1),
        ])->json('POST', '/api/scan/warehouse', [
            'code' => $json_code
        ]);

        $response
            ->assertJson(['status' => 'error', 'message' => 'Code not registered'])
            ->assertJsonStructure(['status', 'message'])
            ->assertStatus(401);
    }

    /**
     * @test
     */
    public function warehouseCannotReceiveInvalidJson()
    {
        $json_code = '{}{}{}{}{}';

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1),
        ])->json('POST', '/api/scan/warehouse', [
            'code' => $json_code
        ]);

        $response
            ->assertJson(['status' => 'error', 'message' => 'Invalid Json Code'])
            ->assertJsonStructure(['status', 'message'])
            ->assertStatus(401);
    }

    /**
     * @test
     */
    public function userWithoutPremissionCannotAccessWarehouse()
    {
        $sku = Sku::find(1);

        $json_code = json_encode(['code' => $sku->sku, 'unique' => '00001']);

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(3),
        ])->json('POST', '/api/scan/warehouse', [
            'code' => $json_code
        ]);

        $response
            ->assertJson(['status' => 'error'])
            ->assertJsonStructure(['status', 'message'])
            ->assertStatus(401);
    }

    /**
     * @test
     */
    public function scanEtalase()
    {
        $sku = Sku::find(1);

        $json_code = json_encode(['code' => $sku->sku, 'unique' => '00001']);

        $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1),
        ])->json('POST', '/api/scan/warehouse', [
            'code' => $json_code
        ]);

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1),
        ])->json('POST', '/api/scan/storefront', [
            'code' => $json_code
        ]);

        $response
            ->assertJson(['status' => 'success'])
            ->assertJsonStructure(['status', 'message'])
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function scanEtalaseNonVarition()
    {
        // 1. Scan to gudang first
        // 2. Then, scan to etalase
        
        $sku = Product::whereNotNull('sku')->orderby('id','desc')->first();

        $json_code = json_encode(['code' => $sku->sku, 'unique' => '00001']);

        $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1),
        ])->json('POST', '/api/scan/warehouse', [
            'code' => $json_code
        ]);

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1),
        ])->json('POST', '/api/scan/storefront', [
            'code' => $json_code
        ]);

        $response
            ->assertJson(['status' => 'success'])
            ->assertJsonStructure(['status', 'message'])
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function cannotMultipleScanEtalase()
    {
        $sku = Sku::find(1);

        $json_code = json_encode(['code' => $sku->sku, 'unique' => '00001']);

        $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1),
        ])->json('POST', '/api/scan/warehouse', [
            'code' => $json_code
        ]);

        $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1),
        ])->json('POST', '/api/scan/storefront', [
            'code' => $json_code
        ]);

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1),
        ])->json('POST', '/api/scan/storefront', [
            'code' => $json_code
        ]);

        $response
            ->assertJson(['status' => 'error'])
            ->assertJsonStructure(['status', 'message'])
            ->assertStatus(401);
    }

    /**
     * @test
     */
    public function storefrontCannotScanWithInvalidSku()
    {
        $json_code = json_encode(['code' => 'ZB123456', 'unique' => '00001']);

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1),
        ])->json('POST', '/api/scan/storefront', [
            'code' => $json_code
        ]);

        $response
            ->assertJson(['status' => 'error', 'message' => 'Code not registered'])
            ->assertJsonStructure(['status', 'message'])
            ->assertStatus(401);
    }

    /**
     * @test
     */
    public function storefrontCannotReceiveInvalidJson()
    {
        $json_code = '{}{}{}{}{}';

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1),
        ])->json('POST', '/api/scan/storefront', [
            'code' => $json_code
        ]);

        $response
            ->assertJson(['status' => 'error', 'message' => 'Invalid Json Code'])
            ->assertJsonStructure(['status', 'message'])
            ->assertStatus(401);
    }

    /**
     * @test
     */
    public function userWithoutPremissionCannotAccessStorefront()
    {
        $sku = Sku::find(1);

        $json_code = json_encode(['code' => $sku->sku, 'unique' => '00001']);

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(2),
        ])->json('POST', '/api/scan/storefront', [
            'code' => $json_code
        ]);

        $response
            ->assertJson(['status' => 'error'])
            ->assertJsonStructure(['status', 'message'])
            ->assertStatus(401);
    }

    /**
     * @test
     */
    public function itemOnEtalaseCannotAddAgain()
    {
        $sku = Sku::find(1);

        $json_code = json_encode(['code' => $sku->sku, 'unique' => '00001']);

        $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1),
        ])->json('POST', '/api/scan/warehouse', [
            'code' => $json_code
        ]);

        $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1),
        ])->json('POST', '/api/scan/storefront', [
            'code' => $json_code
        ]);

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1),
        ])->json('POST', '/api/scan/warehouse', [
            'code' => $json_code
        ]);

        $response
            ->assertJson(['status' => 'error'])
            ->assertJsonStructure(['status', 'message'])
            ->assertStatus(401);
    }

    /**
     * @test
     */
    public function scanReturnToWarehouse()
    {
        // 1. Scan to gudang
        // 2. Scan to etalase
        // 3. Scan restore
        $sku = Sku::find(1);

        $json_code = json_encode(['code' => $sku->sku, 'unique' => '00001']);

        $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1),
        ])->json('POST', '/api/scan/warehouse', [
            'code' => $json_code
        ]);

        $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1),
        ])->json('POST', '/api/scan/storefront', [
            'code' => $json_code
        ]);

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1),
        ])->json('POST', '/api/scan/restore', [
            'code' => $json_code
        ]);

        $response
            ->assertJson(['status' => 'success'])
            ->assertJsonStructure(['status', 'message'])
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function scanReturnToWarehouseNonVariations()
    {
        // 1. Scan from gudang
        // 2. Scan from etalase
        // 3. Scan restore
        
        $sku = Product::whereNotNull('sku')->orderby('id','desc')->first();

        $json_code = json_encode(['code' => $sku->sku, 'unique' => '00001']);

        $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1),
        ])->json('POST', '/api/scan/warehouse', [
            'code' => $json_code
        ]);

        $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1),
        ])->json('POST', '/api/scan/storefront', [
            'code' => $json_code
        ]);

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1),
        ])->json('POST', '/api/scan/restore', [
            'code' => $json_code
        ]);

        $response
            ->assertJson(['status' => 'success'])
            ->assertJsonStructure(['status', 'message'])
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function scanShipping()
    {
        // 1. Scan to gudang
        // 2. Scan to etalase
        // 3. Scan to shipping
        $sku = Sku::find(1);

        $json_code = json_encode(['code' => $sku->sku, 'unique' => '00001']);

        $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1),
        ])->json('POST', '/api/scan/warehouse', [
            'code' => $json_code
        ]);

        $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1),
        ])->json('POST', '/api/scan/storefront', [
            'code' => $json_code
        ]);

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1),
        ])->json('POST', '/api/scan/shipping', [
            'code' => $json_code
        ]);

        $response
            ->assertJson(['status' => 'success'])
            ->assertJsonStructure(['status', 'message'])
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function scanShippingNonVariations()
    {
        // 1. Scan to gudang
        // 2. Scan to etalase
        // 3. Scan to shipping
        
        $sku = Product::whereNotNull('sku')->orderby('id','desc')->first();

        $json_code = json_encode(['code' => $sku->sku, 'unique' => '00001']);

        $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1),
        ])->json('POST', '/api/scan/warehouse', [
            'code' => $json_code
        ]);

        $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1),
        ])->json('POST', '/api/scan/storefront', [
            'code' => $json_code
        ]);

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1),
        ])->json('POST', '/api/scan/shipping', [
            'code' => $json_code
        ]);

        $response
            ->assertJson(['status' => 'success'])
            ->assertJsonStructure(['status', 'message'])
            ->assertStatus(200);
    }
}
