<?php

namespace Tests\Feature;

use App\Product;
use App\Sku;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ApiBrandTest extends TestCase
{
    use DatabaseTransactions;

    private function getUserToken($user_id)
    {
        $user = User::find($user_id);

        $response = $this->json('POST', '/api/login', [
            'email' => $user->email,
            'password' => 'secret'
        ]);

        $response = json_decode($response->getContent());

        return $response->token;
    }

    /**
     * @test
     */
    public function home()
    {
        $response = $this->json('GET', '/api/home');

        $response->assertJsonStructure(['brands'])
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function brand()
    {
        
        $response = $this->json('GET', '/api/brand/1');

        $response->assertJsonStructure(['category','slider','bestbuy'])
            ->assertStatus(200);
    }

}
