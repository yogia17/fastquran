<?php

namespace Tests\Feature;

use App\Shipping;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ShippingTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testProvince()
    {
        dump(Shipping::getProvince()->toArray());
    }

    public function testCity()
    {
        dump(Shipping::getCity(1)->toArray());
    }

    public function testSubddistrict()
    {
        dump(Shipping::getSubdistrict(447)->toArray());
    }

    public function testCost()
    {
        dump(Shipping::getCost(6188)->toArray());
    }
}
