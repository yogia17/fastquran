<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Combination;

class AnyTest extends TestCase
{
    public function testGetCombination()
    {
        $response = Combination::getModels(1);

        $this->assertIsArray($response);
    }
}
