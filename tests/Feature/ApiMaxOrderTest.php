<?php

namespace Tests\Feature;

use App\User;
use App\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiMaxOrderTest extends TestCase
{
    public function auth($user_id)
    {
        $user = User::find($user_id);

        $response = $this->json('POST', '/api/login', [
            'email' => $user->email,
            'password' => 'secret'
        ]);

        $response = json_decode($response->getContent());

        return $response->token;
    }

    public function clearCart($user_id)
    {
        $response = $this
            ->withHeaders(['X-Authorization' => $this->auth($user_id)])
            ->json('GET', 'api/cart/clear');

        $response->assertStatus(200);
    }

    public function testMaximalOrderProduct()
    {
        $user_id = 4;
        $product = Product::find(1);

        $this->clearCart($user_id);

        $response = $this
            ->withHeaders([
                'X-Authorization' => $this->auth($user_id)
            ])
            ->json('POST', 'api/cart/store', [
                'product_id' => $product->id,
                'sku_id' => $product->skus->first()->id,
                'qty' => 6,
                'notes' => 'test note',
                'cut' => 2,
                'cut_notes' => '111|222',
            ]);

        $response->assertStatus(401);
    }

    public function testMaximalOrderSku()
    {
        $user_id = 4;
        $product = Product::find(1);

        $this->clearCart($user_id);

        $response = $this
            ->withHeaders([
                'X-Authorization' => $this->auth($user_id)
            ])
            ->json('POST', 'api/cart/store', [
                'product_id' => $product->id,
                'sku_id' => $product->skus->first()->id,
                'qty' => 4,
                'notes' => 'test note',
                'cut' => 2,
                'cut_notes' => '111|222',
            ]);

        $response->assertStatus(401);
    }
}
